package com.intenocloud.core.login;

import com.intenocloud.wifijoe3.LoginActivity;


/**
 * Created by erik on 9/12/13.
 */
public class LoginActivityStatus implements Runnable {
    static private boolean ongoing = false;
    private String userName = "";
    private String passWord = "";
    private String server = "";
    private Thread runner;

    public LoginActivityStatus() {
        if (ongoing) {
            System.out.println("LoginActivityStatus already ongoing");
            runner = null;
            return;
        }
        ongoing = true;
        runner = new Thread(this, "LoginActivityStatus"); // (1) Create a new thread.
        System.out.println(runner.getName());
    }
    public void start(String userName, String passWord, String server) {
        this.userName = userName;
        this.passWord = passWord;
        this.server = server;
        if (runner != null) {
            runner.start();
        }
    }
    public void run() {
        LoginActivity.loginActivity.cf.connect(userName, passWord, server);
        ongoing = false;
    }

}
