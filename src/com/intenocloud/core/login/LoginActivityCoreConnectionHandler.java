package com.intenocloud.core.login;


import com.intenocloud.core.CoreConnectionHandler;
import com.intenocloud.wifijoe3.LoginActivity;

/**
 * Created by erik on 6/11/13.
 */
public class LoginActivityCoreConnectionHandler extends CoreConnectionHandler {
    private String type;

    public LoginActivityCoreConnectionHandler(String type) {
        this.type = type;
    }

    @Override
    public void connected() {
        if (type.compareTo("pairing") == 0) {
            LoginActivity.loginActivity.update("pairingConnected");
        }else{
            LoginActivity.loginActivity.update("connected");
        }
    }
    @Override
    public void disconnected(int reason) {
        System.out.println("Disconnected from Cloud Server");
        if (type.compareTo("pairing") == 0) {
            LoginActivity.loginActivity.update("pairingDisconnected");
        }else{
            if (reason == CoreConnectionHandler.AuthenticationFailed) {
                // Back to login screen we go
                LoginActivity.loginActivity.update("authenticationFailed");
            } else if (reason == CoreConnectionHandler.UserDisconnect) {
                // update button status, disconnect performed
                LoginActivity.loginActivity.update("userDisconnect");
            }else{
                LoginActivity.loginActivity.update("disconnected");
            }
        }
    }
}
