package com.intenocloud.core.login;

import java.util.Set;

import org.cretz.swig.collection.NativeSet;

import com.intenocloud.core.BlockedCommandHandler;
import com.intenocloud.core.BlockedCommandResult;
import com.intenocloud.core.CommandEntry;
import com.intenocloud.core.JNIbridge;
import com.intenocloud.core.SWIGTYPE_p_std__listT_std__string_t;
import com.intenocloud.core.ServicePresenceHandler;
import com.intenocloud.core.StringListIterator;
import com.intenocloud.core.StringListWrapper;
import com.intenocloud.wifijoe3.LoginActivity;
import com.intenocloud.wifijoe3.screenActivity;

/**
 * Created by erik on 6/21/13.
 */
public class PairingServicePresenceHandler extends ServicePresenceHandler {
    private boolean gettingInfoOngoing = false;
    private boolean firstTime = true;
    protected screenActivity notifyUpdateScreenActivityObject = null;
    LoginDiscoveredGateway[] loginDiscoveredGatewaysOld;
    LoginDiscoveredGateway[] loginDiscoveredGatewaysNew;

    public PairingServicePresenceHandler() {
        System.out.println("PairingServicePresenceHandler: created");
        loginDiscoveredGatewaysOld = new LoginDiscoveredGateway[0];
        loginDiscoveredGatewaysNew = new LoginDiscoveredGateway[0];
    }

    @Override
    public void handlePresence(String from, int state) {
        System.err.println("PairingServicePresenceHandler: Presence event received from: " + from + " state " + state);
        if (gettingInfoOngoing) return;
        if (gatewayPresent(from, state) == true) return;
        gettingInfoOngoing = true;
        System.out.println("PairingServicePresenceHandler: updating list for " + from);
        new Thread(new Runnable() {
            public void run() {
                updateGatewaysInfo();
                pushUpdate();
            }
        }).start();
        gettingInfoOngoing = false;
    }
    private boolean gatewayPresent(String from, int state) {
        if (loginDiscoveredGatewaysOld.length == 0) return false;
        int j = 0;
        while (j < loginDiscoveredGatewaysOld.length) {
            if (loginDiscoveredGatewaysOld[j].deviceName.compareTo(from) == 0) {
                System.out.println("PairingServicePresenceHandler: already present in list: " + loginDiscoveredGatewaysOld[j].deviceName);
                if (state == Down) return false; // will refresh the list, to remove the entry
            }
            j++;
        }
        return true;
    }
    protected void pushUpdate() {
        // Run through registered Activities and give update
        if (notifyUpdateScreenActivityObject == null) return;
        notifyUpdateScreenActivityObject.update("ServicePresence");
    }
    public void registerUpdateScreenEvent(screenActivity screenActivityObject) {
        notifyUpdateScreenActivityObject = screenActivityObject;
        gettingInfoOngoing = true;
        if (firstTime) {
            firstTime = false;
            System.out.println("PairingServicePresenceHandler: Checking for first time if there were events missed");
            new Thread(new Runnable() {
                public void run() {
                    updateGatewaysInfo();
                    pushUpdate();
                }
            }).start();
            gettingInfoOngoing = false;
        }
    }
    private void updateGatewaysInfo() {
        SWIGTYPE_p_std__listT_std__string_t accList;
        SWIGTYPE_p_std__listT_std__string_t typeList;
        SWIGTYPE_p_std__listT_std__string_t localList;
        Set<String> set;
        String[] listAccount;
        String[] listType;
        String[] listLocal;
        String attachedServer = LoginActivity.loginActivity.server;

        if (LoginActivity.loginActivity.cf.isConnectedToServer() == false) {
            return;
        }
        accList = JNIbridge.getEmptyCoreStringList();
        typeList = JNIbridge.getEmptyCoreStringList();
        localList = JNIbridge.getEmptyCoreStringList();

        LoginActivity.loginActivity.cf.getPresenceList(accList, typeList, localList);
        set = new NativeSet<String>(String.class, StringListIterator.class,
                accList, StringListWrapper.class);
        listAccount = set.toArray(new String[0]);
        set = new NativeSet<String>(String.class, StringListIterator.class,
                typeList, StringListWrapper.class);
        listType = set.toArray(new String[0]);
        set = new NativeSet<String>(String.class, StringListIterator.class,
                localList, StringListWrapper.class);
        listLocal = set.toArray(new String[0]);

        int j = 0;
        int gwCount = 0;
        while (j < listAccount.length) {
            System.out.println("Account " + listAccount[j] + "  type: " + listType[j] + " local: " + listLocal[j]);
            if ((listAccount[j].contains(attachedServer))
                    && (listType[j].equals("gateway"))
                    && (listLocal[j].equals("true"))) {
                System.out.println("PairingServicePresenceHandler: found gateway jid: " + listAccount[j]);
                gwCount++;
            }
            j++;
        }
        if (gwCount == 0) {
            loginDiscoveredGatewaysNew = new LoginDiscoveredGateway[0];
            loginDiscoveredGatewaysOld = loginDiscoveredGatewaysNew;
            return;
        }

        loginDiscoveredGatewaysNew = new LoginDiscoveredGateway[gwCount];
        BlockedCommandHandler request = new BlockedCommandHandler();

        j = 0;
        int i = 0;
        while (j < listAccount.length) {
            SWIGTYPE_p_std__listT_std__string_t accessList;
            String[] listAccess;

            accessList = JNIbridge.getEmptyCoreStringList();


            if ((listAccount[j].contains(attachedServer))
                    && (listType[j].equals("gateway"))
                    && (listLocal[j].equals("true"))) {
                LoginActivity.loginActivity.cf.getLocalAccessList(listAccount[j], accessList);

                set = new NativeSet<String>(String.class, StringListIterator.class,
                        accessList, StringListWrapper.class);
                listAccess = set.toArray(new String[0]);

                System.out.println("PairingServicePresenceHandler: adding gateway jid: " + listAccount[j]);
                boolean local = false;
                if (listLocal[j].compareTo("true") == 0) local = true;

                String serialNumber = "";
                if (local) {
                    if (listAccess.length != 0) {
                        System.out.println("PairingServicePresenceHandler: sending identify.device for " + listAccount[j]);
                        String dst = listAccess[0];
                        CommandEntry cmd = new CommandEntry("device", "");
                        LoginActivity.loginActivity.cf.sendHttpCmd("identify", cmd, dst, request);
                        BlockedCommandResult result = request.waitForResult();

                        if (result.getResultType() != BlockedCommandResult.REPLY) {
                            System.err.println("PairingServicePresenceHandler: Did not receive reply for pairing");

                        }else{
                            serialNumber = result.getCmd().getReply("serial");
                        }
                    }
                }
                loginDiscoveredGatewaysNew[i] = new LoginDiscoveredGateway(
                        local,
                        listAccount[j],
                        serialNumber);
            }
            j++;
        }
        loginDiscoveredGatewaysOld = loginDiscoveredGatewaysNew;
    }

    public LoginDiscoveredGateway[] getDiscovererdGateways() {
        return loginDiscoveredGatewaysOld;
    }

}
