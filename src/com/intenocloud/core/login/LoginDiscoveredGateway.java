package com.intenocloud.core.login;

/**
 * Created by erik on 11/7/13.
 */
public class LoginDiscoveredGateway {
    public Boolean connected;
    public String deviceName;
    public String serial;

    public LoginDiscoveredGateway() {
    }
    public LoginDiscoveredGateway(Boolean connected, String deviceName, String serial) {
        this.connected = connected;
        this.deviceName = deviceName;
        this.serial = serial;
    }
}
