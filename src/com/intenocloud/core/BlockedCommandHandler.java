/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.10
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.intenocloud.core;

public class BlockedCommandHandler extends CommandHandler {
  private long swigCPtr;

  protected BlockedCommandHandler(long cPtr, boolean cMemoryOwn) {
    super(JNIbridgeJNI.BlockedCommandHandler_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(BlockedCommandHandler obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        JNIbridgeJNI.delete_BlockedCommandHandler(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    JNIbridgeJNI.BlockedCommandHandler_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    JNIbridgeJNI.BlockedCommandHandler_change_ownership(this, swigCPtr, true);
  }

  public BlockedCommandHandler() {
    this(JNIbridgeJNI.new_BlockedCommandHandler(), true);
    JNIbridgeJNI.BlockedCommandHandler_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  public BlockedCommandResult waitForResult() {
    return new BlockedCommandResult(JNIbridgeJNI.BlockedCommandHandler_waitForResult(swigCPtr, this), true);
  }

  public void commandReply(String topic, CommandEntry cmd, String from) {
    if (getClass() == BlockedCommandHandler.class) JNIbridgeJNI.BlockedCommandHandler_commandReply(swigCPtr, this, topic, CommandEntry.getCPtr(cmd), cmd, from); else JNIbridgeJNI.BlockedCommandHandler_commandReplySwigExplicitBlockedCommandHandler(swigCPtr, this, topic, CommandEntry.getCPtr(cmd), cmd, from);
  }

  public void commandError(String topic, CommandEntry cmd, String from, int err) {
    if (getClass() == BlockedCommandHandler.class) JNIbridgeJNI.BlockedCommandHandler_commandError(swigCPtr, this, topic, CommandEntry.getCPtr(cmd), cmd, from, err); else JNIbridgeJNI.BlockedCommandHandler_commandErrorSwigExplicitBlockedCommandHandler(swigCPtr, this, topic, CommandEntry.getCPtr(cmd), cmd, from, err);
  }

}
