/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.10
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.intenocloud.core;

public class ServiceCommunication extends CommunicationBase {
  private long swigCPtr;

  protected ServiceCommunication(long cPtr, boolean cMemoryOwn) {
    super(JNIbridgeJNI.ServiceCommunication_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ServiceCommunication obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        JNIbridgeJNI.delete_ServiceCommunication(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    JNIbridgeJNI.ServiceCommunication_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    JNIbridgeJNI.ServiceCommunication_change_ownership(this, swigCPtr, true);
  }

  public ServiceCommunication() {
    this(JNIbridgeJNI.new_ServiceCommunication__SWIG_0(), true);
    JNIbridgeJNI.ServiceCommunication_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  public ServiceCommunication(String topicName) {
    this(JNIbridgeJNI.new_ServiceCommunication__SWIG_1(topicName), true);
    JNIbridgeJNI.ServiceCommunication_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  public void registerService() {
    JNIbridgeJNI.ServiceCommunication_registerService(swigCPtr, this);
  }

  public void start(ServiceCommunicationEventHandler sceh) {
    if (getClass() == ServiceCommunication.class) JNIbridgeJNI.ServiceCommunication_start(swigCPtr, this, ServiceCommunicationEventHandler.getCPtr(sceh), sceh); else JNIbridgeJNI.ServiceCommunication_startSwigExplicitServiceCommunication(swigCPtr, this, ServiceCommunicationEventHandler.getCPtr(sceh), sceh);
  }

  public void stop() {
    if (getClass() == ServiceCommunication.class) JNIbridgeJNI.ServiceCommunication_stop(swigCPtr, this); else JNIbridgeJNI.ServiceCommunication_stopSwigExplicitServiceCommunication(swigCPtr, this);
  }

  public void setLSceh(ServiceCommunicationEventHandler value) {
    JNIbridgeJNI.ServiceCommunication_lSceh_set(swigCPtr, this, ServiceCommunicationEventHandler.getCPtr(value), value);
  }

  public ServiceCommunicationEventHandler getLSceh() {
    long cPtr = JNIbridgeJNI.ServiceCommunication_lSceh_get(swigCPtr, this);
    return (cPtr == 0) ? null : new ServiceCommunicationEventHandler(cPtr, false);
  }

}
