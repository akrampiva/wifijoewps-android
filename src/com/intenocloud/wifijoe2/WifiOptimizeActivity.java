package com.intenocloud.wifijoe2;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.intenocloud.wifijoe3.LoginActivity;
import com.intenocloud.wifijoe3.R;
import com.intenocloud.wifijoe3.WifiOptimizeSetChannel;
import com.intenocloud.wifijoe3.WifiOptimizeStatus;
import com.intenocloud.wifijoe3.screenActivity;

/**
 * Created by erik on 9/4/13.
 */
public class WifiOptimizeActivity extends Activity implements screenActivity {
	// The BroadcastReceiver that tracks network connectivity changes.
	private NetworkReceiver receiver = null;

	private Handler hm;
	private WifiOptimizeStatus oStatus = null;
	private WifiOptimizeActivity wifiOptimizeActivity = null;
	private boolean runCurrentScan = false;
	private boolean runOptimizeScan = false;
	private String currentChannel = "";
	private String bestChannel = "";
	@SuppressWarnings("unused")
	private int bestDataSent = 0;
	private int currentDataSent = 0;
	@SuppressWarnings("unused")
	private int currentTimeSent = 0;
	private Double currentSpeed = 0.0;
	private int maxTestTime = 10; // seconds
	private int currentSpeedTestProgress = 0;
	private WifiOptimizeSetChannel wifiOptimizeSetChannel = null;
	private boolean noLongerConnectedLocal = false;
	private AlertDialog.Builder builder = null;
	private AlertDialog alert = null;

	@SuppressLint("HandlerLeak")
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		System.err.println("WifiSpeedActivity onCreate");
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		wifiOptimizeActivity = this;

		// load up the layout
		setContentView(R.layout.wifi_optimize);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		RelativeLayout extendWifiButton = (RelativeLayout) findViewById(R.id.wifi_extend_button);
		extendWifiButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (oStatus == null)
					return;
				if (oStatus.gwIsLocalAccessible) {
					if (LoginActivity.loginActivity.getWifiPermanentIf() != "") {
						/*Intent i = new Intent(WifiOptimizeActivity.this,
								WifiExtender1ScanActivity.class);
						startActivity(i);*/
					} else {
						String text = getResources().getString(
								R.string.home_no_permanent_wifi_interface);
						LoginActivity.loginActivity.showMessage(text);
						return;
					}

				} else {
					// Cancel current activity
					onBackPressed();
				}
			}
		});

		ImageView optimize_no_wifi = (ImageView) findViewById(R.id.optimize_no_wifi);
		WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		if (wifi.isWifiEnabled()) {
			optimize_no_wifi.setImageResource(R.drawable.wifi_ext);
		} else {
			optimize_no_wifi.setImageResource(R.drawable.wifi_disabled);
		}

		optimize_no_wifi.setImageResource(R.drawable.wifi_ext);

		RelativeLayout optimizeSpeedButton = (RelativeLayout) findViewById(R.id.optimize_button_layout);
		optimizeSpeedButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (oStatus == null)
					return;
				if (oStatus.gwIsLocalAccessible) {
					receiver.setWifiBSSID();
					LinearLayout optimizeWarningLayout = (LinearLayout) findViewById(R.id.optimize_warning_layout);
					optimizeWarningLayout.setVisibility(View.GONE);
					RelativeLayout optimizeStartLayout = (RelativeLayout) findViewById(R.id.optimize_layout_start);
					optimizeStartLayout.setVisibility(View.GONE);
					// create the class that monitors Wifi
					TextView progress = (TextView) findViewById(R.id.optimize_progress_status);
					progress.setText("", TextView.BufferType.NORMAL);
					currentSpeedTestProgress = 0;
					bestDataSent = 0;
					currentDataSent = 0;
					currentTimeSent = 0;
					currentSpeed = 0.0;
					oStatus.start();
					runCurrentScan = true;
				} else {
					// Cancel current activity
					onBackPressed();
				}
			}
		});
		RelativeLayout optimizeCancelButton = (RelativeLayout) findViewById(R.id.optimize_cancel_layout);
		optimizeCancelButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Stop current activity
				TextView buttonLabel = (TextView) findViewById(R.id.optimize_cancel_button_label);
				String label = buttonLabel.getText().toString();
				if (label
						.compareTo(getString(R.string.optimize_apply_settings)) == 0) {
					// save new settings first
					System.out.println("Saving new channel settings: "
							+ bestChannel);
					wifiOptimizeSetChannel = new WifiOptimizeSetChannel();
					wifiOptimizeSetChannel.start(bestChannel);
					wifiOptimizeSetChannel
							.registerUpdateScreenEvent(wifiOptimizeActivity);
					ProgressBar progressBar = (ProgressBar) findViewById(R.id.optimize_progressbar_set_channel);
					progressBar.setVisibility(View.VISIBLE);
					return;
				}
				oStatus.stopRunning();
				finish();
			}
		});
		ImageView backButton = (ImageView) findViewById(R.id.optimize_header_back_image);
		backButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View viewParam) {
				onBackPressed();
			}
		}); // end of settings.setOnclickListener
		hm = new Handler() {
			public void handleMessage(Message m) {
				// receive message
				Bundle data = m.getData();
				String dataString = data.getString("updateScreen");
				if (oStatus == null)
					return;
				if (dataString.compareTo("notConnectedLocal") == 0) {
					showDialogBoxTryToReconnect();
					noLongerConnectedLocal = true;
					new Thread(new Runnable() {
						@SuppressWarnings("static-access")
						public void run() {
							while ((LoginActivity.loginActivity.cf
									.isConnectedToServer() == false)
									|| (LoginActivity.loginActivity
											.isTryingToReConnect())
									|| (LoginActivity.loginActivity
											.isGatewayLocal() == false)) {
								// Still to figure out which device to contact
								try {
									Thread.currentThread().sleep(1000);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
							noLongerConnectedLocal = false;
							update("reconnected");
							if ((runOptimizeScan == true)
									|| (runCurrentScan == true)) {
								update("failedTransmitTest");
							}
							return;
						}
					}).start();
					return;
				}
				if (dataString.compareTo("reconnected") == 0) {
					// dialog no longer needed
					if (alert != null) {
						alert.dismiss();
						alert = null;
					}
				}
				if (dataString.compareTo("connectionDetermined") == 0) {
					// We know how the gateway is connected
					RelativeLayout optimizeSpeedButton = (RelativeLayout) findViewById(R.id.optimize_button_layout);
					TextView optimizeButtonLabel = (TextView) findViewById(R.id.optimize_optimize_label);
					@SuppressWarnings("unused")
					TextView optimizeMessage = (TextView) findViewById(R.id.optimize_performing_test);
					String label = "";
					@SuppressWarnings("unused")
					String text = "";
					if (oStatus.gwIsLocalAccessible) {
						// GW attached and local available
						text = getString(R.string.optimize_warning);
						label = getString(R.string.optimize_continue);
						System.out.println("Gateway is local available");
					} else {
						text = getString(R.string.optimize_gateway_not_local_connected);
						label = getString(R.string.optimize_cancel_button_label);
						System.out.println("Gateway is remote available");
					}
					// optimizeMessage.setText(text,
					// TextView.BufferType.NORMAL);
					optimizeButtonLabel.setText(label,
							TextView.BufferType.NORMAL);
					optimizeSpeedButton.setVisibility(View.VISIBLE);

					return;
				}
				if (dataString.compareTo("channelSet") == 0) {
					ProgressBar progressBar = (ProgressBar) findViewById(R.id.optimize_progressbar_set_channel);
					progressBar.setVisibility(View.INVISIBLE);
					TextView buttonLabel = (TextView) findViewById(R.id.optimize_cancel_button_label);
					buttonLabel.setText(
							getString(R.string.optimize_apply_settings_done),
							TextView.BufferType.NORMAL);

				}

				if (runCurrentScan) {
					if (dataString.compareTo("optimizeGraphUpdate") == 0) {
						if (noLongerConnectedLocal) {
							System.out
									.println("Optimize not connected local, skipping other events");
							return;
						}
						ProgressBar progressBar = (ProgressBar) findViewById(R.id.optimize_scanning_current_bar);
						currentSpeedTestProgress = currentSpeedTestProgress
								+ (75 / maxTestTime);
						progressBar.setProgress(currentSpeedTestProgress);

					}
					if (dataString.compareTo("failedTransmitTest") == 0) {
						if (noLongerConnectedLocal) {
							System.out
									.println("Optimize not connected local, skipping other events");
							return;
						}
						// Could not complete the test without errors, we should
						// redo it
						ProgressBar progressBar = (ProgressBar) findViewById(R.id.optimize_scanning_current_bar);
						progressBar.setProgress(0);
						progressBar.setMax(100);
						progressBar = (ProgressBar) findViewById(R.id.optimize_scanning_optimize_bar);
						progressBar.setProgress(0);
						progressBar.setMax(100);
						System.out
								.println("Optimize Restarting test to determine current speed");

						oStatus.start();
						return;
					}

					if (dataString.compareTo("speedTestStopped") == 0) {
						if (noLongerConnectedLocal) {
							System.out
									.println("Optimize not connected local, skipping other events");
							return;
						}
						// Test completed, start the next one
						ProgressBar progressBar = (ProgressBar) findViewById(R.id.optimize_scanning_current_bar);

						runCurrentScan = false;
						currentChannel = oStatus.permanentChannel;
						bestChannel = oStatus.permanentChannel;
						bestDataSent = oStatus.measuredDataFromEvents;
						currentSpeed = oStatus.measuredSpeedFromEvents;
						currentDataSent = oStatus.measuredDataFromEvents;
						currentTimeSent = oStatus.measuredTimeFromEvents;
						runOptimizeScan = true;

						int progressMax = (currentDataSent / 75) * 100;
						System.out.println("Optimize setting current max to "
								+ progressMax);
						progressBar.setMax(progressMax);
						progressBar.setProgress(currentDataSent);
						progressBar = (ProgressBar) findViewById(R.id.optimize_scanning_optimize_bar);
						progressBar.setMax(progressMax);
						progressBar.setProgress(0);
						progressBar.setSecondaryProgress(0);
						System.out.println("Optimize setting optimize max to "
								+ progressBar.getMax());

						if (currentChannel.compareTo("1") == 0) {
							oStatus.setChannelToTest("2");
						} else {
							oStatus.setChannelToTest("1");
						}
						TextView progress = (TextView) findViewById(R.id.optimize_progress_status);
						String testString = "[" + oStatus.testChannel + "/12]";
						progress.setText(testString, TextView.BufferType.NORMAL);
						oStatus.start();
					}
					return;
				}
				if (runOptimizeScan) {
					if (noLongerConnectedLocal) {
						System.out
								.println("Optimize not connected local, skipping other events");
						return;
					}
					if (dataString.compareTo("failedTransmitTest") == 0) {
						// Could not complete the test without errors, we should
						// redo it
						// ProgressBar progressBarCurrent = (ProgressBar)
						// findViewById(R.id.optimize_scanning_current_bar);
						ProgressBar progressBar = (ProgressBar) findViewById(R.id.optimize_scanning_optimize_bar);
						progressBar.setProgress(0);
						System.out
								.println("Optimize Restarting optimize test channel "
										+ oStatus.testChannel);

						oStatus.start();
						return;
					}
					if ((dataString.compareTo("optimizeGraphUpdate") == 0)
							|| (dataString.compareTo("speedTestStopped") == 0)) {

						ProgressBar progressBar = (ProgressBar) findViewById(R.id.optimize_scanning_optimize_bar);

						/*
						 * if (progressBar.getMax() <
						 * oStatus.measuredDataFromEvents) {
						 * progressBar.setMax(oStatus.measuredDataFromEvents *
						 * 100 / 75); ProgressBar progressBarCurrent =
						 * (ProgressBar)
						 * findViewById(R.id.optimize_scanning_current_bar);
						 * progressBarCurrent
						 * .setMax(oStatus.measuredDataFromEvents * 100 / 75);
						 * progressBar.setProgress(currentDataSent); }
						 */
						if (progressBar.getMax() < oStatus.measuredDataFromEvents) {
							progressBar.setProgress(progressBar.getMax());
						} else {
							progressBar
									.setProgress(oStatus.measuredDataFromEvents);
						}
						if (progressBar.getProgress() > progressBar
								.getSecondaryProgress()) {
							progressBar.setSecondaryProgress(progressBar
									.getProgress());
							System.out
									.println("Optimize setting optimize secondary to "
											+ progressBar.getProgress());
						}
					}
					if (dataString.compareTo("speedTestStopped") == 0) {
						ProgressBar progressBar = (ProgressBar) findViewById(R.id.optimize_scanning_optimize_bar);
						progressBar.setProgress(0);
						if (oStatus.measuredSpeedFromEvents > currentSpeed) {
							bestChannel = oStatus.testChannel;
							bestDataSent = oStatus.measuredDataFromEvents;
						}
						int channel = Integer.parseInt(oStatus.testChannel);
						channel += 1;

						if (Integer.parseInt(currentChannel) == channel) {
							channel += 1;
						}

						if (channel >= 13) {
							// Test completed
							TextView progress = (TextView) findViewById(R.id.optimize_progress_status);
							String testString = "Test completed";
							progress.setText(testString,
									TextView.BufferType.NORMAL);
							TextView buttonLabel = (TextView) findViewById(R.id.optimize_cancel_button_label);
							buttonLabel
									.setText(
											getString(R.string.optimize_apply_settings),
											TextView.BufferType.NORMAL);
							runOptimizeScan = false;
						} else {
							// Continue test with new channel
							String testChannel = "" + channel;
							oStatus.setChannelToTest(testChannel);
							TextView progress = (TextView) findViewById(R.id.optimize_progress_status);
							String testString = "[" + oStatus.testChannel
									+ "/12]";
							progress.setText(testString,
									TextView.BufferType.NORMAL);
							oStatus.start();
						}
					}
				}
			}
		};
		ProgressBar progressBar = (ProgressBar) findViewById(R.id.optimize_scanning_current_bar);
		progressBar.setProgress(0);
		progressBar.setMax(100);
		progressBar = (ProgressBar) findViewById(R.id.optimize_scanning_optimize_bar);
		progressBar.setProgress(0);
		progressBar.setMax(100);

		// Registers BroadcastReceiver to track network connection changes.
		IntentFilter filter = new IntentFilter(
				ConnectivityManager.CONNECTIVITY_ACTION);
		receiver = new NetworkReceiver();
		this.registerReceiver(receiver, filter);

		oStatus = new WifiOptimizeStatus(maxTestTime);
		oStatus.registerUpdateScreenEvent(wifiOptimizeActivity);
		oStatus.determineConnection();
	}

	private void showDialogBoxTryToReconnect() {
		if (alert == null) {
			System.out.println("showDialogBoxTryToConnect already visible");
		}
		@SuppressWarnings("unused")
		TextView optimizeMessage = (TextView) findViewById(R.id.optimize_performing_test);
		String text = getString(R.string.optimize_connect_wifi_to_network);
		text = text + " " + oStatus.permanentSsid;
		// optimizeMessage.setText(text, TextView.BufferType.NORMAL);

		builder = new AlertDialog.Builder(wifiOptimizeActivity);

		String title = getResources().getString(
				R.string.optimize_disconnect_dialog_title);
		builder.setTitle(title);
		builder.setMessage(text);

		// Set an EditText

		/*
		 * String cont = getResources().getString(R.string.optimize_continue);
		 * builder.setPositiveButton(cont, new DialogInterface.OnClickListener()
		 * { public void onClick(DialogInterface dialog, int whichButton) { //
		 * Now automatically reconnects from the moment the interface changes //
		 * Should also automatically dismiss the dialog box... //
		 * LoginActivity.loginActivity.reconnect(); noLongerConnectedLocal =
		 * false; oStatus.start(); alert.dismiss(); alert = null; } });
		 */

		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Canceled.
						noLongerConnectedLocal = false;
						if (alert != null) {
							alert.dismiss();
							alert = null;
						}
						finish();
					}
				});
		alert = builder.create();
		alert.setCanceledOnTouchOutside(false);
		alert.show();
	}

	public void update(String message) {
		Message msg = Message.obtain();

		Bundle bundle = new Bundle();
		bundle.putString("updateScreen", message);

		msg.setData(bundle);

		try {
			hm.sendMessage(msg);
		} catch (Exception e) {
			System.err.println("updateScreen failed to send message");
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if ((LoginActivity.loginActivity == null)
				|| (LoginActivity.loginActivity.cf == null)) {
			// Restart App in Login screen
			// move to screen handling the settings
			Intent i = getBaseContext().getPackageManager()
					.getLaunchIntentForPackage(
							getBaseContext().getPackageName());
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			finish();
			return;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		oStatus.stopRunning();
		oStatus = null;
	}

	@Override
	protected void onDestroy() {
		// Unregisters BroadcastReceiver when app is destroyed.
		if (receiver != null) {
			this.unregisterReceiver(receiver);
		}
		super.onDestroy();

		// No longer visible, destroy the class doing the updates
		if (oStatus == null)
			return;
		oStatus.stopRunning();
		oStatus = null;
	}

	public class NetworkReceiver extends BroadcastReceiver {
		@SuppressWarnings("unused")
		private String lastbssid = "";
		private String gwBSSID = "";
		@SuppressWarnings("unused")
		private boolean connected = false;

		NetworkReceiver() {

		}

		public void setWifiBSSID() {
			gwBSSID = getWifiBSSID();
		}

		public String getWifiBSSID() {
			WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			String bssid = "";
			bssid = wifiInfo.getBSSID();
			return bssid;
		}

		@Override
		public void onReceive(Context context, Intent intent) {
			if (gwBSSID.compareTo("") == 0)
				return;
			ConnectivityManager conn = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = conn.getActiveNetworkInfo();
			if (networkInfo == null) {
				return;
			}
			if (networkInfo.isConnected() == false) {
				connected = false;
				if (LoginActivity.loginActivity == null)
					return;
				if (LoginActivity.loginActivity.cf.isConnectedToServer()) {
					LoginActivity.loginActivity.cf.disconnect();
				}
				return;
			}
			connected = true;

			String currentbssid = "";
			if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
				currentbssid = getWifiBSSID();
			}

			if (currentbssid.compareTo(gwBSSID) != 0) {
				// bssid connection changed
				lastbssid = currentbssid;
				System.out
						.println("WifiOptimizeActivity: No longer connected to gateway BSSID");
				if (LoginActivity.loginActivity == null)
					return;
				if (LoginActivity.loginActivity.cf.isConnectedToServer() == true) {
					LoginActivity.loginActivity.cf.disconnect();
				}
			} else {
				if (LoginActivity.loginActivity == null)
					return;
				System.out
						.println("WifiOptimizeActivity: Reconnected to GW BSSID");
				if (LoginActivity.loginActivity.cf.isConnectedToServer() == false) {
					LoginActivity.loginActivity.reconnect();
				}
			}

		}

	}
}
