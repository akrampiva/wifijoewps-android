package com.intenocloud.wifijoe2;

import com.intenocloud.wifijoe3.LoginActivity;
import com.intenocloud.wifijoe3.R;
import com.intenocloud.wifijoe3.WifiSpeedStatus;
import com.intenocloud.wifijoe3.screenActivity;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by erik on 8/30/13.
 */
public class WifiSpeedActivity extends Activity implements screenActivity {
    private Handler hm;
    private WifiSpeedStatus wifiSpeedStatus;

    @SuppressLint("HandlerLeak")
	public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        System.err.println("WifiSpeedActivity onCreate");

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        // load up the layout
        setContentView(R.layout.wifi_speed_check);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        RelativeLayout optimizeSpeedButton = (RelativeLayout) findViewById(R.id.speed_optimize_layout);
        optimizeSpeedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent().setClass(
                        LoginActivity.loginActivity.getApplicationContext(),
                        WifiOptimizeActivity.class);
                startActivity(intent);
            }
        });
        ImageView backButton = (ImageView) findViewById(R.id.speed_header_back_image);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View viewParam) {
                onBackPressed();
            }
        }
        ); // end of settings.setOnclickListener
        hm = new Handler() {
            public void handleMessage(Message m) {
                // receive message
                Bundle data = m.getData();
                String dataString = data.getString("updateScreen");

                if (dataString.compareTo("connectionDetermined") == 0){
                    // We know how the gateway is connected
                    ImageView connectedImage = (ImageView) findViewById(R.id.speed_connection_overview_image);
                    TextView performingTest = (TextView) findViewById(R.id.speed_performing_test);
                    String text;
                    if (wifiSpeedStatus.gwJid.compareTo("") == 0) {
                        // No GW attached
                        connectedImage.setImageResource(R.drawable.sc_o_vx);
                        text= getString(R.string.speed_final_cannot_perform_speed_test);
                        System.out.println("Gateway is not connected");

                        text= getString(R.string.speed_final_tests_complete);
                        performingTest.setText(text, TextView.BufferType.NORMAL);
                    }else{
                        if (wifiSpeedStatus.gwIsLocalAccessible) {
                            // GW attached and local available
                            connectedImage.setImageResource(R.drawable.sc_h_vv);
                            text = getString(R.string.speed_performing_gateway_speed_test);
                            System.out.println("Gateway is local available");
                        }else{
                            connectedImage.setImageResource(R.drawable.sc_o_vv);
                            text= getString(R.string.speed_performing_gateway_speed_test);
                            System.out.println("Gateway is remote available");
                        }
                        // layout visible now
                        RelativeLayout gwResult = (RelativeLayout)findViewById(R.id.speed_gateway_result_layout);
                        gwResult.setVisibility(View.VISIBLE);
                        ProgressBar gwDownloadDone = (ProgressBar)findViewById(R.id.speed_gateway_download_progress_bar);
                        gwDownloadDone.setVisibility(View.VISIBLE);
                    }
                    performingTest.setText(text, TextView.BufferType.NORMAL);
                    ProgressBar statusProgress = (ProgressBar)findViewById(R.id.speed_check_progressBar);
                    statusProgress.setVisibility(View.INVISIBLE);

                    return;
                }
                //if (dataString.compareTo("internetSpeedDone") == 0){
                    // We know how the gateway is connected
                //}
                if (dataString.compareTo("gatewaySpeedReceiveDone") == 0){
                    // We know how the gateway is connected
                    // Set the speed test results
                    ProgressBar gwDownloadDone = (ProgressBar)findViewById(R.id.speed_gateway_download_progress_bar);
                    gwDownloadDone.setVisibility(View.INVISIBLE);
                    TextView downloadResult = (TextView)findViewById(R.id.speed_gateway_download);
                    String text = String.format("%s %.2f MBit/s",
                            getString(R.string.speed_download_speed),
                            wifiSpeedStatus.measuredSpeedReceiving/(1024*1024/8));
                    downloadResult.setText(text, TextView.BufferType.NORMAL);

                    RelativeLayout gwUploadResult = (RelativeLayout)findViewById(R.id.speed_gateway_upload_layout);
                    gwUploadResult.setVisibility(View.VISIBLE);
                    ProgressBar gwUploadDone = (ProgressBar)findViewById(R.id.speed_gateway_upload_progress_bar);
                    gwUploadDone.setVisibility(View.VISIBLE);

                    return;
                }
                if (dataString.compareTo("gatewaySpeedTransmitDone") == 0){
                    ProgressBar gwUploadDone = (ProgressBar)findViewById(R.id.speed_gateway_upload_progress_bar);
                    gwUploadDone.setVisibility(View.INVISIBLE);

                    TextView upResult = (TextView)findViewById(R.id.speed_gateway_upload);

                    String text = String.format("%s %.2f MBit/s",
                            getString(R.string.speed_upload_speed),
                            wifiSpeedStatus.measuredSpeedFromEvents/(1024*1024/8));

                    upResult.setText(text, TextView.BufferType.NORMAL);

                    // final test
                    TextView performingTest = (TextView) findViewById(R.id.speed_performing_test);
                    text= getString(R.string.speed_final_tests_complete);
                    performingTest.setText(text, TextView.BufferType.NORMAL);
                }

            }
        };

        wifiSpeedStatus = new WifiSpeedStatus(LoginActivity.loginActivity.cf);
        wifiSpeedStatus.registerUpdateScreenEvent(this);

    }
    public void update(String message) {
        Message msg = Message.obtain();

        Bundle bundle = new Bundle();
        bundle.putString("updateScreen", message);

        msg.setData(bundle);

        try {
            hm.sendMessage(msg);
        } catch (Exception e) {
            System.err.println("updateScreen failed to send message");
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if ((LoginActivity.loginActivity == null)
                || (LoginActivity.loginActivity.cf == null)) {
            // Restart App in Login screen
            // move to screen handling the settings
            Intent i = getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage( getBaseContext().getPackageName() );
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
            return;
        }
        if (LoginActivity.loginActivity.cf.isConnectedToServer() == false) {
            onBackPressed();
            return;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (wifiSpeedStatus != null) wifiSpeedStatus.stopRunning();
    }
}
