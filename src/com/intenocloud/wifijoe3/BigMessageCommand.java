package com.intenocloud.wifijoe3;

import java.util.ArrayList;
import java.util.List;

import com.intenocloud.core.CommandEntry;
import com.intenocloud.core.CommandHandler;
import com.intenocloud.core.ParamEntry;

/**
 * Created by erik on 3/29/14.
 */
public class BigMessageCommand extends CommandEntry {
    List<CommandEntry> cmdList = new ArrayList<CommandEntry>();

    class ReplyHandler extends CommandHandler {

        public void commandReply(String topic, CommandEntry cmd, String from) {
            System.out.println("BigMessageCommand: received reply" );
            if (cmdList.remove(cmd) == false) {
                System.out.println("BigMessageCommand: removed command" );
            }
        }

        public void commandError(String topic, CommandEntry cmd, String from, int err) {
            System.out.println("BigMessageCommand: received error" );
        }
    }
    ReplyHandler replyHandler = null;
    int messages = 1;
    int size = 0;

    public BigMessageCommand(String cmdName, String cmdDescr) {
        super(cmdName, cmdDescr);

        registerInputParam("size", ParamEntry.paramTypeString, "characters included in the message");
        registerInputParam("messages", ParamEntry.paramTypeString, "Amount of times the command is fired, default 1");

        registerReplyParam("result", ParamEntry.paramTypeString, "success of failed");

        replyHandler = new ReplyHandler();
    }
    public void execute() {
        // Called when external party wants to carry out the command

        if (isInputSet("size")) {
            try {
                size = Integer.parseInt(getInput("size"));
            }catch (NumberFormatException e){
                size = 0;
            }
        }
        if (isInputSet("messages")) {
            try {
                messages = Integer.parseInt(getInput("messages"));
            }catch (NumberFormatException e){
                messages = 1;
            }
        }
        StringBuffer outputBuffer = new StringBuffer(size);
        for (int i = 0; i < size; i++){
            outputBuffer.append("Q");
        }

        int toSend = messages;
        while(toSend > 0) {
            // Check the status and update the dialog message
            if (LoginActivity.loginActivity.cf.isConnectedToServer() == false) {
                System.err.println("No longer connected");
                break;
            }
            String gwJid = LoginActivity.loginActivity.getGatewayJid();
            if (gwJid.compareTo("") == 0) {
                System.err.println("No GW attached");
                break;
            }

            CommandEntry cmd = new CommandEntry("topicList", "");
            cmdList.add(cmd);

            // This command does not know the parameter, but we can send it anyway
            cmd.setInput("size", outputBuffer.toString());

            LoginActivity.loginActivity.cf.sendCommand("core", cmd, gwJid, replyHandler);

            toSend -= 1;
            System.out.println("Sent message to go: " + toSend);

        }

        setReply("result", "success");
    }
}
