package com.intenocloud.wifijoe3;

import android.os.Build;

import com.intenocloud.core.CommandEntry;
import com.intenocloud.core.ParamEntry;

/**
 * Created by erik on 9/16/13.
 */
public class OSCommand extends CommandEntry {
    public OSCommand(String cmdName, String cmdDescr) {
        super(cmdName, cmdDescr);

        registerInputParam("name", ParamEntry.paramTypeString, "OS name");
        registerInputParam("version", ParamEntry.paramTypeString, "OS version");
        registerInputParam("vendor", ParamEntry.paramTypeString, "platform vendor");
        registerInputParam("firmwareVersion", ParamEntry.paramTypeString, "firmware version");
        registerInputParam("hardwareVersion", ParamEntry.paramTypeString, "hardware version");

        registerReplyParam("name", ParamEntry.paramTypeString, "OS name");
        registerReplyParam("version", ParamEntry.paramTypeString, "OS version");
        registerReplyParam("vendor", ParamEntry.paramTypeString, "platform vendor");
        registerReplyParam("firmwareVersion", ParamEntry.paramTypeString, "firmware version");
        registerReplyParam("hardwareVersion", ParamEntry.paramTypeString, "hardware version");

    }
    public void execute() {
        // Called when external party wants to carry out the command
        boolean nothingIsSet = true;
        if (isInputSet("name")
                || isInputSet("version")
                || isInputSet("vendor")
                || isInputSet("firmwareVersion")
                || isInputSet("hardwareVersion")) {
            nothingIsSet = false;
        }
        if (isInputSet("name") || nothingIsSet) {
            setReply("name", "android");
        }
        if (isInputSet("version")|| nothingIsSet) {
            setReply("version", Build.VERSION.RELEASE);
        }
        if (isInputSet("vendor")|| nothingIsSet) {
            setReply("vendor", Build.MANUFACTURER);
        }
        if (isInputSet("firmwareVersion")|| nothingIsSet) {
            setReply("firmwareVersion", System.getProperty("os.version"));
        }
        if (isInputSet("hardwareVersion")|| nothingIsSet) {
            setReply("hardwareVersion", Build.PRODUCT);
        }
    }

}
