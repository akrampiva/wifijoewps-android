package com.intenocloud.wifijoe3;

import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;

import com.intenocloud.core.BlockedCommandHandler;
import com.intenocloud.core.BlockedCommandResult;
import com.intenocloud.core.CommandEntry;
import com.intenocloud.core.JNIbridge;
import com.intenocloud.core.Observable;
import com.intenocloud.core.SWIGTYPE_p_std__listT_std__string_t;

/**
 * Created by erik on 9/6/13.
 */
public class WifiOptimizeStatus implements Runnable {
    protected screenActivity notifyUpdateScreenActivityObject = null;
    public String gwJid = ""; // To be retrieved
    public String permanentSsid = "";
    public String permanentChannel = "";
    public String testChannel = "";
    public boolean stop = false;
    protected Thread runner = null;
    private int secondsToTest;
    private String wifiIf = "";
    public Boolean gwIsLocalAccessible = false;

    // Speedtest
    private Boolean lTestOngoing = false;
    public long lStart = 0;
    public Double measuredSpeedSending = 0.0;
    public Double measuredSpeedFromEvents = 0.0;
    public int measuredDataFromEvents = 0;
    public int measuredTimeFromEvents = 0;

    public WifiOptimizeStatus(int secondsToTest) {
        this.secondsToTest = secondsToTest;
        notifyUpdateScreenActivityObject = null;

    }

    public void start() {
        stop = true;
        if (runner != null) {
            try {
                runner.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            runner = null;
        }
        runner = new Thread(this, "WifiOptimizeStatus"); // (1) Create a new thread.
        System.out.println(runner.getName());
        stop = false;
        runner.start(); // (2) Start the thread.
    }
    public void setChannelToTest(String channel) {
        testChannel = channel;
    }

    @SuppressWarnings("static-access")
	public void run() {
        System.out.println("OptimizeThread: Start");
        gwJid = LoginActivity.loginActivity.getGatewayJid();
        while (((LoginActivity.loginActivity.isTryingToReConnect())
                || (gwJid.compareTo("") == 0))
                && (stop == false)) {
            // Still to figure out which device to contact
            gwJid = LoginActivity.loginActivity.getGatewayJid();
            try {
                Thread.currentThread().sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (stop) return;
        System.out.println("OptimizeThread: islocal?");
        if (LoginActivity.loginActivity.isGatewayLocal() == false) {
            // Wrongly connected, send message to the user to reconnect properly
            updateScreens("notConnectedLocal");
            return;
        }
        String lGwJid = gwJid;

        System.out.println("OptimizeThread: set Wifi channel " + testChannel);

        // set the correct testing channel
        //Retrieve the Wifi SSID, security and password
        CommandEntry cmd = new CommandEntry("wifi", "");
        cmd.setInput("interface", wifiIf);
        cmd.setInput("channel", testChannel);

        BlockedCommandHandler request = new BlockedCommandHandler();
        LoginActivity.loginActivity.cf.sendCommand("netMgr", cmd, lGwJid, request);
        BlockedCommandResult result = request.waitForResult();

        while ((stop == false) && (result.getResultType() != BlockedCommandResult.REPLY)) {
            System.out.println("WifiStatus: Did not receive reply on setting channel");
            try {
                Thread.currentThread().sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (LoginActivity.loginActivity.cf.isConnectedToServer() == false) {
                if (LoginActivity.loginActivity.isTryingToReConnect() == false) {
                    // We have stopped trying to connect, user intervention needed
                    updateScreens("notConnectedLocal");
                    return;
                }
                continue;
            }
            System.out.println("OptimizeThread: set wifi channel again " + testChannel);

            // Retry setting the wifi (should be set but can cause connection loss
            cmd.setInput("interface", wifiIf);
            cmd.setInput("channel", testChannel);

            request = new BlockedCommandHandler();
            LoginActivity.loginActivity.cf.sendCommand("netMgr", cmd, lGwJid, request);
            result = request.waitForResult();
        }
        System.out.println("OptimizeThread: get channel response");

        String channel = result.getCmd().getReply("channel");
        if (channel.compareTo(testChannel) != 0) {
            System.err.println("Channel could not be set " + channel);
        }
        // Give it a second to let the change take effect if it did not yet
        try {
            Thread.currentThread().sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("OptimizeThread: verifiy connectivity again before starting test");
        if (stop) return;
        if (LoginActivity.loginActivity.isGatewayLocal() == false) {
            while ((stop == false)
                    && (LoginActivity.loginActivity.isGatewayLocal() == false)
                    && (LoginActivity.loginActivity.isTryingToReConnect() == true)) {
                try {
                    Thread.currentThread().sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                gwJid = LoginActivity.loginActivity.getGatewayJid();
            }
            if (LoginActivity.loginActivity.isGatewayLocal() == false) {
                updateScreens("notConnectedLocal");
                return;
            }
        }
        if (stop) return;
        boolean transmitResult = transmit(gwJid, secondsToTest, 100*1024*1024);
        System.out.println("OptimizeThread: transmit final, ending thread");
        if (transmitResult == false) {
            // failed the test
            updateScreens("failedTransmitTest");
        }
    }
    public void determineConnection() {

        wifiIf = LoginActivity.loginActivity.getWifiPermanentIf();
        gwJid = LoginActivity.loginActivity.getGatewayJid();
        permanentSsid = LoginActivity.loginActivity.getPermanentSsid();
        permanentChannel = LoginActivity.loginActivity.getPermanentChannel();
        gwIsLocalAccessible = LoginActivity.loginActivity.isGatewayLocal();

        updateScreens("connectionDetermined");


        testChannel = permanentChannel;
    }
    public void registerUpdateScreenEvent(screenActivity screenActivityObject) {
        notifyUpdateScreenActivityObject = screenActivityObject;
    }
    protected void updateScreens(String msg) {
        // Run through registered Activities and give update
        if (notifyUpdateScreenActivityObject == null) return;
        System.out.println("WifiOptimizeStatus updateScreens msg: " + msg);
        notifyUpdateScreenActivityObject.update(msg);
    }
    protected Boolean transmit(String lDst, long lSeconds, long lBytes)
    {
        System.out.println("OptimizeThread: starting transmit test");

        if (lTestOngoing) {
            System.out.println("Test already ongoing, wait for it to complete");
            return false;
        }

        lTestOngoing = true;
        measuredSpeedSending = 0.0;
        measuredSpeedFromEvents = 0.0;

        int tryPort = 15353;
        while (available(tryPort) == false) {
            tryPort += 1;
        }
        System.err.println("Opening connection to 127.0.0.1:" + tryPort);
        String ipPortCombo = "127.0.0.1:" + tryPort;

        /* Create a proxy where we locally accept a socket that can connect with
         * the created proxy tunnel. We use local address 127.0.0.1:5556 for this.
         * The other side will be setup with an element that writes zeros
         * We will measure the throughput speed in the thread.
         */
        Observable pObs = new Observable();
        WifiOptimizeEventHandler wifiSpeedEventHandler = new WifiOptimizeEventHandler(this);
        SWIGTYPE_p_std__listT_std__string_t lSessionIdList = JNIbridge.getEmptyCoreStringList();
        int res = LoginActivity.loginActivity.cf.createProxies(lDst, 1, lSessionIdList, "accept", ipPortCombo,
                "writeDiscard", "", null, wifiSpeedEventHandler, pObs);
        if (res < 0) {
            System.out.println("Failed to create the needed proxy tunnel");
            lTestOngoing = false;
            return false;
        }
        Socket sock;
        OutputStream sockOutput = null;
        try {
            sock = new Socket("127.0.0.1", tryPort);
            sockOutput = sock.getOutputStream();
        }
        catch (IOException e){
            e.printStackTrace(System.err);
            LoginActivity.loginActivity.cf.closeTransferSessions(lSessionIdList);
            lTestOngoing = false;
            return false;
        }

        System.out.println("OptimizeThread: Going in write loop");

        /* Pump data for a while */
        long totalWrite = 0;
        long maxToWrite = lBytes;
        long start = System.nanoTime();
        long totalTestTime = start + lSeconds * 1000 * 1000 * 1000;
        long nano_seconds_since_start = start;
        long secondLater = start + 1 * 1000 * 1000 * 1000;
        boolean ioError = false;
        while ((nano_seconds_since_start < totalTestTime)
                && (totalWrite < maxToWrite)
                && (stop == false)) {
            byte[] buf = new byte[64*1024];

            try {
                sockOutput.write(buf, 0, buf.length);
            }
            catch (IOException e){
                e.printStackTrace(System.err);
                ioError = true;
                break;
            }

            totalWrite += buf.length;
            nano_seconds_since_start = System.nanoTime();
            if (secondLater < nano_seconds_since_start) {
                System.out.println("Progress: " + totalWrite + " bytes");
                secondLater += 1 * 1000 * 1000 * 1000;
            }
        }

        System.out.println("============Sending done============================");

       /* Calculate speed */
        nano_seconds_since_start = nano_seconds_since_start - start;
        measuredSpeedSending = ((double)totalWrite)/(nano_seconds_since_start/(1000 * 1000 * 1000));
        System.out.println("Sending: bytes per second " + measuredSpeedSending
                + " (" + measuredSpeedSending/(1024*1024) + "MB/s)");

       /* Close connection */
        try {
            sock.close();
        }
        catch (IOException e){
            System.err.println("Exception closing socket.");
            e.printStackTrace(System.err);
        }

        /* Close transfer session */
        if (LoginActivity.loginActivity.cf.isConnectedToServer()) {
            LoginActivity.loginActivity.cf.closeTransferSessions(lSessionIdList);
        }

        lTestOngoing = false;
        if (ioError == true) return false;
        return true;
    }
    public static boolean available(int port) {
        if (port < 1200 || port > 63000) {
            throw new IllegalArgumentException("Invalid start port: " + port);
        }

        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return true;
        } catch (IOException e) {
        } finally {
            if (ds != null) {
                ds.close();
            }

            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                /* should not be thrown */
                }
            }
        }

        return false;
    }
    public void stopRunning()  {
        if (stop) return;
        stop = true;
        LoginActivity.loginActivity.cf.cancelOutstandingCmds(gwJid);
        if (runner != null) {
            try {
                runner.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        runner = null;
    }

}
