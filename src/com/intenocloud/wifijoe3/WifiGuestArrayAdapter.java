package com.intenocloud.wifijoe3;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by erik on 8/27/13.
 */
public class WifiGuestArrayAdapter extends ArrayAdapter<WifiGuestAccessDevice>  {
    Context context;
    int layoutResourceId;
    ArrayList<WifiGuestAccessDevice>dataGuest = null;

    public WifiGuestArrayAdapter(Context context, int layoutResourceId, ArrayList<WifiGuestAccessDevice> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.dataGuest = data;
    }
    @Override
    public int getViewTypeCount() {
        // the amount of different views that are defined
        return 3;
    }
    @Override
    public int getItemViewType(int position) {
        // Provide a layout index for each view
        // Be careful, the views will be reused for rows with the same 'type'
        // Content should best only be changed, not view elements as they are reused over rows
        // by other row items

        WifiGuestAccessDevice device = dataGuest.get(position);
        if (device == null) return 1;
        System.out.println("WifiGuestArrayAdapter: position " + position
                + " accessR " + device.accessRights
                + " placeH " + device.placeholder);
        if (device.placeholder == true) {
            System.out.println("WifiGuestArrayAdapter: found placeH " + position);
            if (device.accessRights.compareTo("searching") == 0) {
                return 2;
            }
            return 1;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        guestAccessDeviceHolder holder = null;

        WifiGuestAccessDevice device = dataGuest.get(position);
        System.out.println("WifiGuestArrayAdapter: getView position " + position);
        if(row == null)
        {
            System.out.println("WifiGuestArrayAdapter: getView row == null");
            if (getItemViewType(position) == 2) {
                System.out.println("WifiGuestArrayAdapter: getItemViewType = 2");

                // placeholder so check which and set proper text
                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                row = inflater.inflate(R.layout.guest_access_searching_row, parent, false);
                holder = new guestAccessDeviceHolder();
                holder.accessImage = (ImageView)null;
                holder.txtTitle = (TextView)null;

                row.setTag(holder);
                return row;
            }
            if (getItemViewType(position) == 1) {
                System.out.println("WifiGuestArrayAdapter: getItemViewType = 1");

                // placeholder so check which and set proper text
                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                row = inflater.inflate(R.layout.guest_access_type_row, parent, false);
                holder = new guestAccessDeviceHolder();
                holder.accessImage = (ImageView)null;
                holder.txtTitle = (TextView)row.findViewById(R.id.guest_access_type_label);

                row.setTag(holder);
                return row;
            }
            if (getItemViewType(position) == 0) {
                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new guestAccessDeviceHolder();
                holder.accessImage = (ImageView)row.findViewById(R.id.device_active_icon);
                holder.wifiSignalImage = (ImageView)row.findViewById(R.id.wifi_strength_icon);
                holder.txtTitle = (TextView)row.findViewById(R.id.device_name);
            }

            row.setTag(holder);
        }
        else
        {
            holder = (guestAccessDeviceHolder)row.getTag();
        }
        if (device.placeholder == true) {

            // placeholder so check which and set proper text
            System.out.println("WifiGuestArrayAdapter: device accessR " + device.accessRights
                    + " on position " + position);
            if (device.accessRights.compareTo("none") == 0) {
                // WAN access only
                holder.txtTitle.setText(R.string.guest_no_access);
                System.err.println("WifiGuestArrayAdapter: Setting wan separator");
            }
            if (device.accessRights.compareTo("wan") == 0) {
                // WAN access only
                holder.txtTitle.setText(R.string.guest_internet_access);
                System.err.println("WifiGuestArrayAdapter: Setting wan separator");
            }
            if (device.accessRights.compareTo("lan") == 0) {
                // LAN access only
                holder.txtTitle.setText(R.string.guest_local_network_access);
                System.err.println("WifiGuestArrayAdapter: Setting lan separator");
            }
            if (device.accessRights.compareTo("lanwan") == 0) {
                // Full access
                holder.txtTitle.setText(R.string.guest_full_access);
                System.err.println("WifiGuestArrayAdapter: Setting full separator");
            }
            return row;
        }

        holder.txtTitle.setText(device.deviceName);
        double S= device.signalStrength;
        double N= device.noise;
        double snr = S-N;
        int icon = R.drawable.wifi_perfect;
        if(snr < 15){
            icon = R.drawable.wifi_bad;
        } else if (snr<25){
            icon = R.drawable.wifi_slow;
        } else if (snr<40){
            icon = R.drawable.wifi_ext_good;
        }
        holder.accessImage.setImageResource(icon);

        holder.wifiSignalImage.setImageResource(icon);
        if (device.connected) {
            if (device.accessRights.compareTo("none") == 0) {
                holder.accessImage.setImageResource(R.drawable.no_access);
            }
            if (device.accessRights.compareTo("wan") == 0) {
                // WAN access only
                holder.accessImage.setImageResource(R.drawable.internet_access_blue_icon);
            }
            if (device.accessRights.compareTo("lan") == 0) {
                // LAN access only
                holder.accessImage.setImageResource(R.drawable.home_access_blue_icon);
            }
            if (device.accessRights.compareTo("lanwan") == 0) {
                // Full access
                holder.accessImage.setImageResource(R.drawable.full_access_blue_icon);
            }

            holder.wifiSignalImage.setVisibility(View.VISIBLE);
        }else{
            if (device.accessRights.compareTo("none") == 0) {
                holder.accessImage.setImageResource(R.drawable.no_access);
            }
            if (device.accessRights.compareTo("wan") == 0) {
                // WAN access only
                holder.accessImage.setImageResource(R.drawable.internet_access_blue_icon);
            }
            if (device.accessRights.compareTo("lan") == 0) {
                // LAN access only
                holder.accessImage.setImageResource(R.drawable.home_access_grey_icon);
            }
            if (device.accessRights.compareTo("lanwan") == 0) {
                // Full access
                holder.accessImage.setImageResource(R.drawable.full_access_grey_icon);
            }

            holder.wifiSignalImage.setVisibility(View.INVISIBLE);
        }

        return row;
    }

    static class guestAccessDeviceHolder
    {
        ImageView accessImage;
        ImageView wifiSignalImage;
        TextView txtTitle;
    }
}
