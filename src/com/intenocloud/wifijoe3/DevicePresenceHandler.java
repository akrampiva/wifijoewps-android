package com.intenocloud.wifijoe3;

import java.util.Set;

import org.cretz.swig.collection.NativeSet;

import com.intenocloud.core.JNIbridge;
import com.intenocloud.core.SWIGTYPE_p_std__listT_std__string_t;
import com.intenocloud.core.ServicePresenceHandler;
import com.intenocloud.core.StringListIterator;
import com.intenocloud.core.StringListWrapper;

/**
 * Created by erik on 11/9/13.
 */
public class DevicePresenceHandler extends ServicePresenceHandler {
    private int stateChecked = -1;
    private String fromChecked = "";
    DevicePresenceHandler() {
    }
    @Override
    public void handlePresence(String from, int state) {
        System.out.println("DevicePresenceHandler: Presence event received from: " + from + " state " + state);
        String gwJid = getPresentGateway();
        if ((gwJid == null)
                || (from.compareTo(gwJid) == 0)) {
            LoginActivity.loginActivity.forceUpdate();
        }
        if (state == Down) {
            if ((gwJid != null) && (from.compareTo(gwJid) == 0)) {
                LoginActivity.loginActivity.forceUpdate();
            }
            return;
        }
        if ((from.compareTo(fromChecked) == 0) && (stateChecked == state)) {
            // The event is called also when additional IP-addresses are found
            // that allow you to get local access through HTTP
            System.out.println("DevicePresenceHandler: Skipping this event");
            return;
        }
        //stateChecked = -1;
        //fromChecked = from;
        if (gwJid != null) {
            new Thread(new Runnable() {
                public void run() {
                    LoginActivity.loginActivity.determineConnection();
                }
            }).start();
        }
    }
    private String getPresentGateway() {
        SWIGTYPE_p_std__listT_std__string_t accList;
        SWIGTYPE_p_std__listT_std__string_t typeList;
        SWIGTYPE_p_std__listT_std__string_t localList;
        Set<String> set;
        String[] listAccount;
        String[] listType;
        String[] listLocal;
        String attachedServer = LoginActivity.loginActivity.server;

        if (LoginActivity.loginActivity.cf.isConnectedToServer() == false) {
            return null;
        }
        accList = JNIbridge.getEmptyCoreStringList();
        typeList = JNIbridge.getEmptyCoreStringList();
        localList = JNIbridge.getEmptyCoreStringList();

        LoginActivity.loginActivity.cf.getPresenceList(accList, typeList, localList);
        set = new NativeSet<String>(String.class, StringListIterator.class,
                accList, StringListWrapper.class);
        listAccount = set.toArray(new String[0]);
        set = new NativeSet<String>(String.class, StringListIterator.class,
                typeList, StringListWrapper.class);
        listType = set.toArray(new String[0]);
        set = new NativeSet<String>(String.class, StringListIterator.class,
                localList, StringListWrapper.class);
        listLocal = set.toArray(new String[0]);

        int j = 0;
        int gwCount = 0;
        while (j < listAccount.length) {
            System.out.println("Account " + listAccount[j] + "  type: " + listType[j] + " local: " + listLocal[j]);
            if ((listAccount[j].contains(attachedServer)) && (listType[j].equals("gateway"))) {
                System.out.println("LoginActivity: found gateway jid: " + listAccount[j]);
                gwCount++;
                break;
            }
            j++;
        }
        if (gwCount == 0) {
            //stateChecked = -1;
            return null;
        }
        return listAccount[j];
    }

}
