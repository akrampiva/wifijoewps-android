package com.intenocloud.wifijoe3.wps;

import java.util.Set;

import org.cretz.swig.collection.NativeSet;

import android.util.Log;

import com.intenocloud.core.BlockedCommandHandler;
import com.intenocloud.core.BlockedCommandResult;
import com.intenocloud.core.CloudFriends;
import com.intenocloud.core.CommandEntry;
import com.intenocloud.core.JNIbridge;
import com.intenocloud.core.SWIGTYPE_p_std__listT_std__string_t;
import com.intenocloud.core.StringListIterator;
import com.intenocloud.core.StringListWrapper;
import com.intenocloud.wifijoe3.LoginActivity;
import com.intenocloud.wifijoe3.screenActivity;

/**
 * Created by erik on 9/17/13.
 */
public class WPSStatus implements Runnable {

	protected screenActivity notifyUpdateScreenActivityObject = null;
	private CloudFriends cf;
	protected Thread runner;
	private String dstJid = "";
	private WPSStatusUpdateEvent cameraStatusUpdateEvent;

	public WPSStatus(screenActivity notifyUpdateScreenActivityObject) {
		cf = LoginActivity.loginActivity.cf;
		this.notifyUpdateScreenActivityObject = notifyUpdateScreenActivityObject;
		runner = new Thread(this, "WPSStatus");
		runner.start();
	}

	public void run() {
		CommandEntry cmd;
		BlockedCommandHandler request = new BlockedCommandHandler();
		BlockedCommandResult result;

		cmd = new CommandEntry("wps", "");
		cmd.setInput("activate", "");
		cmd.setPublicObservable();
		dstJid = LoginActivity.loginActivity.getGatewayJid();
		cameraStatusUpdateEvent = new WPSStatusUpdateEvent(this);
		cf.sendCommand("netMgr", cmd, dstJid, request, cameraStatusUpdateEvent);
		result = request.waitForResult();
		if (result.getResultType() != BlockedCommandResult.REPLY) {
			System.err
					.println("CameraOverviewActivity: Did not receive reply from cameramanager");
		}
		String[] reply = getArrayReply(result, "result");
		if (reply != null && reply.length > 0)
			Log.i("wps", "result " + reply[0]);
		else
			Log.i("wps", "result null");

	}

	public String[] getArrayReply(BlockedCommandResult result, String parameter) {
		SWIGTYPE_p_std__listT_std__string_t listReply = JNIbridge
				.getEmptyCoreStringList();
		result.getCmd().getReply(parameter, listReply);
		Set<String> replySet = new NativeSet<String>(String.class,
				StringListIterator.class, listReply, StringListWrapper.class);
		String[] reply = replySet.toArray(new String[0]);
		return (reply);
	}

	public void forceUpdate(String status, String mac) {
		// TODO Auto-generated method stub
		if (this.notifyUpdateScreenActivityObject != null) {
			if (mac != null) {
				this.notifyUpdateScreenActivityObject.update("MAC:" + mac);
				return;
			}
			if (status != null) {
				this.notifyUpdateScreenActivityObject.update(status);
				return;
			}
		}

	}
	
	public void stopRunning()  {
		if(cameraStatusUpdateEvent!=null)
		{
			cameraStatusUpdateEvent.unsubscribe();
			cameraStatusUpdateEvent.delete();
		}
        if (dstJid != "") {
            LoginActivity.loginActivity.cf.cancelOutstandingCmds(dstJid);
        }
        try {
            runner.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
