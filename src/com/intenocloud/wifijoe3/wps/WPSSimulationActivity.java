package com.intenocloud.wifijoe3.wps;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.intenocloud.wifijoe3.R;
import com.intenocloud.wifijoe3.screenActivity;

public class WPSSimulationActivity extends Activity implements screenActivity {

	private ProgressBar searchBar;
	private TextView infoText;
	private WPSStatus wpsStatus;
	private boolean wpsActivated;
	private ImageView resultImageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.wifi_wps);
		ImageView backButton = (ImageView) findViewById(R.id.wps_header_back_image);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View viewParam) {
                onBackPressed();
            }
        }
        ); 
		searchBar = (ProgressBar) findViewById(R.id.wps_search_bar);
		infoText = (TextView) findViewById(R.id.wps_info_text);
		resultImageView = (ImageView) findViewById(R.id.resultImageView);
		// launch WPS
		wpsActivated = false;
		activateWPS();

		// init
		searchBar.setVisibility(View.VISIBLE);
		resultImageView.setVisibility(View.GONE);
		infoText.setText(WPSSimulationActivity.this.getResources().getString(
				R.string.wps_activation));

	}

	public void activateWPS() {

		if (!wpsActivated || wpsStatus == null) {
			wpsStatus = new WPSStatus(this);
		}

	}

	public void removeWPS() {
		// TODO Auto-generated method stub
		if (wpsStatus != null) {
			wpsStatus.stopRunning();
			wpsStatus = null;
		}
		wpsActivated = false;
	}

	@Override
	public void update(final String message) {
		// TODO Auto-generated method stub
		WPSSimulationActivity.this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (message != null) {
					if (message.equals("active")) {
						wpsActivated = true;
						infoText.setText(WPSSimulationActivity.this
								.getResources().getString(
										R.string.Please_press_the_WPS_button));
						searchBar.setVisibility(View.VISIBLE);
						resultImageView.setVisibility(View.GONE);
					} else if (wpsActivated && message.equals("timeout")) {

						removeWPS();
						infoText.setText(WPSSimulationActivity.this
								.getResources().getString(
										R.string.Failed_to_connect_your_device));
						searchBar.setVisibility(View.GONE);
						resultImageView.setVisibility(View.VISIBLE);
						resultImageView.setImageResource(R.drawable.warning);

					} else if (wpsActivated && message.contains("MAC")) {
						removeWPS();
						infoText.setText(WPSSimulationActivity.this
								.getResources()
								.getString(
										R.string.Success_a_device_was_now_connected));
						searchBar.setVisibility(View.GONE);
						resultImageView.setVisibility(View.VISIBLE);
						resultImageView.setImageResource(R.drawable.check);

					}
				}

			}
		});

	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		removeWPS();
	}

}
