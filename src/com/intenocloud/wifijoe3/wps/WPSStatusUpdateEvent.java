package com.intenocloud.wifijoe3.wps;

import com.intenocloud.core.CommandEventHandler;

/**
 * Created by erik on 3/25/14.
 */
public class WPSStatusUpdateEvent extends CommandEventHandler {
	WPSStatus wpsStatus;
	String status = null;
	String mac = null;

	public WPSStatusUpdateEvent(WPSStatus wpsStatus) {
		super();
		this.wpsStatus = wpsStatus;
	}

	@Override
	public void commandEvent(String eventSubject, String value) {
		if (value == null)
			return;
		if (eventSubject == null)
			return;
		System.out.println("WPSStatusUpdateEvent event: " + eventSubject
				+ " value " + value);
		if (eventSubject.contains("status")) {
			status = value;
		}
		if (eventSubject.contains("mac")) {
			mac = value;
		}
		if (wpsStatus != null) {
			wpsStatus.forceUpdate(status, mac);
		}
		return;

	}
}
