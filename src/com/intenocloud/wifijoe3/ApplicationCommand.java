package com.intenocloud.wifijoe3;

import com.intenocloud.core.CommandEntry;
import com.intenocloud.core.ParamEntry;

/**
 * Created by erik on 9/16/13.
 */
public class ApplicationCommand extends CommandEntry {
    public ApplicationCommand(String cmdName, String cmdDescr) {
        super(cmdName, cmdDescr);
        registerReplyParam("name", ParamEntry.paramTypeString, "Provides the name of the application");
        registerReplyParam("version", ParamEntry.paramTypeString, "Provides the version of the application");
    }
    public void execute() {
        // Called when external party wants to carry out the command
        setReply("name", LoginActivity.loginActivity.applicationName);
        setReply("version", LoginActivity.loginActivity.versionName);
    }
}
