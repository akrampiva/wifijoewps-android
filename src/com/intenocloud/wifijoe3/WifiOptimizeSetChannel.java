package com.intenocloud.wifijoe3;

import com.intenocloud.core.BlockedCommandHandler;
import com.intenocloud.core.BlockedCommandResult;
import com.intenocloud.core.CommandEntry;

/**
 * Created by erik on 9/13/13.
 */
public class WifiOptimizeSetChannel implements Runnable {
    private String channel = "";
    private Thread runner;
    protected screenActivity notifyUpdateScreenActivityObject = null;

    public WifiOptimizeSetChannel() {
        runner = new Thread(this, "WifiOptimizeSetChannel"); // (1) Create a new thread.
        System.out.println(runner.getName());
    }
    public void start(String channel) {
        this.channel = channel;
        runner.start();
    }
    @SuppressWarnings("static-access")
	public void run() {
        String gwJid = LoginActivity.loginActivity.getGatewayJid();
        while ((gwJid.compareTo("") == 0)) {

            try {
                Thread.currentThread().sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            gwJid = LoginActivity.loginActivity.getGatewayJid();

        }
        CommandEntry cmd = new CommandEntry("wifi", "");
        cmd.setInput("interface", LoginActivity.loginActivity.getWifiPermanentIf());
        cmd.setInput("channel", channel);

        BlockedCommandHandler request = new BlockedCommandHandler();
        LoginActivity.loginActivity.cf.sendCommand("netMgr", cmd, gwJid, request);
        BlockedCommandResult result = request.waitForResult();

        if (result.getResultType() != BlockedCommandResult.REPLY) {
            System.err.println("WifiOptimize setChannel: Did not receive reply");
        }

        while (((result.getResultType() != BlockedCommandResult.REPLY)
                        || (LoginActivity.loginActivity.getGatewayJid().compareTo("") == 0))) {
            System.out.println("WifiStatus: Did not receive reply on setting channel");
            try {
                Thread.currentThread().sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            gwJid = LoginActivity.loginActivity.getGatewayJid();
            if (gwJid.compareTo("") == 0) continue;
            // Retry setting the wifi (should be set but can cause connection loss
            cmd.setInput("interface", LoginActivity.loginActivity.getWifiPermanentIf());
            cmd.setInput("channel", channel);

            request = new BlockedCommandHandler();
            LoginActivity.loginActivity.cf.sendCommand("netMgr", cmd, gwJid, request);
            result = request.waitForResult();
        }
        if (notifyUpdateScreenActivityObject != null) {
            updateScreens("channelSet");
        }
    }
    public void registerUpdateScreenEvent(screenActivity screenActivityObject) {
        notifyUpdateScreenActivityObject = screenActivityObject;
    }
    protected void updateScreens(String msg) {
        // Run through registered Activities and give update
        if (notifyUpdateScreenActivityObject == null) return;
        notifyUpdateScreenActivityObject.update(msg);
    }

}