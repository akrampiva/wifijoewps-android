package com.intenocloud.wifijoe3;

/**
 * Created by erik on 8/27/13.
 */
public class WifiGuestAccessDevice extends WifiPermanentAccessDevice {
    public String accessRights;
    public boolean placeholder;
    public String ipAddr;

    public WifiGuestAccessDevice() {

    }

    public WifiGuestAccessDevice(
            boolean connected, String deviceName, int signalStrength, int noise,
            String accessRights, boolean placeholder, String ipAddr
    ) {
        this.connected = connected;
        this.deviceName = deviceName;
        this.signalStrength = signalStrength;
        this.noise = noise;
        this.accessRights = accessRights;
        this.placeholder = placeholder;
        this.ipAddr = ipAddr;
    }
}
