package com.intenocloud.wifijoe3;

/**
 * Created by erik on 8/26/13.
 */
public class WifiPermanentAccessDevice {
    public boolean connected;
    public String deviceName;
    public int signalStrength;
    public int noise;
    public String ipAddr;
    public String macAdress;

    public WifiPermanentAccessDevice() {
    }
    public WifiPermanentAccessDevice(boolean connected, String deviceName, int signalStrength, int noise, String ipAddr,String macAdress) {
        this.connected = connected;
        this.deviceName = deviceName;
        this.signalStrength = signalStrength;
        this.noise = noise;
        this.ipAddr = ipAddr;
        this.macAdress = macAdress;
    }
}
