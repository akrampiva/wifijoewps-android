package com.intenocloud.wifijoe3;

import com.intenocloud.core.CommandEntry;
import com.intenocloud.core.ParamEntry;

/**
 * Created by erik on 3/29/14.
 */
public class StressCommunicationCommand extends CommandEntry {
    int threads = 1;
    int messages = 1;
    int delay = 0; // delay in ms

    public StressCommunicationCommand(String cmdName, String cmdDescr) {
        super(cmdName, cmdDescr);

        registerInputParam("threads", ParamEntry.paramTypeString, "Amount of threads to start, default 1");
        registerInputParam("messages", ParamEntry.paramTypeString, "Amount of times the command is fired, default 1");
        registerInputParam("delay", ParamEntry.paramTypeString, "delay in ms between messages, 0 default");

        registerReplyParam("result", ParamEntry.paramTypeString, "success of failed");

    }
    public void execute() {
        // Called when external party wants to carry out the command

        if (isInputSet("threads")) {
            try {
                threads = Integer.parseInt(getInput("threads"));
            }catch (NumberFormatException e){
                threads = 1;
            }
        }
        if (isInputSet("messages")) {
            try {
                messages = Integer.parseInt(getInput("messages"));
            }catch (NumberFormatException e){
                messages = 1;
            }
        }
        if (isInputSet("delay")) {
            try {
                delay = Integer.parseInt(getInput("delay"));
            }catch (NumberFormatException e){
                delay = 0;
            }
        }

        // Got input, now lets launch threads to request data
        while (threads > 0) {
            new Thread(new Runnable() {
                int toSend = messages;
                @SuppressWarnings("static-access")
				public void run() {
                    System.out.println("Thread started " + Thread.currentThread().getId());
                    while(toSend > 0) {
                        try {
                            if (delay > 0) {
                                Thread.currentThread().sleep(delay);
                            }
                        } catch (InterruptedException e) {
                            break;
                        }
                        // Check the status and update the dialog message
                        if (LoginActivity.loginActivity.cf.isConnectedToServer() == false) {
                            System.err.println("No longer connected");
                            break;
                        }
                        String gwJid = LoginActivity.loginActivity.getGatewayJid();
                        if (gwJid.compareTo("") == 0) {
                            System.err.println("No GW attached");
                            break;
                        }
                        LoginActivity.loginActivity.cf.getRemoteTopicList(gwJid);
                        toSend -= 1;
                    }
                    System.out.println("Thread stopped " + Thread.currentThread().getId());
                }
            }).start();
            threads -= 1;
        }

        setReply("result", "success");
    }
}
