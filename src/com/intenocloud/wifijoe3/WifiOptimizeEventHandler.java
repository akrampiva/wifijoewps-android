package com.intenocloud.wifijoe3;

import com.intenocloud.core.CommandEventHandler;

/**
 * Created by erik on 9/6/13.
 */
public class WifiOptimizeEventHandler  extends CommandEventHandler {
    WifiOptimizeStatus wStatus;

    public WifiOptimizeEventHandler(WifiOptimizeStatus wStatus) {
        super();

        this.wStatus = wStatus;
    }

    @Override
    public void commandEvent(String eventSubject, String value) {
        if (value == null) return;
        if (eventSubject == null) return;
        if (eventSubject.contains("Status")) {
            if (value.compareTo("closed") == 0) {
                // Done testing, stream is closed
                wStatus.updateScreens("speedTestStopped");
            }
        }
        if (eventSubject.contains("Tx")) {
            System.out.println("WifiSpeedEventHandler Event: "+ eventSubject + " value " + value);
            if (value.compareTo("0") == 0) {
                /* Set the timer to this moment in time
                 * We want to measure the transfer speed, not the transfer time
                 * plus the setup time.
                 * The setup time is rather large per session
                 */
                System.out.println("Initializing timer to calculate transfer speed");
                wStatus.lStart = System.nanoTime();
                wStatus.measuredDataFromEvents = 0;
                wStatus.measuredTimeFromEvents = 0;

                return;
            }
            if (value.compareTo("") != 0) {
                long seconds_since_start = (System.nanoTime() - wStatus.lStart)/(1000 * 1000 * 1000);
                long totalBytes = Integer.parseInt(value);
                if (seconds_since_start == 0) seconds_since_start = 1;
                wStatus.measuredSpeedFromEvents = (double)(totalBytes/(seconds_since_start));
                wStatus.measuredDataFromEvents = (int)totalBytes;
                wStatus.measuredTimeFromEvents = (int)seconds_since_start;
                System.out.println("+++Event speed: bytes per second " + wStatus.measuredSpeedFromEvents
                        + " (" + wStatus.measuredSpeedFromEvents/(1024*1024) + "MB/s)");
                wStatus.updateScreens("optimizeGraphUpdate");
            }
            return;
        }
    }
}