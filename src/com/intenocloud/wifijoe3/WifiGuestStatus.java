package com.intenocloud.wifijoe3;

import com.intenocloud.core.BlockedCommandHandler;
import com.intenocloud.core.BlockedCommandResult;
import com.intenocloud.core.CloudFriends;
import com.intenocloud.core.CommandEntry;

/**
 * Created by erik on 8/27/13.
 */
public class WifiGuestStatus extends WifiStatus {
    private boolean forceUpdate;
    private boolean NO_HEADER_FOR_ACCESS = true;

    private WifiGuestAccessDevice wifiGuestAccessDevice[] = {};

    public WifiGuestStatus(CloudFriends cfInstance,int timeInterval) {
        super(cfInstance,timeInterval);
        this.timeInterval = timeInterval;
    }

    @SuppressWarnings("static-access")
	@Override
    public void run() {
        WifiGuestAccessDevice tmpWifiGuestAccessDevice[] = {};
        WifiGuestStatusEventHandler wifiGuestMgrEventHandler = null;
        CommandEntry cmd;
        BlockedCommandHandler request;
        BlockedCommandResult result;
        int noise;
        String[] leasesMac = null;
        String[] leasesIpAddr = null;
        String[] leasesName = null;
        String[] wifiGuestIfMac = null; // Direct query of the interface to fetch IP-addr and signalstrength
        String[] wifiGuestIfIpAddr = null;
        String[] wifiGuestIfName = null;
        String[] wifiGuestIfSignalStrength = null;
        String[] wifiGuestMgrMac = null;
        String[] wifiGuestMgrAccess = null;
        String[] arpTableIpAddr = null;

        //Display info about this particular thread
        System.out.println(Thread.currentThread());

        stop = false;

        // Create temporary array, showing the placeholder for 'searching'

        if (NO_HEADER_FOR_ACCESS) {
            wifiGuestAccessDevice = new WifiGuestAccessDevice[1];
            wifiGuestAccessDevice[0] = new WifiGuestAccessDevice(false, "", 0, 0, "searching", true, "");
        }else{
            wifiGuestAccessDevice = new WifiGuestAccessDevice[2];
            wifiGuestAccessDevice[0] = new WifiGuestAccessDevice(false, "", 0, 0, "none", true, "");
            wifiGuestAccessDevice[1] = new WifiGuestAccessDevice(false, "", 0, 0, "searching", true, "");
        }

        init();


        if (wifiGuestState.compareTo("true") != 0) {
            LoginActivity.loginActivity.setGuestState("true");
            // Not enabled, so enable the interface
            // Activate the Guest interface
            //cmd = new CommandEntry("create", "");

            //request = new BlockedCommandHandler();
            //cf.sendCommand("guestMgr", cmd, gwJid, request);
            //result = request.waitForResult();
            //if (result.getResultType() != BlockedCommandResult.REPLY) {
            //    System.err.println("WifiGuestStatus runner: Did not receive reply on create cmd");
            //}
        }


        wifiStatusUpToDate = true;


        updateScreens(); // Force update already
        request = new BlockedCommandHandler();

        while (stop == false) {
            try {
                wifiGuestIfMac = new String[] {};
                wifiGuestIfSignalStrength = new String[] {};
                noise = 0;
                forceUpdate = false;


                // Fetch ARP table
                cmd = new CommandEntry("arpTable", "");
                cf.sendCommand("netMgr", cmd, gwJid, request);
                result = request.waitForResult();
                if (result.getResultType() != BlockedCommandResult.REPLY) {
                    System.err.println("WifiStatus runner: Did not receive reply on arpTable");
                    Thread.currentThread().sleep(10000);
                    continue;
                }
                arpTableIpAddr = getArrayReply(result,"ipAddr");

                // Fetch wifi associated devices information
                cmd = new CommandEntry("status", "");
                cmd.setInput("connected", "");
                cmd.setInput("access", "");


// Not keeping the object
// Seems to unsubscribe when re-entering the loop. Resubscribe seems best for now
//
                if (wifiGuestMgrEventHandler == null) {
                    wifiGuestMgrEventHandler = new WifiGuestStatusEventHandler(this);
                    cmd.setPublicObservable();
                    cf.sendCommand("guestMgr", cmd, gwJid, request, wifiGuestMgrEventHandler);
                }else{
                    cf.sendCommand("guestMgr", cmd, gwJid, request);
                }
                result = request.waitForResult();
                if (result.getResultType() != BlockedCommandResult.REPLY) {
                    System.err.println("WifiGuestStatus runner: Did not receive reply on status cmd");
                    Thread.currentThread().sleep(10000);
                    continue;
                }
                wifiGuestMgrMac = getArrayReply(result, "connected");
                wifiGuestSsid = result.getCmd().getReply("ssid");
                wifiGuestState = result.getCmd().getReply("state");
                wifiGuestMgrAccess = getArrayReply(result, "access");

                if (wifiGuestState.compareTo("true") == 0) {
                    // Fetch wifi associated devices information
                    cmd = new CommandEntry("wifi", "");
                    cmd.setInput("interface", wifiGuestIf);
                    cmd.setInput("noise", "");
                    cmd.setInput("signalStrength", "");
                    //cmd.setInput("assocTime", ""); maybe to support later
                    cmd.setInput("macAddress", "");

                    cf.sendCommand("netMgr", cmd, gwJid, request);
                    result = request.waitForResult();
                    if (result.getResultType() != BlockedCommandResult.REPLY) {
                        System.err.println("WifiStatus runner: Did not receive reply on wifi cmd");
                        Thread.currentThread().sleep(10000);
                        continue;
                    }
                    wifiGuestIfMac = getArrayReply(result, "macAddress");
                    wifiGuestIfSignalStrength = getArrayReply(result, "signalStrength");
                    noise = Integer.parseInt(result.getCmd().getReply("noise"));
                }else{
                    // Create temporary array, showing the placeholder for 'searching'
                    wifiGuestAccessDevice = new WifiGuestAccessDevice[2];
                    wifiGuestAccessDevice[0] = new WifiGuestAccessDevice(false, "", 0, 0, "none", true, "");
                    wifiGuestAccessDevice[1] = new WifiGuestAccessDevice(false, "", 0, 0, "searching", true, "");
                    updateScreens();
                    Thread.currentThread().sleep(10000);
                    continue;
                }

                // Fetch leases information
                cmd = new CommandEntry("leases", "");
                cf.sendCommand("netMgr", cmd, gwJid, request);
                result = request.waitForResult();
                if (result.getResultType() != BlockedCommandResult.REPLY) {
                    System.err.println("WifiStatus runner: Did not receive reply on leases");
                    Thread.currentThread().sleep(10000);
                    continue;
                }
                leasesMac = getArrayReply(result, "mac");
                leasesIpAddr = getArrayReply(result, "ipAddr");
                leasesName = getArrayReply(result, "name");


                // Got all needed information to update the list
                // Arping scanning of each device is also still needed
                // Filter out devices that are not connected to the permanent wifi to create the final list to display
                wifiGuestIfIpAddr = new String[wifiGuestMgrMac.length];
                wifiGuestIfName = new String[wifiGuestMgrMac.length];
                tmpWifiGuestAccessDevice = new WifiGuestAccessDevice[wifiGuestMgrMac.length];
                int wanOnly = 0;
                int lanOnly = 0;
                int lanWan = 0;
                int noAccess = 0;
                boolean matchedAll = true;
                for (int j = 0; j< wifiGuestMgrMac.length; j++) {
                    boolean foundMatch = false;
                    wifiGuestIfIpAddr[j] = "";
                    wifiGuestIfName[j] = "";
                    tmpWifiGuestAccessDevice[j] = new WifiGuestAccessDevice();
                    System.out.println("WifiGuestStatus runner: Created WifiGuestAccessDevice arr size: " + wifiGuestMgrMac.length);
                    for (int i = 0; i< leasesMac.length; i++) {
                        if (leasesMac[i].compareTo(wifiGuestIfMac[j]) == 0) {
                            // We have a match, update name and IP-addr information
                            tmpWifiGuestAccessDevice[j].deviceName = leasesName[i];
                            if (leasesName[i].equals("*")) {
                                tmpWifiGuestAccessDevice[j].deviceName = leasesIpAddr[i];
                            }
                            tmpWifiGuestAccessDevice[j].connected = false;
                            tmpWifiGuestAccessDevice[j].signalStrength = Integer.parseInt(wifiGuestIfSignalStrength[j]);
                            tmpWifiGuestAccessDevice[j].noise = noise;
                            tmpWifiGuestAccessDevice[j].accessRights = wifiGuestMgrAccess[j];
                            tmpWifiGuestAccessDevice[j].ipAddr = leasesIpAddr[i];
                            wifiGuestIfIpAddr[j] = leasesIpAddr[i];
                            wifiGuestIfName[j] = leasesName[i];
                            if (tmpWifiGuestAccessDevice[j].accessRights.compareTo("wan") == 0) {
                                // WAN access only
                                wanOnly++;
                            }
                            if (tmpWifiGuestAccessDevice[j].accessRights.compareTo("lan") == 0) {
                                // LAN access only
                                lanOnly++;
                            }
                            if (tmpWifiGuestAccessDevice[j].accessRights.compareTo("lanwan") == 0) {
                                // Full access
                                lanWan++;
                            }
                            if (tmpWifiGuestAccessDevice[j].accessRights.compareTo("none") == 0) {
                                // No access
                                noAccess++;
                            }

                            tmpWifiGuestAccessDevice[j].placeholder = false;
                            foundMatch = true;
                            break;
                        }
                    }
                    if (foundMatch == false) {
                        // Did not find a match, the array is not complete, will need to retry
                        matchedAll = false;
                        System.err.println("WifiStatus runner: Did not receive reply on leases");
                        Thread.currentThread().sleep(10000);
                        break;
                    }
                }
                if (matchedAll == false) {
                    // Did not find a match, the array is not complete, will need to retry
                    System.err.println("WifiStatus runner: Did not find full match with leases, retry");
                    Thread.currentThread().sleep(3000);
                    continue;
                }
                for (int j = 0; j< wifiGuestIfIpAddr.length; j++) {
                    for (int i = 0; i< arpTableIpAddr.length; i++) {
                        if (arpTableIpAddr[i].compareTo(wifiGuestIfIpAddr[j]) == 0) {
                            // Found a match, device is alive
                            tmpWifiGuestAccessDevice[j].connected = true;
                        }
                    }
                }
                // Still to insert placeholders for the titles 'no access', 'internet', ...
                // Group all access types
                int placeholdersToAdd = 2; // for none and 'searching'
                if (NO_HEADER_FOR_ACCESS) {
                    placeholdersToAdd = 1; // for 'searching' only
                }
                if (wanOnly > 0) placeholdersToAdd++;
                if (lanOnly > 0) placeholdersToAdd++;
                if (lanWan > 0) placeholdersToAdd++;
                // Create final array, including space for the placeholders
                wifiGuestAccessDevice = new WifiGuestAccessDevice[wifiGuestMgrMac.length + placeholdersToAdd];
                System.out.println("WifiGuestStatus runner: Elements to add " + (wifiGuestMgrMac.length + placeholdersToAdd));

                if (NO_HEADER_FOR_ACCESS) {
                    wifiGuestAccessDevice[0] = new WifiGuestAccessDevice(false, "", 0, 0, "searching", true, "");
                    System.out.println("WifiGuestStatus runner: Add element " + (0)
                            + " from " + 0
                            + " connected " + false
                            + " name " + ""
                            + " accessR " + "none"
                            + " placeH " + true);
                }else{
                    wifiGuestAccessDevice[0] = new WifiGuestAccessDevice(false, "", 0, 0, "none", true, "");
                    System.out.println("WifiGuestStatus runner: Add element " + (0)
                            + " from " + 0
                            + " connected " + false
                            + " name " + ""
                            + " accessR " + "none"
                            + " placeH " + true);
                    wifiGuestAccessDevice[1] = new WifiGuestAccessDevice(false, "", 0, 0, "searching", true, "");
                    System.out.println("WifiGuestStatus runner: Add element " + (1)
                            + " from " + 0
                            + " connected " + false
                            + " name " + ""
                            + " accessR " + "none"
                            + " placeH " + true);

                }

                int noAccessAdded = 2 + noAccess;
                if (NO_HEADER_FOR_ACCESS) {
                    noAccessAdded = 1 + noAccess;
                }
                int lanWanAdded = 0;
                if (lanWan > 0) {
                    wifiGuestAccessDevice[noAccessAdded] = new WifiGuestAccessDevice(false, "", 0, 0, "lanwan", true, "");
                    lanWanAdded = 1 + lanWan;
                    System.out.println("WifiGuestStatus runner: Add element " + (noAccessAdded)
                            + " from " + 0
                            + " connected " + false
                            + " name " + ""
                            + " accessR " + "lanwan"
                            + " placeH " + true);
                }
                int lanOnlyAdded = 0;
                if (lanOnly > 0) {
                    wifiGuestAccessDevice[noAccessAdded + lanWanAdded] = new WifiGuestAccessDevice(false, "", 0, 0, "lan", true, "");
                    lanOnlyAdded = 1 + lanOnly;
                    System.out.println("WifiGuestStatus runner: Add element " + (noAccessAdded + lanWanAdded)
                            + " from " + 0
                            + " connected " + false
                            + " name " + ""
                            + " accessR " + "lan"
                            + " placeH " + true);
                }
                @SuppressWarnings("unused")
				int wanOnlyAdded = 0;
                if (wanOnly > 0) {
                    wifiGuestAccessDevice[noAccessAdded + lanWanAdded + lanOnlyAdded] = new WifiGuestAccessDevice(false, "", 0, 0, "wan", true, "");
                    wanOnlyAdded = 1 + wanOnly;
                    System.out.println("WifiGuestStatus runner: Add element " + (noAccessAdded + lanWanAdded + lanOnlyAdded)
                            + " from " + 0
                            + " connected " + false
                            + " name " + ""
                            + " accessR " + "wan"
                            + " placeH " + true);
                }


                // Add all found devices without access
                int noAccesIx = 2;
                if (NO_HEADER_FOR_ACCESS) {
                    noAccesIx = 1;
                }

                int lanWanIx = 1 + noAccessAdded;
                int lanOnlyIx = 1 + noAccessAdded + lanWanAdded;
                int wanOnlyIx = 1 + noAccessAdded + lanWanAdded + lanOnlyAdded;

                for (int j = 0; j< tmpWifiGuestAccessDevice.length; j++) {
                    int index = 0;
                    if (tmpWifiGuestAccessDevice[j].accessRights.compareTo("none") == 0) {
                        index = noAccesIx++;
                    }
                    if (tmpWifiGuestAccessDevice[j].accessRights.compareTo("lanwan") == 0) {
                        index = lanWanIx++;
                    }
                    if (tmpWifiGuestAccessDevice[j].accessRights.compareTo("lan") == 0) {
                        index = lanOnlyIx++;
                    }
                    if (tmpWifiGuestAccessDevice[j].accessRights.compareTo("wan") == 0) {
                        index = wanOnlyIx++;
                    }
                    System.out.println("WifiGuestStatus runner: Add element " + index
                                        + " from " + j
                                        + " connected " + tmpWifiGuestAccessDevice[j].connected
                                        + " name " + tmpWifiGuestAccessDevice[j].deviceName
                                        + " accessR " + tmpWifiGuestAccessDevice[j].accessRights
                                        + " placeH " + tmpWifiGuestAccessDevice[j].placeholder);
                    wifiGuestAccessDevice[index] = new WifiGuestAccessDevice(
                            tmpWifiGuestAccessDevice[j].connected,
                            tmpWifiGuestAccessDevice[j].deviceName,
                            tmpWifiGuestAccessDevice[j].signalStrength,
                            tmpWifiGuestAccessDevice[j].noise,
                            tmpWifiGuestAccessDevice[j].accessRights,
                            tmpWifiGuestAccessDevice[j].placeholder,
                            tmpWifiGuestAccessDevice[j].ipAddr);

                }

                updateScreens();

                // Now let's check which devices are still online



                int counter = 0;
                while ((forceUpdate == false)
                        && (stop == false)
                        && (counter < 300000)) {
                    Thread.currentThread().sleep(500);
                    counter += 500;
                }

            } catch (InterruptedException e) {
            }
        }
        // End of the line
        System.out.println("WifiGuestStatus: Thread terminating " + Thread.currentThread());

    }
    public WifiGuestAccessDevice[] getWifiGuestAccessDevices() {
        WifiGuestAccessDevice copyWifiGuestAccessDevice[] = new WifiGuestAccessDevice[wifiGuestAccessDevice.length];
        for (int j = 0; j< wifiGuestAccessDevice.length; j++) {
            copyWifiGuestAccessDevice[j] = new WifiGuestAccessDevice(
                    wifiGuestAccessDevice[j].connected,
                    wifiGuestAccessDevice[j].deviceName,
                    wifiGuestAccessDevice[j].signalStrength,
                    wifiGuestAccessDevice[j].noise,
                    wifiGuestAccessDevice[j].accessRights,
                    wifiGuestAccessDevice[j].placeholder,
                    wifiGuestAccessDevice[j].ipAddr);
        }
        return copyWifiGuestAccessDevice;
    }
    public void forceStatusUpdate() {
        forceUpdate = true;
    }

}

