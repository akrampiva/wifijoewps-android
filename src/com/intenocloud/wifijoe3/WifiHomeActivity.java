package com.intenocloud.wifijoe3;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.intenocloud.wifijoe2.WifiSpeedActivity;
import com.intenocloud.wifijoe3.wps.WPSSimulationActivity;

/**
 * Created by erik on 8/26/13.
 */
public class WifiHomeActivity extends Activity {

    // The BroadcastReceiver that tracks network connectivity changes.
    private NetworkReceiver receiver = null;

    private Handler hm;
    private WifiHomeActivity homeActivity;
    private Dialog dialog = null;
    private TextView warning = null;

    @SuppressLint("HandlerLeak")
	public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        System.err.println("WifiHomeActivity onCreate");
        homeActivity = this;

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        // load up the layout
        setContentView(R.layout.wifi_home);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);



        // get the button resource in the xml file and assign it to a local
        LinearLayout homeDevicesButton = (LinearLayout) findViewById(R.id.homeLayoutDevices);
        homeDevicesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (systemAvailable() == false) {
                    onBackPressed();
                    return;
                }
                if (createDialog()) {
                    return;
                }
                if (LoginActivity.loginActivity.getWifiPermanentIf() != "") {
                    Intent intent = new Intent().setClass(
                            LoginActivity.loginActivity.getApplicationContext(),
                            WifiDevicesActivity.class);
                    startActivity(intent);
                }else{
                    String text = getResources().getString(R.string.home_no_permanent_wifi_interface);
                    LoginActivity.loginActivity.showMessage(text);
                    return;
                }
            }
        });

        LinearLayout homeGuestButton = (LinearLayout)findViewById(R.id.homeLayoutGuest);
        homeGuestButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (systemAvailable() == false) {
                    onBackPressed();
                    return;
                }
                if (createDialog()) {
                    return;
                }
                String gwGuestIf = LoginActivity.loginActivity.getGuestIf();
                if (gwGuestIf.compareTo("") == 0) {
                    String text = getResources().getString(R.string.home_no_guest_interface_found_message);
                    LoginActivity.loginActivity.showMessage(text);
                }else{
                    Intent intent = new Intent().setClass(
                            LoginActivity.loginActivity.getApplicationContext(),
                            WifiGuestActivity.class);
                    startActivity(intent);
                }
            }
        });
        LinearLayout homeSpeedButton = (LinearLayout) findViewById(R.id.homeLayoutSpeed);
        homeSpeedButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (systemAvailable() == false) {
                    onBackPressed();
                    return;
                }
                if (createDialog()) {
                    return;
                }
                Intent intent = new Intent().setClass(
                        LoginActivity.loginActivity.getApplicationContext(),
                        WifiSpeedActivity.class);
                startActivity(intent);
            }
        });
        LinearLayout homeWPSButton = (LinearLayout) findViewById(R.id.homeLayoutwps);
        homeWPSButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (systemAvailable() == false) {
                    onBackPressed();
                    return;
                }
                if (createDialog()) {
                    return;
                }
                boolean isLocal = LoginActivity.loginActivity.isGatewayLocal();
                if (isLocal == false) {
                    String text = getResources().getString(R.string.home_gateway_not_local_connected_message);
                    LoginActivity.loginActivity.showMessage(text);
                }else{
                	Intent intent = new Intent().setClass(
                            LoginActivity.loginActivity.getApplicationContext(),
                            WPSSimulationActivity.class);
                    startActivity(intent);
                }
            }
        });
        LinearLayout homeConfigureButton = (LinearLayout) findViewById(R.id.homeLayoutConfigure);
        homeConfigureButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	
                if (systemAvailable() == false) {
                    onBackPressed();
                    return;
                }
                if (createDialog()) {
                    return;
                }

                Intent intent = new Intent().setClass(
                        LoginActivity.loginActivity.getApplicationContext(),
                        WifiSettingsActivity.class);
                startActivity(intent);
                
            	
            	
            }
        });

        LinearLayout homeLogoutButton = (LinearLayout) findViewById(R.id.homeLayoutLogout);
        homeLogoutButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }
        });
        hm = new Handler() {
            public void handleMessage(Message m) {
                // receive message
                Bundle data = m.getData();
                String dataString = data.getString("updateScreen");

                if (dataString != null) {
                    if (dataString.equals("dismissDialog")) {
                        // time to shut down the dialog box
                        System.out.println("WifiHomeActivity All is fine, remove dialog");
                        if (dialog != null) dialog.dismiss();
                        dialog = null;
                        System.out.println("WifiHomeActivity dialog removed");
                        return;
                    }
                    if (dialog != null) {
                        warning = (TextView) dialog.findViewById(R.id.home_dialog_warning);
                        if (warning != null) warning.setText(dataString);
                    }
                }
            }
        };

        createDialog();

        // Registers BroadcastReceiver to track network connection changes.
        //IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        //receiver = new NetworkReceiver();
        //this.registerReceiver(receiver, filter);
    }
    private boolean systemAvailable() {
        if ((LoginActivity.loginActivity == null)
                || (LoginActivity.loginActivity.cf == null)) {
            return false;
        }
        return true;
    }

    private boolean createDialog() {
        if (systemAvailable() == false) return false;

        if ((LoginActivity.loginActivity.cf.isConnectedToServer() == false)
                || (LoginActivity.loginActivity.getGatewayJid().compareTo("") == 0)
                || (LoginActivity.loginActivity.isApiAvailable() == false)
                || (LoginActivity.loginActivity.installingApi() == true)) {

            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
            wmlp.gravity = Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL;

            dialog.setContentView(R.layout.wifi_home_dialog);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));



            LinearLayout cancelButton = (LinearLayout) dialog.findViewById(R.id.home_dialog_button_container);
            // if button is clicked, close the custom dialog
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    homeActivity.onBackPressed();
                }
            });
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();

            new Thread(new Runnable() {
                @SuppressWarnings("static-access")
				public void run() {

                    while(true) {
                        try {
                            Thread.currentThread().sleep(1000);
                        } catch (InterruptedException e) {
                            break;
                        }
                        if ((LoginActivity.loginActivity == null)
                                || (LoginActivity.loginActivity.cf == null)
                                || (dialog == null)) {
                            break;
                        }
                        // Check the status and update the dialog message
                        if (LoginActivity.loginActivity.cf.isConnectedToServer() == false) {
                            String text = getResources().getString(R.string.home_not_connected_to_server_message);
                            update(text);
                            continue;
                        }
                        String gwJid = LoginActivity.loginActivity.getGatewayJid();
                        if (gwJid.compareTo("") == 0) {
                            String text = getResources().getString(R.string.home_gateway_not_yet_found_message);
                            update(text);
                            continue;
                        }
                        if (LoginActivity.loginActivity.installingApi() == true) {
                            String text = getResources().getString(R.string.home_api_installing);
                            update(text);
                            continue;
                        }
                        if (LoginActivity.loginActivity.isApiAvailable() == false) {
                            String text = getResources().getString(R.string.home_api_not_available);
                            update(text);
                            continue;
                        }
                        update("dismissDialog");

                        break;

                    }
                }
            }).start();

            return true;
        }
        return false;
    }
    private void update(String message) {
        Message msg = Message.obtain();

        Bundle bundle = new Bundle();
        bundle.putString("updateScreen", message);

        msg.setData(bundle);

        try {
            hm.sendMessage(msg);
        } catch (Exception e) {
            System.err.println("updateScreen failed to send message");
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
        if (receiver != null) {
            this.unregisterReceiver(receiver);
            receiver = null;
        }
    }
    @Override
    public void onDestroy() {
        // Unregisters BroadcastReceiver when app is destroyed.
        if (receiver != null) {
            this.unregisterReceiver(receiver);
            receiver = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        // Unregisters BroadcastReceiver when app is destroyed.
        if (receiver != null) {
            this.unregisterReceiver(receiver);
            receiver = null;
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if ((LoginActivity.loginActivity == null)
                || (LoginActivity.loginActivity.cf == null)) {
            // Restart App in Login screen
            // move to screen handling the settings
            Intent i = getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage( getBaseContext().getPackageName() );
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
            return;
        }
        // Registers BroadcastReceiver to track network connection changes.
        if (receiver == null) {
            IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
            receiver = new NetworkReceiver();
            this.registerReceiver(receiver, filter);
        }
    }

    public static boolean isConnected(Context ctx) {
        ConnectivityManager conMgr = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if (i == null)
            return false;
        if (!i.isConnected())
            return false;
        if (!i.isAvailable())
            return false;
        return true;
    }

    public class NetworkReceiver extends BroadcastReceiver {
        private String bssid = "";
        @SuppressWarnings("unused")
		private boolean connected = false;
        private boolean initialized = false;

        NetworkReceiver(){
            initialized = false;
        }
        public String getWifiBSSID() {
            WifiManager wifiManager = (WifiManager)
                    getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            String bssid = wifiInfo.getBSSID();
            return bssid;
        }


        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager conn =  (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conn.getActiveNetworkInfo();
            if (networkInfo == null) {
                return;
            }

            connected = true;
            if (networkInfo.isConnected() == false) {
                connected = false;
                if (initialized == false) initialized = true;
                return;
            }

            String currentbssid = "";
            if ( networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                currentbssid = getWifiBSSID();
            }
            if (currentbssid == null) currentbssid = "";
            if (bssid == null) bssid = "";

            if (currentbssid.compareTo(bssid) != 0) {
                // bssid connection changed
                bssid = currentbssid;

                // Now reconnect
                if (LoginActivity.loginActivity == null) return;
                if (initialized == false) {
                    initialized = true;
                }else{
                    LoginActivity.loginActivity.reconnect();
                }
            }
        }

    }
}
