package com.intenocloud.wifijoe3;

import com.intenocloud.core.CloudFriends;

/**
 * Created by erik on 9/1/13.
 */
public class WifiSettingsStatus implements Runnable {
    protected screenActivity notifyUpdateScreenActivityObject = null;
    protected CloudFriends cf = null;
    public String gwJid = ""; // To be retrieved
    public String guestSsid = "";
    public String guestState = "";
    public String guestDelayed = "";
    public String permanentSsid = "";
    public String permanentPasswd = "";
    public String permanentSecurityType = "";
    public String permanentInterface = "";
    protected Thread runner;
    protected Boolean stop = false;

    public WifiSettingsStatus(CloudFriends cfInstance) {
        cf = cfInstance;
        notifyUpdateScreenActivityObject = null;

        runner = new Thread(this, "WifiStatus"); // (1) Create a new thread.
        System.out.println(runner.getName());
        stop = false;

    }
    public void run() {
        determineConnection();
        updateScreens("settingsCollected");
    }
    protected void determineConnection() {
        gwJid = LoginActivity.loginActivity.getGatewayJid();
        permanentInterface = LoginActivity.loginActivity.getWifiPermanentIf();
        permanentSsid = LoginActivity.loginActivity.getPermanentSsid();
        permanentPasswd = LoginActivity.loginActivity.getPermanentPasswd();
        permanentSecurityType = LoginActivity.loginActivity.getPermanentSecurityType();
        guestSsid = LoginActivity.loginActivity.getGuestSsid();
        guestDelayed = LoginActivity.loginActivity.getGuestTimeout();
        guestState = LoginActivity.loginActivity.getGuestState();
    }
    public void setGuestTimeout(String timeout) {
        LoginActivity.loginActivity.setGuestTimeout(timeout);
        updateScreens("");
    }
    public void setGuest(String ssid, String state) {
        LoginActivity.loginActivity.setGuest(ssid, state);
        updateScreens("");
    }
    public void setGuestState(String state) {
        LoginActivity.loginActivity.setGuestState(state);
        updateScreens("");
    }
    public void setPermanent(String ssid, String pass) {
        LoginActivity.loginActivity.setPermanent(ssid, pass);
        updateScreens("");
    }
    public void registerUpdateScreenEvent(screenActivity screenActivityObject) {
        notifyUpdateScreenActivityObject = screenActivityObject;
        runner.start();
    }
    protected void updateScreens(String msg) {
        // Run through registered Activities and give update
        if (notifyUpdateScreenActivityObject == null) return;
        notifyUpdateScreenActivityObject.update(msg);
    }
    public void forceUpdate() {
        LoginActivity.loginActivity.forceUpdate();
        runner.start();
    }
}
