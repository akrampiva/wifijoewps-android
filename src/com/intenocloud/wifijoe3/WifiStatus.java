package com.intenocloud.wifijoe3;

import java.util.Set;

import org.cretz.swig.collection.NativeSet;


import com.intenocloud.core.BlockedCommandHandler;
import com.intenocloud.core.BlockedCommandResult;
import com.intenocloud.core.CloudFriends;
import com.intenocloud.core.CommandEntry;
import com.intenocloud.core.JNIbridge;
import com.intenocloud.core.SWIGTYPE_p_std__listT_std__string_t;
import com.intenocloud.core.StringListIterator;
import com.intenocloud.core.StringListWrapper;

/**
 * Created by erik on 8/18/13.
 */
public class WifiStatus implements Runnable {
	public String gwJid = ""; // To be retrieved
	public String wifiSsid = ""; // To be retrieved
	public String wifiSecurity = ""; // To be retrieved
	public String wifiPassword = ""; // To be retrieved
	public String wifiGuestState = "disabled";
	public Boolean wifiStatusUpToDate = false;
	public Boolean wifiStatusUpDating = false;
	protected String wifiIf;
	public String wifiGuestSsid = "";
	public String wifiGuestIf = "";

	protected CloudFriends cf;
	protected screenActivity notifyUpdateScreenActivityObject = null;
	private WifiPermanentAccessDevice wifiPermanentAccessDevice[] = {};
	private WifiPermanentAccessDevice wifiPermanentAccessDeviceCpy[] = {};

	private LANPermanentAccessDevice lanPermanentAccessDevice[] = {};
	private LANPermanentAccessDevice lanPermanentAccessDeviceCpy[] = {};

	protected Thread runner;
	public boolean stop = false;
	protected int timeInterval;

	public WifiStatus(CloudFriends cfInstance, int timeInterval) {
		this.timeInterval = timeInterval;
		cf = cfInstance;
		notifyUpdateScreenActivityObject = null;

		runner = new Thread(this, "WifiStatus"); // (1) Create a new thread.
		System.out.println(runner.getName());
		stop = false;
		runner.start(); // (2) Start the thread.
	}

	protected void init() {
		gwJid = LoginActivity.loginActivity.getGatewayJid();
		wifiSsid = LoginActivity.loginActivity.getPermanentSsid();
		wifiPassword = LoginActivity.loginActivity.getPermanentPasswd();
		wifiSecurity = LoginActivity.loginActivity.getPermanentSecurityType();
		wifiGuestSsid = LoginActivity.loginActivity.getGuestSsid();
		wifiGuestState = LoginActivity.loginActivity.getGuestState();
		wifiGuestIf = LoginActivity.loginActivity.getGuestIf();
		wifiIf = LoginActivity.loginActivity.getWifiPermanentIf();
	}

	@SuppressWarnings("static-access")
	public void run() {
		CommandEntry cmd;
		BlockedCommandHandler request;
		BlockedCommandResult result;
		int noise;
		String[] leasesMac = null;
		String[] leasesIpAddr = null;
		String[] leasesName = null;
		String[] wifiPermanentMac = null;
		String[] wifiPermanentIpAddr = null;
		String[] wifiPermanentName = null;
		String[] wifiPermanentSignalStrength = null;
		String[] arpTableIpAddr = null;
		String[] arpTableMac = null;

		// Display info about this particular thread
		System.out.println(Thread.currentThread());
		init();

		while (stop == false) {

			try {
				wifiStatusUpDating = true;
				if ((gwJid != "") && (wifiIf != "")) {
					// Fetch leases information
					cmd = new CommandEntry("leases", "");
					request = new BlockedCommandHandler();
					cf.sendCommand("netMgr", cmd, gwJid, request);
					result = request.waitForResult();
					if (result.getResultType() != BlockedCommandResult.REPLY) {
						System.err
								.println("WifiStatus runner: Did not receive reply on leases");
						wifiStatusUpDating = false;
						if (stop == false)
							Thread.currentThread().sleep(1000);
						continue;
					}
					leasesMac = getArrayReply(result, "mac");
					leasesIpAddr = getArrayReply(result, "ipAddr");
					leasesName = getArrayReply(result, "name");

					// Fetch ARP table
					cmd = new CommandEntry("arpTable", "");
					request = new BlockedCommandHandler();
					cf.sendCommand("netMgr", cmd, gwJid, request);
					result = request.waitForResult();
					if (result.getResultType() != BlockedCommandResult.REPLY) {
						System.err
								.println("WifiStatus runner: Did not receive reply on arpTable");
						wifiStatusUpDating = false;
						if (stop == false)
							Thread.currentThread().sleep(1000);
						continue;
					}
					arpTableIpAddr = getArrayReply(result, "ipAddr");
					arpTableMac = getArrayReply(result, "mac");

					// Fetch wifi associated devices information
					cmd = new CommandEntry("wifi", "");
					cmd.setInput("interface", wifiIf);
					cmd.setInput("noise", "");
					cmd.setInput("signalStrength", "");
					// cmd.setInput("assocTime", ""); maybe to support later
					cmd.setInput("macAddress", "");

					request = new BlockedCommandHandler();
					cf.sendCommand("netMgr", cmd, gwJid, request);
					result = request.waitForResult();
					if (result.getResultType() != BlockedCommandResult.REPLY) {
						System.err
								.println("WifiStatus runner: Did not receive reply on wifi cmd");
						wifiStatusUpDating = false;
						if (stop == false)
							Thread.currentThread().sleep(1000);
						continue;
					}
					wifiPermanentMac = getArrayReply(result, "macAddress");
					wifiPermanentSignalStrength = getArrayReply(result,
							"signalStrength");
					noise = Integer.parseInt(result.getCmd().getReply("noise"));

					// Got all needed information to update the list
					// Arping scanning of each device is also still needed
					// Filter out devices that are not connected to the
					// permanent wifi to create the final list to display
					wifiPermanentIpAddr = new String[wifiPermanentMac.length];
					wifiPermanentName = new String[wifiPermanentMac.length];
					wifiPermanentAccessDevice = new WifiPermanentAccessDevice[wifiPermanentMac.length];

					lanPermanentAccessDevice = new LANPermanentAccessDevice[leasesMac.length];

					for (int j = 0; j < wifiPermanentMac.length; j++) {
						wifiPermanentIpAddr[j] = "";
						wifiPermanentName[j] = "";
						wifiPermanentAccessDevice[j] = new WifiPermanentAccessDevice();
						for (int i = 0; i < leasesMac.length; i++) {
							if (leasesMac[i].compareTo(wifiPermanentMac[j]) == 0) {
								// We have a match, update name and IP-addr
								// information
								wifiPermanentAccessDevice[j].macAdress = leasesMac[i];
								wifiPermanentAccessDevice[j].deviceName = leasesName[i];
								if (leasesName[i].equals("*")) {
									wifiPermanentAccessDevice[j].deviceName = leasesIpAddr[i];
								}
								wifiPermanentAccessDevice[j].connected = false;
								wifiPermanentAccessDevice[j].signalStrength = Integer
										.parseInt(wifiPermanentSignalStrength[j]);
								wifiPermanentAccessDevice[j].noise = noise;
								if(i < leasesIpAddr.length && leasesIpAddr[i]!=null && leasesIpAddr[i].length() > 0)
								{
									wifiPermanentAccessDevice[j].connected = true;
									wifiPermanentAccessDevice[j].ipAddr = leasesIpAddr[i];
								}
								else
									wifiPermanentAccessDevice[j].ipAddr = "no IP";
								wifiPermanentAccessDevice[j].macAdress = leasesMac[i];

								wifiPermanentIpAddr[j] = leasesIpAddr[i];
								wifiPermanentName[j] = leasesName[i];

							}
						}
						if (wifiPermanentAccessDevice[j].deviceName == null) {
							for (int i = 0; i < arpTableMac.length; i++) {
								if (arpTableMac[i].toUpperCase().compareTo(
										wifiPermanentMac[j].toUpperCase()) == 0) {
									// We have a match, update name and IP-addr
									// information
									wifiPermanentAccessDevice[j].macAdress = arpTableMac[i];
									wifiPermanentAccessDevice[j].deviceName = arpTableIpAddr[i];
									wifiPermanentAccessDevice[j].connected = false;
									wifiPermanentAccessDevice[j].signalStrength = Integer
											.parseInt(wifiPermanentSignalStrength[j]);
									wifiPermanentAccessDevice[j].noise = noise;
									if(i < arpTableIpAddr.length && arpTableIpAddr[i]!=null && arpTableIpAddr[i].length() > 0)
									{
										wifiPermanentAccessDevice[j].connected = true;
										wifiPermanentAccessDevice[j].ipAddr = arpTableIpAddr[i];
									}
									else
										wifiPermanentAccessDevice[j].ipAddr = "no IP";
									
									wifiPermanentIpAddr[j] = arpTableIpAddr[i];
									wifiPermanentName[j] = arpTableIpAddr[i];
								}
							}
						}
						if (wifiPermanentAccessDevice[j].deviceName == null) {
							// We have a match, update name and IP-addr
							// information
							wifiPermanentAccessDevice[j].deviceName = wifiPermanentMac[j]
									.toUpperCase();
							wifiPermanentAccessDevice[j].connected = false;
							wifiPermanentAccessDevice[j].signalStrength = Integer
									.parseInt(wifiPermanentSignalStrength[j]);
							wifiPermanentAccessDevice[j].noise = noise;
							wifiPermanentAccessDevice[j].ipAddr = "no IP";
							wifiPermanentIpAddr[j] = "no IP";
							wifiPermanentName[j] = wifiPermanentMac[j]
									.toUpperCase();
						}

					}

					// LAN DEVICES
					for (int k = 0; k < leasesMac.length; k++) {

						lanPermanentAccessDevice[k] = new LANPermanentAccessDevice();
						lanPermanentAccessDevice[k].macAdress = leasesMac[k];
						lanPermanentAccessDevice[k].deviceName = leasesName[k];
						if (leasesName[k].equals("*")) {
							lanPermanentAccessDevice[k].deviceName = leasesIpAddr[k];
						}
						if(k < leasesIpAddr.length && leasesIpAddr[k]!=null && leasesIpAddr[k].length() > 0)
						{
							lanPermanentAccessDevice[k].connected = true;
							lanPermanentAccessDevice[k].ipAddr = leasesIpAddr[k];
						}
						else
						{
							lanPermanentAccessDevice[k].connected = false;
							lanPermanentAccessDevice[k].ipAddr = "no IP";
						}

						lanPermanentAccessDevice[k].signalStrength = 0;
						lanPermanentAccessDevice[k].noise = noise;
						lanPermanentAccessDevice[k].connectionType = "lan";
						
						for (int i = 0; i < wifiPermanentAccessDevice.length; i++) {
							if (leasesMac[k]
									.compareTo(wifiPermanentAccessDevice[i].macAdress) == 0) {
								// We have a match, update name and IP-addr
								// information
								lanPermanentAccessDevice[k].deviceName = wifiPermanentAccessDevice[i].deviceName;
								lanPermanentAccessDevice[k].connected = wifiPermanentAccessDevice[i].connected;
								lanPermanentAccessDevice[k].signalStrength = wifiPermanentAccessDevice[i].signalStrength;
								lanPermanentAccessDevice[k].noise = wifiPermanentAccessDevice[i].noise;
								lanPermanentAccessDevice[k].ipAddr = wifiPermanentAccessDevice[i].ipAddr;
								lanPermanentAccessDevice[k].connectionType = "wifi";

							}
						}

					}

					
					wifiStatusUpDating = false;
					wifiStatusUpToDate = true;
					wifiPermanentAccessDeviceCpy = wifiPermanentAccessDevice
							.clone();
					lanPermanentAccessDeviceCpy = lanPermanentAccessDevice
							.clone();

					updateScreens();

					// Now let's check which devices are still online

				} else {
					// Try to setup again
					init();
				}
				// delay before we continue polling
				int counter = 0;
				while ((stop == false) && (counter < 10000)) {
					Thread.currentThread().sleep(timeInterval);
					counter += timeInterval;
				}

			} catch (InterruptedException e) {
			}
		}
	}

	protected void updateScreens() {
		// Run through registered Activities and give update
		if (notifyUpdateScreenActivityObject == null)
			return;
		notifyUpdateScreenActivityObject.update("screen");
	}

	public WifiPermanentAccessDevice[] getWifiPermanentAccessDevices() {
		return wifiPermanentAccessDeviceCpy.clone();
	}

	public LANPermanentAccessDevice[] getLanPermanentAccessDevice() {
		return lanPermanentAccessDeviceCpy.clone();
	}

	public void setLanPermanentAccessDevice(
			LANPermanentAccessDevice lanPermanentAccessDevice[]) {
		this.lanPermanentAccessDevice = lanPermanentAccessDevice;
	}

	public String[] getArrayReply(BlockedCommandResult result, String parameter) {
		SWIGTYPE_p_std__listT_std__string_t listReply = JNIbridge
				.getEmptyCoreStringList();
		result.getCmd().getReply(parameter, listReply);
		Set<String> replySet = new NativeSet<String>(String.class,
				StringListIterator.class, listReply, StringListWrapper.class);
		String[] reply = replySet.toArray(new String[0]);
		return (reply);
	}

	public void registerUpdateScreenEvent(screenActivity screenActivityObject) {
		notifyUpdateScreenActivityObject = screenActivityObject;
		if (wifiStatusUpDating == false) {
			notifyUpdateScreenActivityObject.update("screen");
		}
	}

	public void stopRunning() {
		stop = true;
		LoginActivity.loginActivity.cf.cancelOutstandingCmds(gwJid);
		try {
			runner.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public LANPermanentAccessDevice[] getLanPermanentAccessDeviceCpy() {
		return lanPermanentAccessDeviceCpy;
	}

	public void setLanPermanentAccessDeviceCpy(
			LANPermanentAccessDevice lanPermanentAccessDeviceCpy[]) {
		this.lanPermanentAccessDeviceCpy = lanPermanentAccessDeviceCpy;
	}
}
