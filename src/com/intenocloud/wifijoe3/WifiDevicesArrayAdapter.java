package com.intenocloud.wifijoe3;


import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * Created by erik on 8/26/13.
 */
public class WifiDevicesArrayAdapter extends ArrayAdapter<WifiPermanentAccessDevice> {
    Context context;
    int layoutResourceId;
    ArrayList<WifiPermanentAccessDevice>data = null;

    public WifiDevicesArrayAdapter(Context context, int layoutResourceId, ArrayList<WifiPermanentAccessDevice> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        permanentAccessDeviceHolder holder = null;

        String deviceName;
        double S;
        double N;
        boolean connected;
        synchronized (this) {
            WifiPermanentAccessDevice device;
            device = data.get(position);
            deviceName = device.deviceName;
            S= device.signalStrength;
            N= device.noise;
            connected = device.connected;
        }
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new permanentAccessDeviceHolder();
            holder.accessImage = (ImageView)row.findViewById(R.id.device_active_icon);
            holder.wifiSignalImage = (ImageView)row.findViewById(R.id.wifi_strength_icon);
            holder.txtTitle = (TextView)row.findViewById(R.id.device_name);

            row.setTag(holder);
        }
        else
        {
            holder = (permanentAccessDeviceHolder)row.getTag();
        }

        holder.txtTitle.setText(deviceName);

        double snr = S-N;
        int icon = R.drawable.wifi_perfect;
        if(snr < 15){
            icon = R.drawable.wifi_bad;
        } else if (snr<25){
            icon = R.drawable.wifi_slow;
        } else if (snr<40){
            icon = R.drawable.wifi_ext_good;
        }
        holder.accessImage.setImageResource(icon);

        holder.wifiSignalImage.setImageResource(icon);
        if (connected) {
            holder.accessImage.setImageResource(R.drawable.full_access_blue_icon);
            holder.wifiSignalImage.setVisibility(View.VISIBLE);
        }else{
            holder.accessImage.setImageResource(R.drawable.full_access_grey_icon);
            holder.wifiSignalImage.setVisibility(View.INVISIBLE);
        }

        return row;
    }

    static class permanentAccessDeviceHolder
    {
        ImageView accessImage;
        ImageView wifiSignalImage;
        TextView txtTitle;
    }
}
