package com.intenocloud.wifijoe3;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;


/**
 * Created by erik on 8/31/13.
 */
public class WifiSettingsActivity extends Activity implements screenActivity {
    private Handler hm;
    private WifiSettingsStatus wifiSettingsStatus = null;
    private String origUserName = "";
    private String origPassword = "";
    private String origServer = "";
    private String origGuestSSID = "";
    private String origGuestState = "";
    private String origGuestDelay = "";
    private String origPermSSID = "";
    private String origPermPass = "";
    private String origPairingState = "";
    private String currentUserName = "";
    private String currentPassword = "";
    private String currentServer = "";
    private String currentPairingState = "";
    private String currentGuestSSID = "";
    private String currentGuestState = "";
    private String currentGuestDelay = "";
    private String currentPermSSID = "";
    private String currentPermPass = "";


    @SuppressLint("HandlerLeak")
	public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        System.err.println("WifiSettingsActivity onCreate");

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        // load up the layout
        setContentView(R.layout.wifi_settings);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        String userName = "";
        String passWord = "";
        String server = "";
        String pairing = "";

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        userName = sharedPrefs.getString("prefUsername", "");
        passWord = sharedPrefs.getString("prefPassword", "");
        server = sharedPrefs.getString("prefServer", "");
        pairing = sharedPrefs.getString("prefPairingState", "");
        if (pairing.compareTo("") == 0) {
            pairing = "off";
        }
        EditText gwAccountSetting = (EditText)findViewById(R.id.username_settings);
        gwAccountSetting.setText(userName, EditText.BufferType.NORMAL);
        gwAccountSetting = (EditText)findViewById(R.id.userpassword);
        gwAccountSetting.setText(passWord, EditText.BufferType.NORMAL);
        gwAccountSetting = (EditText)findViewById(R.id.servername_settings);
        gwAccountSetting.setText(server, EditText.BufferType.NORMAL);
        origUserName = userName;
        origPassword = passWord;
        origServer = server;

        currentPairingState = pairing;
        origPairingState = currentPairingState;

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            TextView version = (TextView)findViewById(R.id.version_settings);
            version.setText(pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            System.err.println("WifiSettingsActivity: Version of the application not found");
        }

        // get the button resource in the xml file and assign it to a local
        // variable of type Button
        final ImageButton eyeAccount = (ImageButton) findViewById(R.id.account_user_password_eye);
        final ImageView eyeAccountDelegate = (ImageView) findViewById(R.id.settings_trans1);


        // this is the action listener
        eyeAccountDelegate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View viewParam) {
                // Toggle button image and password field visibility
                EditText passwordText = (EditText) findViewById(R.id.userpassword);
                if ((passwordText.getInputType() & InputType.TYPE_TEXT_VARIATION_PASSWORD) == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
                    // Password was mode set, toggle to visible normal mode
                    passwordText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                    eyeAccount.setImageResource(R.drawable.permanent_access_eye_open);
                } else {
                    passwordText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    eyeAccount.setImageResource(R.drawable.permanent_access_eye_closed);
                }
            }
        }
        ); // end of settings.setOnclickListener
        final ImageButton eyePermanent = (ImageButton) findViewById(R.id.settings_permanent_password_eye);
        final ImageView eyePermanentDelegate = (ImageView) findViewById(R.id.settings_trans2);


        // this is the action listener
        eyePermanentDelegate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View viewParam) {
                // Toggle button image and password field visibility
                TextView passwordText = (TextView) findViewById(R.id.settings_permanent_password);
                if ((passwordText.getInputType() & InputType.TYPE_TEXT_VARIATION_PASSWORD) == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
                    // Password was mode set, toggle to visible normal mode
                    passwordText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                    eyePermanent.setImageResource(R.drawable.permanent_access_eye_open);
                } else {
                    passwordText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    eyePermanent.setImageResource(R.drawable.permanent_access_eye_closed);
                }
            }
        }
        ); // end of settings.setOnclickListener


        hm = new Handler() {
            public void handleMessage(Message m) {
                // receive message

                if ((wifiSettingsStatus.gwJid.compareTo("") != 0)
                    && (LoginActivity.loginActivity.getWifiPermanentIf() != "")) {
                    // GW connected and Wifi interface configured
                    RelativeLayout gwSettings = (RelativeLayout)findViewById(R.id.configure_gateway_settings_container);
                    gwSettings.setVisibility(View.VISIBLE);

                    TextView gwSettingsPermPass = (TextView)findViewById(R.id.settings_permanent_password);
                    gwSettingsPermPass.setText(wifiSettingsStatus.permanentPasswd, TextView.BufferType.NORMAL);
                    origPermPass = wifiSettingsStatus.permanentPasswd;

                    TextView gwSettingsSsid = (TextView)findViewById(R.id.settings_permanent_ssid);
                    gwSettingsSsid.setText(wifiSettingsStatus.permanentSsid, TextView.BufferType.NORMAL);
                    origPermSSID = wifiSettingsStatus.permanentSsid;

                    TextView gwSettingsDelay = (TextView)findViewById(R.id.guest_timeout_settings);
                    gwSettingsDelay.setText(wifiSettingsStatus.guestDelayed, TextView.BufferType.NORMAL);
                    origGuestDelay = wifiSettingsStatus.guestDelayed;

                    TextView gwSettingsGuestSsid = (TextView)findViewById(R.id.guest_ssid_settings);
                    gwSettingsGuestSsid.setText(wifiSettingsStatus.guestSsid, TextView.BufferType.NORMAL);
                    origGuestSSID = wifiSettingsStatus.guestSsid;

                    ToggleButton toggle = (ToggleButton)findViewById(R.id.toggleGuestButtonState);
                    origGuestState = wifiSettingsStatus.guestState;
                    if (wifiSettingsStatus.guestState.compareTo("true") == 0) {
                        toggle.setChecked(true);
                    }else{
                        toggle.setChecked(false);
                    }
                }
            }
        };
        ToggleButton toggle = (ToggleButton)findViewById(R.id.togglePairingButtonState);
        if (currentPairingState.compareTo("on") == 0) {
            toggle.setChecked(true);
        }else{
            toggle.setChecked(false);
        }
        ImageView backButton = (ImageView) findViewById(R.id.settings_header_back_image);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View viewParam) {
                onBackPressed();
            }
        }
        ); // end of settings.setOnclickListener

        wifiSettingsStatus = new WifiSettingsStatus(LoginActivity.loginActivity.cf);
        wifiSettingsStatus.registerUpdateScreenEvent(this);
    }
    public void update(String message) {
        Message msg = Message.obtain();

        Bundle bundle = new Bundle();
        bundle.putString("updateScreen", message);

        msg.setData(bundle);

        try {
            hm.sendMessage(msg);
        } catch (Exception e) {
            System.err.println("updateScreen failed to send message");
            e.printStackTrace();
        }
    }
    @Override
    public void onBackPressed() {


        Boolean changed = getCurrentSettings();
        if (changed) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Updating settings")
                    .setMessage("Do you want to apply the new settings?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            updateSettings();
                            finish();
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }else{
            super.onBackPressed();
            finish();
        }
    }
    public Boolean getCurrentSettings() {
        Boolean changed = false;
        EditText accountServer = (EditText) findViewById(R.id.servername_settings);
        currentServer = accountServer.getText().toString();
        if (currentServer.compareTo(origServer) != 0){
            changed = true;
        }

        EditText userPassword = (EditText) findViewById(R.id.userpassword);
        currentPassword = userPassword.getText().toString();
        if (currentPassword.compareTo(origPassword) != 0){
            changed = true;
        }

        EditText usernameEditText = (EditText) findViewById(R.id.username_settings);
        currentUserName = usernameEditText.getText().toString();
        if (currentUserName.compareTo(origUserName) != 0){
            changed = true;
        }

        if (LoginActivity.loginActivity.cf.isConnectedToServer()) {
            EditText guestTimeout = (EditText)findViewById(R.id.guest_timeout_settings);
            currentGuestDelay = guestTimeout.getText().toString();
            if (currentGuestDelay.compareTo(origGuestDelay) != 0){
                changed = true;
            }

            EditText guestSsid = (EditText)findViewById(R.id.guest_ssid_settings);
            currentGuestSSID = guestSsid.getText().toString();
            if (currentGuestSSID.compareTo(origGuestSSID) != 0){
                changed = true;
            }

            EditText permanentSsid = (EditText)findViewById(R.id.settings_permanent_ssid);
            currentPermSSID = permanentSsid.getText().toString();
            if (currentPermSSID.compareTo(origPermSSID) != 0){
                changed = true;
            }

            EditText permanentPass = (EditText)findViewById(R.id.settings_permanent_password);
            currentPermPass = permanentPass.getText().toString();
            if (currentPermPass.compareTo(origPermPass) != 0){
                changed = true;
            }
            ToggleButton toggle = (ToggleButton)findViewById(R.id.toggleGuestButtonState);
            if (toggle.isChecked()) {
                currentGuestState = "true";
            }else{
                currentGuestState = "false";
            }
            if (currentGuestState.compareTo(origGuestState) != 0) {
                changed = true;
            }
            toggle = (ToggleButton)findViewById(R.id.togglePairingButtonState);
            if (toggle.isChecked()) {
                currentPairingState = "on";
            }else{
                currentPairingState = "off";
            }
            if (currentPairingState.compareTo(origPairingState) != 0) {
                changed = true;
            }
        }

        return changed;
    }
    public void updateSettings() {

        // Update username field in preferences
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPrefs.edit();
        if (currentServer.compareTo(origServer) != 0) {
            editor.putString("prefServer", currentServer);
            editor.commit();
            System.out.println("WifiSettingsActivity: updated server name preferences");
        }

        // Update username field in preferences
        if (currentPassword.compareTo(origPassword) != 0) {
            editor.putString("prefPassword", currentPassword);
            editor.commit();
            System.out.println("WifiSettingsActivity: updated password name preferences");
        }

        if (currentUserName.compareTo(origUserName) != 0) {
            editor.putString("prefUsername", currentUserName);
            editor.commit();
            System.out.println("WifiSettingsActivity: updated user name preferences");
        }
        editor.putString("prefPairingState", currentPairingState);
        editor.commit();
        if (LoginActivity.loginActivity.cf.isConnectedToServer()
            && (wifiSettingsStatus != null)) {
            if (currentGuestDelay.compareTo(origGuestDelay) != 0){
                wifiSettingsStatus.setGuestTimeout(currentGuestDelay);
            }
            if ((currentGuestSSID.compareTo(origGuestSSID) != 0)
                    || (currentGuestDelay.compareTo(origGuestDelay) != 0)
                    || (currentGuestState.compareTo(origGuestState) != 0)) {
                wifiSettingsStatus.setGuest(currentGuestSSID, currentGuestState);
            }
            if ((currentPermPass.compareTo(origPermPass) != 0)
                || (currentPermSSID.compareTo(origPermSSID) != 0)) {
                wifiSettingsStatus.setPermanent(currentPermSSID, currentPermPass);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if ((LoginActivity.loginActivity == null)
                || (LoginActivity.loginActivity.cf == null)) {
            // Restart App in Login screen
            // move to screen handling the settings
            Intent i = getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage( getBaseContext().getPackageName() );
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
            return;
        }
        if (LoginActivity.loginActivity.cf.isConnectedToServer() == false) {
            return;
        }
        wifiSettingsStatus = new WifiSettingsStatus(LoginActivity.loginActivity.cf);
        wifiSettingsStatus.registerUpdateScreenEvent(this);
    }
}
