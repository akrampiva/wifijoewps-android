package com.intenocloud.wifijoe3;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.intenocloud.core.CommandEntry;
import com.intenocloud.core.ParamEntry;

/**
 * Created by erik on 11/8/13.
 */
public class RegisterAccountCommand extends CommandEntry {
    public RegisterAccountCommand(String cmdName, String cmdDescr) {
        super(cmdName, cmdDescr);

// WanCmds::account("account", "Push a new account for the device");

        registerInputParam("username", ParamEntry.paramTypeString, "device user name");
        registerInputParam("password", ParamEntry.paramTypeString, "password");
        registerInputParam("domain", ParamEntry.paramTypeString, "domain to connect to");

        registerReplyParam("result", ParamEntry.paramTypeString, "success or failed");

    }
    public void execute() {
        String username;
        String password;
        String domain;

        if ((!isInputSet("username"))
                || (!isInputSet("password"))
                || (!isInputSet("domain"))) {
            setReply("result", "failed");
            return;
        }

        username = getInput("username");
        password = getInput("password");
        domain = getInput("domain");

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.loginActivity.getApplicationContext());
        SharedPreferences.Editor editor = sharedPrefs.edit();

        editor.putString("prefServer", domain);
        System.out.println("RegisterAccountCommand: updated server name preferences");
        editor.putString("prefPassword", password);
        System.out.println("RegisterAccountCommand: updated password name preferences");
        editor.putString("prefUsername", username);
        editor.commit();
        System.out.println("RegisterAccountCommand: updated user name preferences");

        setReply("result", "success");

        LoginActivity.loginActivity.update("pairingAccountPushedStart");
    }
}
