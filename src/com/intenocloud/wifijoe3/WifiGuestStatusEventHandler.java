package com.intenocloud.wifijoe3;

import com.intenocloud.core.CommandEventHandler;

/**
 * Created by erik on 8/30/13.
 */
public class WifiGuestStatusEventHandler extends CommandEventHandler {
    WifiGuestStatus wStatus;

    public WifiGuestStatusEventHandler(WifiGuestStatus wStatus) {
        //super();

        this.wStatus = wStatus;
    }

    @Override
    public void commandEvent(String eventSubject, String value) {

        if (eventSubject.compareTo("connected") == 0) {
            // The connected list was modified
            System.out.println("WifiGuestStatusEventHandler: The connected list was modified");
        }
        if (eventSubject.compareTo("state") == 0) {
            // The state of the guestmgr was modified
            System.out.println("WifiGuestStatusEventHandler: The state of the guestmgr was modified");
        }
        if (eventSubject.compareTo("ssid") == 0) {
            // The ssid of the guestmgr was modified
            System.out.println("WifiGuestStatusEventHandler: The ssid of the guestmgr was modified");
        }
        if (eventSubject.compareTo("access") == 0) {
            // The access list of the guestmgr was modified
            System.out.println("WifiGuestStatusEventHandler: The access list of the guestmgr was modified");
        }
        wStatus.forceStatusUpdate();
    }

}

