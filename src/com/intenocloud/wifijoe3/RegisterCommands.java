package com.intenocloud.wifijoe3;

import com.intenocloud.core.ServiceCommunication;

/**
 * Created by erik on 11/8/13.
 */
public class RegisterCommands extends ServiceCommunication {
    private RegisterAccountCommand registerAccountCommand = null;

    public RegisterCommands(String topicName) {
        super(topicName);

        registerAccountCommand = new RegisterAccountCommand("account", "Push a new account for the device");
        addCommand(registerAccountCommand);

        registerService();
    }
}
