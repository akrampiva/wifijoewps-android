package com.intenocloud.wifijoe3;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.intenocloud.core.login.LoginDiscoveredGateway;

/**
 * Created by erik on 11/7/13.
 */
public class LoginDiscoveryArrayAdapter extends ArrayAdapter<LoginDiscoveredGateway> {
    Context context;
    int layoutResourceId;
    ArrayList<LoginDiscoveredGateway> data = null;

    public LoginDiscoveryArrayAdapter(Context context, int layoutResourceId, ArrayList<LoginDiscoveredGateway> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        permanentAccessDeviceHolder holder = null;

        LoginDiscoveredGateway device = data.get(position);
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new permanentAccessDeviceHolder();
            holder.txtTitle = (TextView)row.findViewById(R.id.discovered_device_name);

            row.setTag(holder);

        }
        else
        {
            holder = (permanentAccessDeviceHolder)row.getTag();
        }

        if (device != null) holder.txtTitle.setText(device.serial);

        return row;
    }

    static class permanentAccessDeviceHolder
    {
        TextView txtTitle;
    }
}
