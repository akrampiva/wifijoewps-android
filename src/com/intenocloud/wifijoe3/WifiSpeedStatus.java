package com.intenocloud.wifijoe3;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Set;

import org.cretz.swig.collection.NativeSet;

import com.intenocloud.core.BlockedCommandResult;
import com.intenocloud.core.CloudFriends;
import com.intenocloud.core.JNIbridge;
import com.intenocloud.core.Observable;
import com.intenocloud.core.SWIGTYPE_p_std__listT_std__string_t;
import com.intenocloud.core.StringListIterator;
import com.intenocloud.core.StringListWrapper;

/**
 * Created by erik on 8/30/13.
 */
public class WifiSpeedStatus implements Runnable {
    protected screenActivity notifyUpdateScreenActivityObject = null;
    protected CloudFriends cf = null;
    public String gwJid = ""; // To be retrieved
    public Boolean gwIsLocalAccessible = false;
    protected Thread runner;
    protected Boolean stop = false;

    // Speedtest
    private Boolean lTestOngoing = false;
    public long lStart = 0;
    public Double measuredSpeedReceiving = 0.0;
    public Double measuredSpeedSending = 0.0;
    public Double measuredSpeedFromEvents = 0.0;


    public WifiSpeedStatus(CloudFriends cfInstance) {
        cf = cfInstance;
        notifyUpdateScreenActivityObject = null;

        runner = new Thread(this, "WifiStatus"); // (1) Create a new thread.
        System.out.println(runner.getName());
        stop = false;
        runner.start(); // (2) Start the thread.
    }

    @SuppressWarnings("static-access")
	public void run() {
      

        //Display info about this particular thread
        System.out.println(Thread.currentThread());

        long start = System.nanoTime();

        // Check which gateway and if the mobile device is locally connected
        determineConnection();

        long end = System.nanoTime();
        if (((end - start)/(1000*1000*1000)) < 5) {
            // Wait 2 seconds before starting the next test
            try {
                Thread.currentThread().sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // Check which gateway and if the mobile device is locally connected
        determineConnection();

        // Push to update the screen
        updateScreens("connectionDetermined");

        if (gwJid.compareTo("") == 0) {
            System.out.println("Speedcheck finished, no speed measurement performed");
            return;
        }

        // Check Speed to the server
        // Not yet supported
        //updateScreens("internetSpeedDone");

        // Check speed to the gateway
        // Transfer maximum 10 seconds, and 10 Mega byte of data

        receive(gwJid, 10, 10*1024*1024);
        updateScreens("gatewaySpeedReceiveDone");

        transmit(gwJid, 10, 10*1024*1024);
        updateScreens("gatewaySpeedTransmitDone");

    }
    
    protected void determineConnection() {
        gwJid = LoginActivity.loginActivity.getGatewayJid();
        gwIsLocalAccessible = LoginActivity.loginActivity.isGatewayLocal();
    }

    public String[] getArrayReply(BlockedCommandResult result, String parameter) {
        SWIGTYPE_p_std__listT_std__string_t listReply = JNIbridge.getEmptyCoreStringList();
        result.getCmd().getReply(parameter, listReply);
        Set<String>replySet = new NativeSet<String>(String.class, StringListIterator.class,
                listReply, StringListWrapper.class);
        String[] reply = replySet.toArray(new String[0]);
        return (reply);
    }
    public void registerUpdateScreenEvent(screenActivity screenActivityObject) {
        notifyUpdateScreenActivityObject = screenActivityObject;
    }
    protected void updateScreens(String msg) {
        // Run through registered Activities and give update
        if (notifyUpdateScreenActivityObject == null) return;
        notifyUpdateScreenActivityObject.update(msg);
    }
    @SuppressWarnings("resource")
	protected Boolean receive(String lDst, long lSeconds, long lBytes)
    {
        if (lTestOngoing) {
            System.out.println("Test already ongoing, wait for it to complete");
            return false;
        }
        if (stop) return false;

        lTestOngoing = true;
        measuredSpeedReceiving = 0.0;
        measuredSpeedFromEvents = 0.0;

        int tryPort = 15353;
        while (available(tryPort) == false) {
            tryPort += 1;
        }
        System.err.println("Opening connection to 127.0.0.1:" + tryPort);
        String ipPortCombo = "127.0.0.1:" + tryPort;

    /* Create a proxy where we locally accept a socket that can connect with
     * the created proxy tunnel. We use local address 127.0.0.1:5556 for this.
     * The other side will be setup with an element that writes zeros
     * We will measure the throughput speed in the thread.
     */
        Observable pObs = new Observable();
        WifiSpeedEventHandler wifiSpeedEventHandler = new WifiSpeedEventHandler(this);
        SWIGTYPE_p_std__listT_std__string_t lSessionIdList = JNIbridge.getEmptyCoreStringList();
        int res = cf.createProxies(lDst, 1, lSessionIdList, "accept", ipPortCombo,
                "readZero", "", null, wifiSpeedEventHandler, pObs);
        if (res < 0) {
            System.out.println("Failed to create the needed proxy tunnel");
            lTestOngoing = false;
            return false;
        }

        Socket sock;
        InputStream sockInput = null;
        try {
            sock = new Socket("127.0.0.1", tryPort);
            sockInput = sock.getInputStream();
        }
        catch (IOException e){
            e.printStackTrace(System.err);
            cf.closeTransferSessions(lSessionIdList);
            lTestOngoing = false;
            return false;
        }


        /* Pump data for a while */
        long totalRead = 0;
        long maxToRead = lBytes;
        long start = 0;
        long totalTestTime = start + lSeconds * 1000 * 1000 * 1000;
        long nano_seconds_since_start = start;
        long secondLater = start + 1 * 1000 * 1000 * 1000;

        while ((stop == false)
                && (nano_seconds_since_start < totalTestTime)
                && (totalRead < maxToRead)) {
            byte[] buf = new byte[64*1024];
            int n = 0;
            int bytesToRead = 0;

            if (totalRead == 0) {
                bytesToRead = 1;
            }else{
                bytesToRead = buf.length;
            }
            try {
                n = sockInput.read(buf, 0, bytesToRead);
            }
            catch (IOException e){
                e.printStackTrace(System.err);
            }
            if (n <= 0) {
                System.out.println("ERROR reading from socket ");
                break;
            }
            totalRead += n;
            if (totalRead == 1) {
                // Actual start of the process, we measure the amount of data
                // per second we can transfer
                // We should not add the delay to setup the connection
                start = System.nanoTime();
                totalTestTime = start + lSeconds * 1000 * 1000 * 1000;
            }
            nano_seconds_since_start = System.nanoTime();
            if (secondLater < nano_seconds_since_start) {
                //System.out.println("Progress: " + totalRead + " bytes");
                secondLater += 1 * 1000 * 1000 * 1000;
                secondLater = start + 1 * 1000 * 1000 * 1000;
            }
        }
        if (stop) {
			return false;
		}
        System.out.println("============Receiving done============================");

       /* Calculate speed */
        nano_seconds_since_start = nano_seconds_since_start - start;
        System.out.println("Receiving: Time since start " + ((double)nano_seconds_since_start/(1000 * 1000 * 1000)) + " seconds");
        System.out.println("Receiving: Bytes sent " + totalRead);
        measuredSpeedReceiving = ((double)totalRead)/((double)nano_seconds_since_start/(1000 * 1000 * 1000));
        System.out.println("Receiving: bytes per second " + measuredSpeedReceiving
                + " (" + measuredSpeedReceiving/(1024*1024) + "MB/s)");

       /* Close connection */
        try {
            sock.close();
        }
        catch (IOException e){
            System.err.println("Exception closing socket.");
            e.printStackTrace(System.err);
        }

        /* Close transfer session */
        if (cf.isConnectedToServer()) {
            cf.closeTransferSessions(lSessionIdList);
        }

        lTestOngoing = false;
        return true;
    }
    @SuppressWarnings("resource")
	protected Boolean transmit(String lDst, long lSeconds, long lBytes)
    {
        if (lTestOngoing) {
            System.out.println("Test already ongoing, wait for it to complete");
            return false;
        }
        if (stop) return false;

        lTestOngoing = true;
        measuredSpeedSending = 0.0;
        measuredSpeedFromEvents = 0.0;

        int tryPort = 15353;
        while (available(tryPort) == false) {
            tryPort += 1;
        }
        System.err.println("Opening connection to 127.0.0.1:" + tryPort);
        String ipPortCombo = "127.0.0.1:" + tryPort;

        /* Create a proxy where we locally accept a socket that can connect with
         * the created proxy tunnel. We use local address 127.0.0.1:5556 for this.
         * The other side will be setup with an element that writes zeros
         * We will measure the throughput speed in the thread.
         */
        Observable pObs = new Observable();
        WifiSpeedEventHandler wifiSpeedEventHandler = new WifiSpeedEventHandler(this);
        SWIGTYPE_p_std__listT_std__string_t lSessionIdList = JNIbridge.getEmptyCoreStringList();
        int res = cf.createProxies(lDst, 1, lSessionIdList, "accept", ipPortCombo,
                "writeDiscard", "", null, wifiSpeedEventHandler, pObs);
        if (res < 0) {
            System.out.println("Failed to create the needed proxy tunnel");
            lTestOngoing = false;
            return false;
        }

        Socket sock;
        OutputStream sockOutput = null;
        try {
            sock = new Socket("127.0.0.1", tryPort);
            sockOutput = sock.getOutputStream();
        }
        catch (IOException e){
            e.printStackTrace(System.err);
            cf.closeTransferSessions(lSessionIdList);
            lTestOngoing = false;
            return false;
        }


        /* Pump data for a while */
        long totalWrite = 0;
        long maxToWrite = lBytes;
        long start = System.nanoTime();
        long totalTestTime = start + lSeconds * 1000 * 1000 * 1000;
        long nano_seconds_since_start = start;
        long secondLater = start + 1 * 1000 * 1000 * 1000;
        while ((stop == false)
                && (nano_seconds_since_start < totalTestTime)
                && (totalWrite < maxToWrite)) {
            byte[] buf = new byte[64*1024];
            boolean error = false;

            try {
                sockOutput.write(buf, 0, buf.length);
            }
            catch (IOException e){
                e.printStackTrace(System.err);
                error = true;
            }
            if (error) break;

            totalWrite += buf.length;
            nano_seconds_since_start = System.nanoTime();
            if (secondLater < nano_seconds_since_start) {
                //System.out.println("Progress: " + totalWrite + " bytes");
                secondLater += 1 * 1000 * 1000 * 1000;
            }
        }
        if (stop) return false;
        System.out.println("============Sending done============================");

       /* Calculate speed */
        nano_seconds_since_start = nano_seconds_since_start - start;
        measuredSpeedSending = ((double)totalWrite)/(nano_seconds_since_start/(1000 * 1000 * 1000));
        System.out.println("Sending: bytes per second " + measuredSpeedSending
                + " (" + measuredSpeedSending/(1024*1024) + "MB/s)");

       /* Close connection */
        try {
            sock.close();
        }
        catch (IOException e){
            System.err.println("Exception closing socket.");
            e.printStackTrace(System.err);
        }

        /* Close transfer session */
        if (cf.isConnectedToServer()) {
            cf.closeTransferSessions(lSessionIdList);
        }

        lTestOngoing = false;
        return true;
    }
    public static boolean available(int port) {
        if (port < 1200 || port > 63000) {
            throw new IllegalArgumentException("Invalid start port: " + port);
        }

        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return true;
        } catch (IOException e) {
        } finally {
            if (ds != null) {
                ds.close();
            }

            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                /* should not be thrown */
                }
            }
        }

        return false;
    }
    public void stopRunning() {
        stop = true;
    }
}
