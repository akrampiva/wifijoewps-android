package com.intenocloud.wifijoe3;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class WifiDevicesActivity extends Activity implements screenActivity {
    private WifiStatus wStatus;
    private Handler hm;
    private ListView listView;
    private LANDevicesArrayAdapter adapter;

    @SuppressLint("HandlerLeak")
	public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        System.err.println("WifiDevicesActivity onCreate");

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        // load up the layout
        setContentView(R.layout.wifi_devices);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ArrayList<LANPermanentAccessDevice> values = new ArrayList<LANPermanentAccessDevice>();
        adapter = new LANDevicesArrayAdapter(this, R.layout.permanent_access_row, values);

        listView = (ListView) findViewById(R.id.wifiDevicesListView);

        View header = (View)getLayoutInflater().inflate(R.layout.permanent_access_list_header, null);
        listView.addHeaderView(header);
        listView.setAdapter(adapter);

        // get the button resource in the xml file and assign it to a local
        // variable of type Button
        final ImageView eye = (ImageButton) findViewById(R.id.imageButtonEye);
        final ImageView eyeTrans = (ImageView) findViewById(R.id.wifi_devices_transp);

        // this is the action listener
        eyeTrans.setOnClickListener(new View.OnClickListener() {
            public void onClick(View viewParam) {
                // Toggle button image and password field visibility
                TextView passwordText = (TextView) findViewById(R.id.password);
                if ((passwordText.getInputType() & InputType.TYPE_TEXT_VARIATION_PASSWORD) == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
                    // Password was mode set, toggle to visible normal mode
                    passwordText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                    eye.setImageResource(R.drawable.permanent_access_eye_open);
                } else {
                    passwordText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    eye.setImageResource(R.drawable.permanent_access_eye_closed);
                }
            }
        }
        ); // end of settings.setOnclickListener
        ImageView backButton = (ImageView) findViewById(R.id.device_header_back_image);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View viewParam) {
                onBackPressed();
            }
        }
        ); // end of settings.setOnclickListener

        hm = new Handler() {
            public void handleMessage(Message m) {
                // receive message
                Bundle data = m.getData();
                @SuppressWarnings("unused")
				String dataString = data.getString("updateScreen");

                // Update full screen (message could mre fine grained, but this is enough for now

                TextView yourPrivateWifiNetworkName = (TextView) findViewById(R.id.your_private_wifi_network_name);
                String text = getResources().getString(R.string.your_private_wifi_network_name) + " : " + wStatus.wifiSsid;
                yourPrivateWifiNetworkName.setText(text, TextView.BufferType.NORMAL);

                TextView passwordText = (TextView) findViewById(R.id.password);
                passwordText.setText(wStatus.wifiPassword, TextView.BufferType.NORMAL);

                TextView securityText = (TextView) findViewById(R.id.security_mode);
                securityText.setText(wStatus.wifiSecurity, TextView.BufferType.NORMAL);


                // Update the list of connected devices
                if (!adapter.isEmpty()) {
                    System.out.println("Not Empty listAdapter: Clearing it");
                    synchronized (adapter) {
                        adapter.clear();
                    }
                }
                // Fetch latest status
                LANPermanentAccessDevice permanentAccessDevicesData[] = wStatus.getLanPermanentAccessDevice();
                if(permanentAccessDevicesData!=null && permanentAccessDevicesData.length > 0)
                {
                	for (int i = 0; i< permanentAccessDevicesData.length; i++) {
                        System.out.println("lan permanent listAdapter adding : " + permanentAccessDevicesData[i].deviceName);
                        synchronized (adapter) {
                            adapter.add(permanentAccessDevicesData[i]);
                        }
                    }
                	adapter.notifyDataSetChanged();
                }
                
            }
        };

    }
    @Override
    protected void onPause() {
        super.onPause();
        System.err.println("WifiDevicesActivity onPause");
        // No longer visible, destroy the class doing the updates
        if (wStatus != null) {
            wStatus.stopRunning();
        }
        wStatus = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if ((LoginActivity.loginActivity == null)
                || (LoginActivity.loginActivity.cf == null)) {
            // Restart App in Login screen
            // move to screen handling the settings
            Intent i = getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage( getBaseContext().getPackageName() );
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
            return;
        }

        if (LoginActivity.loginActivity.cf.isConnectedToServer() == false) {
            onBackPressed();
            return;
        }
        // create the class that monitors Wifi
        if (wStatus == null) wStatus = new WifiStatus(LoginActivity.loginActivity.cf,500);
        wStatus.registerUpdateScreenEvent(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // No longer visible, destroy the class doing the updates
        System.err.println("WifiDevicesActivity onDestroy");
        if (wStatus == null) return;
        wStatus.stopRunning();
        wStatus = null;
    }

    public void update(String message) {
        Message msg = Message.obtain();

        Bundle bundle = new Bundle();
        bundle.putString("updateScreen", message);

        msg.setData(bundle);

        try {
            hm.sendMessage(msg);
        } catch (Exception e) {
            System.err.println("updateScreen failed to send message");
            e.printStackTrace();
        }
    }
}

