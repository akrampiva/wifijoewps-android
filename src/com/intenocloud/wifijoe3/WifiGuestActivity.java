package com.intenocloud.wifijoe3;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.intenocloud.core.BlockedCommandHandler;
import com.intenocloud.core.BlockedCommandResult;
import com.intenocloud.core.CommandEntry;

/**
 * Created by erik on 8/27/13.
 */
public class WifiGuestActivity extends Activity implements screenActivity {
    final Context context = this;
    private Dialog dialog;
    private int listViewClickedPosition = 0;
    private WifiGuestStatus wStatus;
    private Handler hm;
    private ListView listView;
    private WifiGuestArrayAdapter adapter;

    @SuppressLint("HandlerLeak")
	public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        System.err.println("WifiGuestActivity onCreate");

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        // load up the layout
        setContentView(R.layout.wifi_guest);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ArrayList<WifiGuestAccessDevice> values = new ArrayList<WifiGuestAccessDevice>();

        adapter = new WifiGuestArrayAdapter(this, R.layout.guest_access_row, values);

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

        dialog.setContentView(R.layout.guest_access_selection_dialog_layout);


        LinearLayout cancelButton = (LinearLayout) dialog.findViewById(R.id.guest_access_cancel_layout);
        // if button is clicked, close the custom dialog
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        RelativeLayout fullButton = (RelativeLayout) dialog.findViewById(R.id.guest_access_full_access_layout);
        // if button is clicked, close the custom dialog
        fullButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAccessRightsDevice("lanwan");
                dialog.dismiss();
            }
        });
        RelativeLayout localButton = (RelativeLayout) dialog.findViewById(R.id.guest_access_home_access_layout);
        // if button is clicked, close the custom dialog
        localButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAccessRightsDevice("lan");
                dialog.dismiss();
            }
        });
        RelativeLayout wanButton = (RelativeLayout) dialog.findViewById(R.id.guest_access_internet_access_layout);
        // if button is clicked, close the custom dialog
        wanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAccessRightsDevice("wan");
                dialog.dismiss();
            }
        });
        RelativeLayout noneButton = (RelativeLayout) dialog.findViewById(R.id.guest_access_no_access_layout);
        // if button is clicked, close the custom dialog
        noneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAccessRightsDevice("");
                dialog.dismiss();
            }
        });


        listView = (ListView) findViewById(R.id.wifiGuestListView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                @SuppressWarnings("unused")
				WifiGuestAccessDevice wifiGuestAccessDevice = (WifiGuestAccessDevice) listView.getItemAtPosition(position);
                // custom dialog
                listViewClickedPosition = position;

                WifiGuestAccessDevice device = adapter.getItem(position);
                if (device == null) return;
                if (device.placeholder == true) return;
                TextView title = (TextView)dialog.findViewById(R.id.guest_access_dialog_title_label);
                String textTitle = getResources().getString(R.string.guest_dialog_title) + " " + device.deviceName;
                title.setText(textTitle, TextView.BufferType.NORMAL);

                // Make sure we are using the right IP-address so force and update
                wStatus.forceStatusUpdate();

                dialog.show();

            }
        });

        TextView enableGuestNetwork = (TextView) findViewById(R.id.your_guest_wifi_network_name);

        enableGuestNetwork.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if ((wStatus.wifiGuestState.compareTo("false") == 0)
                        && (wStatus.wifiGuestSsid != "")
                        && (wStatus.wifiStatusUpToDate == true)) {
                    LoginActivity.loginActivity.setGuestState("true");
                }
            }
        });
        ImageView backButton = (ImageView) findViewById(R.id.guest_header_back_image);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View viewParam) {
                onBackPressed();
            }
        }
        ); // end of settings.setOnclickListener

        hm = new Handler() {
            public void handleMessage(Message m) {
                // receive message
                Bundle data = m.getData();
                @SuppressWarnings("unused")
				String dataString = data.getString("updateScreen");

                TextView yourGuestWifiNetworkName = (TextView) findViewById(R.id.your_guest_wifi_network_name);
                // Update the list of connected devices
                if (!adapter.isEmpty()) {
                    System.out.println("Not Empty listAdapter: Clearing it");
                    adapter.clear();
                }                    // Fetch latest status
                if (wStatus == null) return;
                WifiGuestAccessDevice guestAccessDevicesData[] = wStatus.getWifiGuestAccessDevices();

                for (int i = 0; i< guestAccessDevicesData.length; i++) {
                    System.out.println("wifi permanent listAdapter adding : " + guestAccessDevicesData[i].deviceName
                                        + " accessR " + guestAccessDevicesData[i].accessRights
                                        + " placeH " + guestAccessDevicesData[i].placeholder);
                    adapter.add(guestAccessDevicesData[i]);
                }
                if (wStatus.wifiStatusUpToDate == true) {
                    if (wStatus.wifiGuestSsid == "") {
                        // No SSID defined for the guest interface. This needs to be configured first
                        String text = getResources().getString(R.string.your_guest_wifi_network_not_yet_configured);
                        yourGuestWifiNetworkName.setText(text, TextView.BufferType.NORMAL);
                        return;
                    }else{
                        String text = getResources().getString(R.string.your_guest_wifi_network_name) + " : " + wStatus.wifiGuestSsid;
                        yourGuestWifiNetworkName.setText(text, TextView.BufferType.NORMAL);
                    }

                    if (wStatus.wifiGuestState.compareTo("false") == 0) {
                        // Guest interface is not yet enabled, ask to click to activate
                        String text = getResources().getString(R.string.your_guest_wifi_network_activation) + " : " + wStatus.wifiGuestSsid;
                        yourGuestWifiNetworkName.setText(text, TextView.BufferType.NORMAL);
                        return;
                    }

                }
            }
        };
        wStatus = new WifiGuestStatus(LoginActivity.loginActivity.cf,500);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if ((LoginActivity.loginActivity == null)
            || (LoginActivity.loginActivity.cf == null)) {
            // Restart App in Login screen
            // move to screen handling the settings
            Intent i = getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage( getBaseContext().getPackageName() );
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
            return;
        }

        if (wStatus == null) return;
        // No longer visible, destroy the class doing the updates
        wStatus.stop = true;
        try {
            wStatus.runner.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        wStatus = null;
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // create the class that monitors Wifi
        if (wStatus == null) wStatus = new WifiGuestStatus(LoginActivity.loginActivity.cf,500);
        wStatus.registerUpdateScreenEvent(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // No longer visible, destroy the class doing the updates
        if (wStatus == null) return;
        LoginActivity.loginActivity.cf.cancelOutstandingCmds(wStatus.gwJid);
        wStatus.stop = true;
        try {
            wStatus.runner.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        wStatus = null;
    }


    public void update(String message) {
        Message msg = Message.obtain();

        Bundle bundle = new Bundle();
        bundle.putString("updateScreen", message);

        msg.setData(bundle);

        try {
            hm.sendMessage(msg);
        } catch (Exception e) {
            System.err.println("updateScreen failed to send message");
            e.printStackTrace();
        }
    }
    public void setAccessRightsDevice(String access) {
        // update access rights to full
        // everything is fine but the guest interface is not enabled, enable it now
        WifiGuestAccessDevice device = adapter.getItem(listViewClickedPosition);
        CommandEntry cmd = new CommandEntry("access", "");
        cmd.setInput("ipAddr", device.ipAddr);
        cmd.setInput("type", access);

        BlockedCommandHandler request = new BlockedCommandHandler();
        LoginActivity.loginActivity.cf.sendCommand("guestMgr", cmd, wStatus.gwJid, request);
        BlockedCommandResult result = request.waitForResult();
        if (result.getResultType() != BlockedCommandResult.REPLY) {
            System.err.println("WifiGuestActivity: Did not receive reply on access cmd");
        }
        // Handled by eventing now
        // wStatus.forceStatusUpdate();


    }
}
