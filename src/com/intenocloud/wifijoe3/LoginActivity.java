package com.intenocloud.wifijoe3;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Set;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import org.cretz.swig.collection.NativeSet;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.intenocloud.core.BlockedCommandHandler;
import com.intenocloud.core.BlockedCommandResult;
import com.intenocloud.core.CloudFriends;
import com.intenocloud.core.CommandEntry;
import com.intenocloud.core.JNIbridge;
import com.intenocloud.core.SWIGTYPE_p_std__listT_std__string_t;
import com.intenocloud.core.StringListIterator;
import com.intenocloud.core.StringListWrapper;
import com.intenocloud.core.login.LoginActivityCoreConnectionHandler;
import com.intenocloud.core.login.LoginActivityStatus;
import com.intenocloud.core.login.LoginDiscoveredGateway;
import com.intenocloud.core.login.PairingServicePresenceHandler;


@SuppressLint("HandlerLeak")
public class LoginActivity extends Activity  implements screenActivity {
    static {
        System.loadLibrary("crypto");
        System.loadLibrary("ssl");
        System.loadLibrary("core");
    }

    public String versionName = "";
    public String applicationName = "";
 
    private String pinCodeByUser = "";


    LoginActivityCoreConnectionHandler lACCH;
    PairingServicePresenceHandler yAASPH;
    DevicePresenceHandler devicePresenceHandler;

    static public boolean accountDefined;
    static private String devID;
    private Boolean viewActive = false;
    public CloudFriends cf;
    private Handler hm;
    public static LoginActivity loginActivity = null;
    public String userN = "";
    public String passWd = "";
    public String serV = "";
    public String server = "";

    private ImageView launch = null;

    // select discovered devices
    private ListView listView;
    private LoginDiscoveryArrayAdapter adapter;
    private int listViewClickedPosition = -1;
    private LoginDiscoveredGateway device = null;

    // fetched from gateway
    private String gwJid = ""; // To be retrieved
    private boolean apisAvailable = false;
    private boolean installingAPI = false;
    private String permanentSsid = "";
    private String permanentChannel = "";
    private String wifiIf = "";
    private Boolean gwIsLocalAccessible = false;
    private String permanentSecurityType = "";
    private String permanentPasswd = "";
    private String guestSsid = "";
    private String guestState = "";
    private String guestDelayed = "";
    private String wifiGuestIf = "";
    private boolean statusUpToDate = false;
    private boolean tryingToReConnect = false;
    private SystemCommands systemCommands = null;
    private RegisterCommands registerCommands = null;

    // Track connection status
    boolean wasConnected = false;
    int retries = 0;
    WifiManager wifi;
    WifiManager.MulticastLock multicastLock;
    
	private static final String HOCKEY_APP_ID = "129d6764bd3bc6f0e6c3238df460ee4e";


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        //hockeyApp functions
        checkForUpdates();
        
        loginActivity = this;
        viewActive = true;

        System.err.println("onCreate LoginActivity");

        cf = new CloudFriends();

        final PackageManager pm = getApplicationContext().getPackageManager();
        ApplicationInfo ai;
        try {
            ai = pm.getApplicationInfo( getPackageName(), 0);
        } catch (final PackageManager.NameNotFoundException e) {
            ai = null;
        }
        applicationName = (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");
        try {
            versionName = pm.getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }



        accountDefined = false;

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Remove automatic keyboard pop-up
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // load up the layout
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);



        initConfigurationSettings();

        // Read config settings
        String tmpVal = "";

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String user = sharedPrefs.getString("prefUsername", "");
        System.err.println("Username: " + user);
        String pass = sharedPrefs.getString("prefPassword", "");
        System.err.println("Password: " + pass);
        server = sharedPrefs.getString("prefServer", "");
        System.err.println("Server: " + server);
        String pairingSetting = sharedPrefs.getString("prefPairingState", "");
        if (pairingSetting.compareTo("") == 0) {
            pairingSetting = "off";
        }
        if (server.equals("")) {
            server = JNIbridge.configGet("Core", "cloudfriends_host", tmpVal, 500, false);
            if (!server.equals("")) {
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString("prefServer", server);
                editor.commit();
            }
        }
        devID = LoginActivity.loginActivity.getUniqueDeviceID();

        System.out.println("server.ini contained: " + user + ":" + pass + "@"+ server);

        lACCH = null;

        System.out.println("Using server: " + server);

        if (user.equals("") || pass.equals("")) {
            accountDefined = false;
        }


        // get the button resource in the xml file and assign it to a local
        // variable of type Button
        launch = (ImageView) findViewById(R.id.login_button);

        // this is the action listener
        launch.setOnClickListener(new OnClickListener() {

            public void onClick(View viewParam) {
                if (LoginActivity.loginActivity.cf.isConnectedToServer() == true) {
                    // Disconnect the client
                    ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar1);
                    progressBar.setVisibility(View.VISIBLE);
                    LoginActivity.loginActivity.cf.disconnect();
                    // Eventing will provide the right state
                    return;
                }
                SharedPreferences sharedPrefs = PreferenceManager.
                                getDefaultSharedPreferences(LoginActivity.this);
                server = sharedPrefs.getString("prefServer", "");

                if (server.equals("")) {
                    showMessage("Please fill in the server to connect to first");
                    Intent intent = new Intent(LoginActivity.this,UserSettingActivity.class);
                    startActivity(intent);
                    return;
                }
                // this gets the resources in the xml file and assigns it to a
                // local variable of type EditText
                EditText usernameEditText = (EditText) findViewById(R.id.txt_username);
                EditText passwordEditText = (EditText) findViewById(R.id.txt_password);

                // the getText() gets the current value of the text box
                // the toString() converts the value to String data type
                // then assigns it to a variable of type String
                final String userName = usernameEditText.getText().toString();
                final String passWord = passwordEditText.getText().toString();

                // display the username and the password in string format
                // showAlert("Logging in", "Username: " + sUserName +
                // "nPassword: " + sPassword , "Ok", true);
                System.out.println("username=" + userName);
                System.out.println("password=" + passWord);
                if (userName.equals("")) {
                    LoginActivity.loginActivity.showMessage("Please fill in your user name");
                    return;
                }
                if (passWord.equals("")) {
                    LoginActivity.loginActivity.showMessage("Please fill in your password");
                    return;
                }

                if (lACCH == null) {
                    lACCH = new LoginActivityCoreConnectionHandler("authenticating");
                    LoginActivity.loginActivity.cf.registerConnectionHandler(lACCH);
                }

                InputMethodManager inputManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(viewParam.getWindowToken(), 0);

                ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar1);

                if (LoginActivity.loginActivity.cf.isConnectedToServer() == false) {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.invalidate();
                    System.out.println("Connecting with " + userName + ":" + passWord + "@" + server + " for " + devID);
                    statusUpToDate = false;
                    userN = userName;
                    passWd = passWord;
                    serV = server;
                    LoginActivityStatus loginStatus = new LoginActivityStatus();
                    tryingToReConnect = true;
                    loginStatus.start(userN, passWd, serV);
                    System.out.println("after connect user");
                }

                System.err.println("FINISH ONCLICK");
            }
        }

        ); // end of launch.setOnclickListener
        hm = new Handler() {
            @SuppressLint("NewApi")
			public void handleMessage(Message m) {
                // receive message
                Bundle data = m.getData();
                String dataString = data.getString("updateScreen");
                if (dataString != null) {
                    if (dataString.compareTo("pairingConnected") == 0){
                        yAASPH = new PairingServicePresenceHandler();
                        yAASPH.registerUpdateScreenEvent(LoginActivity.loginActivity);
                        cf.registerPresenceHandler(yAASPH);
                        registerCommands = new RegisterCommands("register");
                        systemCommands = new SystemCommands("system");
                    }
                    if (dataString.compareTo("pairingDisconnected") == 0){
                        if (registerCommands != null) {
                            registerCommands = null;
                        }
                        if (systemCommands != null) {
                            systemCommands = null;
                        }
                        if (yAASPH != null) cf.unregisterPresenceHandler(yAASPH);
                        yAASPH = null;
                        if (lACCH != null) cf.unregisterConnectionHandler(lACCH);
                        lACCH = null;


                        LoginActivity.loginActivity.launch.performClick();
                    }
                    if (dataString.compareTo("pairingAccountPushedStart") == 0) {
                        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.loginActivity);
                        String user = sharedPrefs.getString("prefUsername", "");
                        String pass = sharedPrefs.getString("prefPassword", "");
                        EditText usernameEditText = (EditText) findViewById(R.id.txt_username);
                        EditText passwordEditText = (EditText) findViewById(R.id.txt_password);
                        usernameEditText.setText(user, TextView.BufferType.NORMAL);
                        passwordEditText.setText(pass, TextView.BufferType.NORMAL);
                        server = sharedPrefs.getString("prefServer", "");

                    }
                    if (dataString.compareTo("pairingAccountPushedEnd") == 0) {

                        // disconnect
                        cf.disconnect();
                    }
                    if (dataString.compareTo("connected") == 0){
                        System.out.println("Connected to Cloud Server");
                        devicePresenceHandler = new DevicePresenceHandler();
                        cf.registerPresenceHandler(devicePresenceHandler);
                        systemCommands = new SystemCommands("system");

                        wasConnected = true;
                        retries = 0;
                        tryingToReConnect = false;
                        //if (!accountDefined) {
                            // Account not previously saved and now connected
                            // Let's update permanent settings for restart later
                            EditText usernameEditText = (EditText) findViewById(R.id.txt_username);
                            EditText passwordEditText = (EditText) findViewById(R.id.txt_password);
                            String userName = usernameEditText.getText().toString();
                            String passWord = passwordEditText.getText().toString();

                            System.out.println("Updating shared preferences: " + userName + "." + passWord);

                            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = sharedPrefs.edit();
                            editor.putString("prefUsername", userName);
                            editor.putString("prefPassword", passWord);
                            editor.commit();
                            accountDefined = true;
                        //}


                        TextView loginButtonLabel = (TextView)findViewById(R.id.login_button_label);
                        loginButtonLabel.setText(getString(R.string.login_button_label_connected));
                        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar1);
                        progressBar.setVisibility(View.INVISIBLE);
                        if (viewActive) {
                            Intent intent = new Intent().setClass(
                                    LoginActivity.loginActivity.getApplicationContext(),
                                    WifiHomeActivity.class);
                            startActivity(intent);
                        }

                    }
                    if (dataString.compareTo("userDisconnect") == 0){
                        TextView loginButtonLabel = (TextView)findViewById(R.id.login_button_label);
                        loginButtonLabel.setText(getString(R.string.login_button_label_disconnected));
                        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar1);
                        progressBar.setVisibility(View.INVISIBLE);
                        if (devicePresenceHandler != null) {
                            cf.unregisterPresenceHandler(devicePresenceHandler);
                            devicePresenceHandler = null;
                        }
                    }
                    if (dataString.compareTo("disconnected") == 0){
                        statusUpToDate = false;
                        if (devicePresenceHandler != null) {
                            cf.unregisterPresenceHandler(devicePresenceHandler);
                            devicePresenceHandler = null;
                        }
                        TextView loginButtonLabel = (TextView)findViewById(R.id.login_button_label);
                        loginButtonLabel.setText(getString(R.string.login_button_label_disconnected));
                        if (wasConnected) {
                            retries += 1;
                            if (retries <= 1) {
                                String text = getResources().getString(R.string.login_retry_message);
                                LoginActivity.loginActivity.showMessage(text);
                                //ImageButton launch = (ImageButton) findViewById(R.id.login_button);
                                //launch.performClick();
                                tryingToReConnect = true;
                                LoginActivityStatus loginStatus = new LoginActivityStatus();
                                loginStatus.start(userN, passWd, serV);
                            }else{
                                ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar1);
                                progressBar.setVisibility(View.INVISIBLE);
                                String text = getResources().getString(R.string.login_connectivity_problem_error);
                                LoginActivity.loginActivity.showMessage(text);
                                tryingToReConnect = false;
                            }
                        }else{
                            // Not able to connect, give message to retry later
                            ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar1);
                            progressBar.setVisibility(View.INVISIBLE);
                            String text = getResources().getString(R.string.login_connectivity_problem_error);
                            LoginActivity.loginActivity.showMessage(text);
                        }
                    }
                    if (dataString.compareTo("authenticationFailed") == 0){
                        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar1);
                        progressBar.setVisibility(View.INVISIBLE);
                        EditText usernameEditText = (EditText) findViewById(R.id.txt_username);
                        EditText passwordEditText = (EditText) findViewById(R.id.txt_password);
                        @SuppressWarnings("unused")
						String userName = "";
                        String passWord = "";
                        if (usernameEditText != null) {
                            userName = usernameEditText.getText().toString();
                        }
                        if (passWord != null) {
                            passWord = passwordEditText.getText().toString();
                        }

                        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        @SuppressWarnings("unused")
						String user = sharedPrefs.getString("prefUsername", "");
                        @SuppressWarnings("unused")
						String pass = sharedPrefs.getString("prefPassword", "");

                        System.out.println("authentication failed: back to login screen");
                        String text = getResources().getString(R.string.login_authentication_failed_message);
                        LoginActivity.loginActivity.showMessage(text);
                        System.out.println("messages sent");
                    }
                    if (dataString.compareTo("providePinCode") == 0){
                        // Show dialog box to get pin code
                        System.out.println("LoginActivity: Received request to type in pin code");
                        AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.loginActivity);

                        alert.setTitle("Access code");
                        if (pinCodeByUser.compareTo("") == 0) {
                            alert.setMessage("Please provide the pin code to access the application");
                        }else{
                            alert.setMessage("The code was incorrect please provide another key");
                        }

                        // Set an EditText view to get user input
                        final EditText pinCode = new EditText(LoginActivity.loginActivity);
                        pinCode.setInputType(InputType.TYPE_CLASS_NUMBER);
                        alert.setView(pinCode);

                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                pinCodeByUser = pinCode.getText().toString();
                                // Do something with value!
                                RelativeLayout pairConnectButton = (RelativeLayout)findViewById(R.id.login_pairbuttonwrapper);
                                pairConnectButton.callOnClick();
                            }
                        });

                        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // Canceled.
                                if (registerCommands != null) {
                                    registerCommands = null;
                                }
                                if (systemCommands != null) {
                                    systemCommands = null;
                                }
                                if (yAASPH != null) cf.unregisterPresenceHandler(yAASPH);
                                yAASPH = null;
                                if (lACCH != null) cf.unregisterConnectionHandler(lACCH);
                                lACCH = null;

                                cf.disconnect();

                                RelativeLayout pairing = (RelativeLayout) findViewById(R.id.pairing_layout);
                                pairing.setVisibility(View.GONE);

                                RelativeLayout loginAccountLayout = (RelativeLayout) findViewById(R.id.usernametextandbox);
                                loginAccountLayout.setVisibility(View.VISIBLE);
                            }
                        });

                        alert.show();

                    }
                    if (dataString.compareTo("portalPinNotDefined") == 0){
                        // Show dialog box to get pin code
                        System.out.println("LoginActivity: Received error, portal pin code not provided");
                        AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.loginActivity);

                        alert.setTitle("Pin code required");
                        alert.setMessage("Please provide a code in the server: " + server + " to gain access to this service");


                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // Do something with value!
                                if (registerCommands != null) {
                                    registerCommands = null;
                                }
                                if (systemCommands != null) {
                                    systemCommands = null;
                                }
                                if (yAASPH != null) cf.unregisterPresenceHandler(yAASPH);
                                yAASPH = null;
                                if (lACCH != null) cf.unregisterConnectionHandler(lACCH);
                                lACCH = null;

                                cf.disconnect();

                                RelativeLayout pairing = (RelativeLayout) findViewById(R.id.pairing_layout);
                                pairing.setVisibility(View.GONE);

                                RelativeLayout loginAccountLayout = (RelativeLayout) findViewById(R.id.usernametextandbox);
                                loginAccountLayout.setVisibility(View.VISIBLE);
                            }
                        });

                        alert.show();
                    }

                    if (dataString.compareTo("ServicePresence") == 0){
                        System.out.println("LoginActivity received ServicePresence message");
                        if (!adapter.isEmpty()) {
                            System.out.println("Not Empty listAdapter: Clearing it");
                            adapter.clear();
                        }                    // Fetch latest status
                        LoginDiscoveredGateway[] gateways = yAASPH.getDiscovererdGateways();

                        for (int i = 0; i< gateways.length; i++) {
                            System.out.println("Login listAdapter adding : " + gateways[i].deviceName);
                            adapter.add(gateways[i]);
                        }

                    }
                }

                dataString = data.getString("ToastMessage");
                if (dataString != null) {
                    Toast.makeText(getApplicationContext(), dataString, Toast.LENGTH_LONG).show();
                }
            }
        };
        // get the button resource in the xml file and assign it to a local
        // variable of type Button
        RelativeLayout settings = (RelativeLayout) findViewById(R.id.login_layout_settings);

        // this is the action listener
        settings.setOnClickListener(new OnClickListener() {

            public void onClick(View viewParam) {
                // move to screen handling the settings
                Intent intent = new Intent().setClass(
                        LoginActivity.loginActivity.getApplicationContext(),
                        WifiSettingsActivity.class);
                startActivity(intent);
            }
        }
        ); // end of set11tings.setOnclickListener

        EditText usernameEditText = (EditText) findViewById(R.id.txt_username);
        EditText passwordEditText = (EditText) findViewById(R.id.txt_password);


        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(usernameEditText.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(passwordEditText.getWindowToken(), 0);

        usernameEditText.setText(user, TextView.BufferType.NORMAL);
        passwordEditText.setText(pass, TextView.BufferType.NORMAL);

        if ((pairingSetting.compareTo("off") == 0)
                || (!(user.equals("")) && !(pass.equals("")))) {
            System.out.println("Starting with account settings: " + user + "@" + server + " " + pass);
            RelativeLayout pairing = (RelativeLayout) findViewById(R.id.pairing_layout);
            pairing.setVisibility(View.GONE);

            RelativeLayout loginAccountLayout = (RelativeLayout) findViewById(R.id.usernametextandbox);
            loginAccountLayout.setVisibility(View.VISIBLE);

            if (!(user.equals("")) && !(pass.equals(""))) {
                // Emulate clicking the button as we already know the username/pass
                launch.performClick();
            }
            return;
        }else {
            // start pairing
            System.out.println("Starting pairing service: " + "@" + server);
            RelativeLayout loginAccountLayout = (RelativeLayout) findViewById(R.id.usernametextandbox);
            loginAccountLayout.setVisibility(View.GONE);

            RelativeLayout pairing = (RelativeLayout) findViewById(R.id.pairing_layout);
            pairing.setVisibility(View.VISIBLE);

            ArrayList<LoginDiscoveredGateway> values = new ArrayList<LoginDiscoveredGateway>();
            adapter = new LoginDiscoveryArrayAdapter(this, R.layout.discovered_devices_row, values);

            listView = (ListView) findViewById(R.id.discovery_list);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    LoginDiscoveredGateway gw = (LoginDiscoveredGateway) listView.getItemAtPosition(position);
                    if (gw == null) return;

                    if (listViewClickedPosition != -1) {
                        // restore the previously clicked row
                        @SuppressWarnings("unused")
						LoginDiscoveredGateway previousSelected = adapter.getItem(position);

                    }
                    // custom dialog
                    listViewClickedPosition = position;

                    // Change the color of the selected item
                    device = adapter.getItem(position);
                    if (device == null) return;

                    // Change button
                    TextView loginButtonLabel = (TextView)findViewById(R.id.login_pairinbutton_label);
                    loginButtonLabel.setText(getString(R.string.login_button_label_connect));
                }
            });


            RelativeLayout pairConnectButton = (RelativeLayout)findViewById(R.id.login_pairbuttonwrapper);
            pairConnectButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    @SuppressWarnings("unused")
					RelativeLayout pairConnectButton = (RelativeLayout)findViewById(R.id.login_pairbuttonwrapper);

                    if (device != null) {
                        ProgressBar progressBar = (ProgressBar) findViewById(R.id.login_pairingbutton_progressBar);
                        progressBar.setVisibility(View.VISIBLE);
                        progressBar = (ProgressBar) findViewById(R.id.login_pairing_ongoing_progressBar);
                        progressBar.setVisibility(View.INVISIBLE);
                        System.out.println("login disabling multicast discovery");
                        if (multicastLock != null) {
                            multicastLock.release();
                            multicastLock = null;
                        }
                        // Should first enable the button but hey, let's try this out first
                        new Thread(new Runnable() {
                            public void run() {
                                CommandEntry cmd = new CommandEntry("pair", "");
                                cmd.setInput("jid", device.deviceName);
                                cmd.setInput("widget", "Wifi Joe");
                                cmd.setInput("pin", pinCodeByUser);

                                BlockedCommandHandler request = new BlockedCommandHandler();
                                System.out.println("LoginActivity: sending cmd pairing request for " + device.deviceName);
                                String dst = "pairing@pairing." + server;
                                cf.sendCommand("pairing", cmd, dst, request);
                                BlockedCommandResult result = request.waitForResult();

                                if (result.getResultType() != BlockedCommandResult.REPLY) {
                                    System.err.println("LoginActivity: Did not receive reply for pairing");
                                    LoginActivity.loginActivity.showMessage("Pairing error, try again later");
                                    return;
                                }
                                if (result.getCmd().getReply("result").compareTo("failed") == 0) {
                                    // command failed
                                    if ((result.getCmd().getReply("errorCode").compareTo("105") == 0)
                                        || (result.getCmd().getReply("errorCode").compareTo("102") == 0)) {
                                        // we need to provide a pin code
                                        System.out.println("LoginActivity: got no pairing code available error");
                                        update("providePinCode");
                                        return;
                                    }else if (result.getCmd().getReply("errorCode").compareTo("109") == 0) {
                                        // we need to first go to the portal to provide pin to the pin-protected app
                                        System.out.println("LoginActivity: 109");
                                        update("portalPinNotDefined");
                                        return;
                                    }else{
                                        // Error we cannot recover from
                                        System.out.println("LoginActivity: got unsupported error " + result.getCmd().getReply("errorCode"));
                                        LoginActivity.loginActivity.showMessage("Pairing error, try again later");
                                        return;
                                    }
                                }else{
                                    System.out.println("LoginActivity: Finishing pairing, account received");
                                    update("pairingAccountPushedEnd");
                                }
                            }
                        }).start();
                    }
                }
            });


            System.out.println("Login registering Presence Handler");
            lACCH = new LoginActivityCoreConnectionHandler("pairing");


            // Acquire multicast lock
            wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            multicastLock = wifi.createMulticastLock("multicastLock");
            multicastLock.setReferenceCounted(true);
            multicastLock.acquire();

            cf.pair(server);
            cf.registerConnectionHandler(lACCH);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
      //hockeyApp functions
        checkForCrashes();
        // Change login settings to settings of the stored preferences
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String user = sharedPrefs.getString("prefUsername", "");
        String pass = sharedPrefs.getString("prefPassword", "");
        EditText usernameEditText = (EditText) findViewById(R.id.txt_username);
        EditText passwordEditText = (EditText) findViewById(R.id.txt_password);
        usernameEditText.setText(user, TextView.BufferType.NORMAL);
        passwordEditText.setText(pass, TextView.BufferType.NORMAL);
        viewActive = true;
        if ((LoginActivity.loginActivity.cf.isConnectedToServer() == true)) {
            System.out.println("Connected and coming back to login, time to disconnect");
            //finish();
            gwJid = "";
            LoginActivity.loginActivity.cf.destroy();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        viewActive = false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.err.println("------------LoginActivity onBackPressed---------");
        if (lACCH != null) {
            System.err.println("------------LoginActivity unregistering callback---------");
            LoginActivity.loginActivity.cf.unregisterConnectionHandler(lACCH);
        }
        viewActive = false;
        lACCH = null;

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onStop();
        if (lACCH != null) LoginActivity.loginActivity.cf.unregisterConnectionHandler(lACCH);
        lACCH = null;
        if (devicePresenceHandler != null) {
            cf.unregisterPresenceHandler(devicePresenceHandler);
            devicePresenceHandler = null;
        }
        LoginActivity.loginActivity.cf.destroy();
        //android.os.Process.killProcess(android.os.Process.myPid());
        System.err.println("------------Finished LoginActivity------------------------");
    }


    private void initConfigurationSettings() {
        copyAssets();
        File dir = getFilesDir();
        String filename = dir.getAbsolutePath() + "/server.ini";
        System.out.println("Startup using config file: " + filename);
        JNIbridge.configInit(filename);
    }

    private boolean doesPrivateFileExist(String filename) {

        String[] listOfPrivateFiles = fileList();

        for( int i = 0; i < listOfPrivateFiles.length - 1; i++)
        {
            String element = listOfPrivateFiles[i];
            if (element.equals(filename)) {
                // config file already present
                System.out.println("File already present " + filename);

                // For now keep overwriting (if we return false)
                // Overwriting files needed, as it seems to solve some access rights problems
                // from the C++ context
                // System.out.println("File already present WILL STILL BE OVERWRITTEN!!!! ");
                return false;
            }
        }
        return false;
    }

    private void copyAssets() {
		AssetManager assetManager = getAssets();
		String[] files = null;
		try {
			files = assetManager.list("");
		} catch (IOException e) {
			Log.e("tag", "Failed to get asset file list.", e);
		}
		for (String filename : files) {
			InputStream in = null;
			OutputStream out = null;
			if (doesPrivateFileExist(filename))
				continue;
			if (filename.equals("images") || filename.equals("sounds")
					|| filename.equals("webkit")
					|| filename.equals("databases")
					|| filename.equals("kioskmode"))
				continue;
			try {
				synchronized (this) {
					in = assetManager.open(filename);
					out = openFileOutput(filename, Context.MODE_PRIVATE);
					copyFile(in, out);
					in.close();
					out.flush();
					out.close();
				}

			} catch (IOException e) {
				Log.e("tag", "Failed to copy asset file: " + filename, e);
			}
		}
	}

    private void copyFile(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[1024];
		int read;
		while ((read = in.read(buffer)) != -1) {
			out.write(buffer, 0, read);
		}
	}
    public String getUniqueDeviceID() {
        return sha1Hash(Settings.Secure.ANDROID_ID + getPackageName());
    }

    private String sha1Hash( String toHash )
    {
        String hash = toHash;
        try
        {
            MessageDigest digest = MessageDigest.getInstance( "SHA-1" );
            byte[] bytes = toHash.getBytes("UTF-8");
            digest.update(bytes, 0, bytes.length);
            bytes = digest.digest();
            StringBuilder sb = new StringBuilder();
            for( byte b : bytes )
            {
                sb.append( String.format("%02X", b) );
            }
            hash = sb.toString();
        }
        catch( NoSuchAlgorithmException e )
        {
            e.printStackTrace();
        }
        catch( UnsupportedEncodingException e )
        {
            e.printStackTrace();
        }
        return hash;
    }
    public void showMessage(String message) {
        Message msg = Message.obtain();

        Bundle bundle = new Bundle();
        bundle.putString("ToastMessage", message);

        msg.setData(bundle);

        try {
            hm.sendMessage(msg);
        } catch (Exception e) {
            System.err.println("showMessage failed to send toast message");
            e.printStackTrace();
        }
    }
    public void update(String message) {
        Message msg = Message.obtain();

        Bundle bundle = new Bundle();
        bundle.putString("updateScreen", message);

        msg.setData(bundle);

        try {
            hm.sendMessage(msg);
        } catch (Exception e) {
            System.err.println("updateScreen failed to send message");
            e.printStackTrace();
        }
    }
    public void determineConnection() {
        // Get the right gwJid to talk to
        SWIGTYPE_p_std__listT_std__string_t accList;
        SWIGTYPE_p_std__listT_std__string_t typeList;
        SWIGTYPE_p_std__listT_std__string_t localList;
        Set<String> set;
        String[] listAccount;
        String[] listType;
        String[] listLocal;


        if (cf.isConnectedToServer() == false) {
            forceUpdate();
            return;
        }

        String tmpGwJid = gwJid;
        boolean tmpGwLocal = gwIsLocalAccessible;
        if ((cf.isPresent(gwJid) == false)
            || (statusUpToDate == false)) {
            gwJid = "";
            accList = JNIbridge.getEmptyCoreStringList();
            typeList = JNIbridge.getEmptyCoreStringList();
            localList = JNIbridge.getEmptyCoreStringList();

            cf.getPresenceList(accList, typeList, localList);
            set = new NativeSet<String>(String.class, StringListIterator.class,
                    accList, StringListWrapper.class);
            listAccount = set.toArray(new String[0]);
            set = new NativeSet<String>(String.class, StringListIterator.class,
                    typeList, StringListWrapper.class);
            listType = set.toArray(new String[0]);
            set = new NativeSet<String>(String.class, StringListIterator.class,
                    localList, StringListWrapper.class);
            listLocal = set.toArray(new String[0]);

            int j = 0;
            while (j < listAccount.length) {
                System.out.println("Account " + listAccount[j] + "  type: " + listType[j] + " local: " + listLocal[j]);
                if (listType[j].equals("gateway")) {
                    System.out.println("LoginActivity: found gateway jid: " + listAccount[j]);
                    tmpGwJid = listAccount[j];
                    if (listLocal[j].equals("true")) {
                        System.out.println("LoginActivity: Gateway is local connected jid: " + tmpGwJid);
                        tmpGwLocal = true;

                    }

                    break;
                }
                j++;
            }
        }
        if (cf.isPresent(tmpGwJid) == false) {
            System.out.println("LoginActivity: gateway not present jid: " + tmpGwJid);
            return;
        }
        gwJid = tmpGwJid;
        gwIsLocalAccessible = tmpGwLocal;
        if (apisAvailable == false) {
            // Need to make sure we have the needed API's available first
            apisAvailable = makeAPIsAvailable(tmpGwJid, "guestMgr", "Wifi Joe");
        }
        if (apisAvailable) updateWifiSettings();
    }
    private void updateWifiSettings() {
        if (gwJid.equals("")) {
            System.out.println("LoginActivity: no gateway jid found");
            return;
        }

        if (statusUpToDate) {
            System.out.println("LoginActivity: status up to date, do not update wifi settings");
            return;
        }


        // Still to get Wifi Guest ssid-delayed, Wifi ssid-security-passwd
        // Start retrieving Wifi status
        // Which are the Wifi interfaces?
        CommandEntry cmd = new CommandEntry("wifi", "");
        cmd.setInput("interface", "");

        BlockedCommandHandler request = new BlockedCommandHandler();
        System.out.println("WifiStatus: sending cmd NetMgr wifi interface to " + gwJid);
        cf.sendCommand("netMgr", cmd, gwJid, request);
        System.out.println("WifiStatus: received reply");
        BlockedCommandResult result = request.waitForResult();

        if (result.getResultType() != BlockedCommandResult.REPLY) {
            System.err.println("WifiStatus: Did not receive reply");
        }

        SWIGTYPE_p_std__listT_std__string_t ifList = JNIbridge.getEmptyCoreStringList();
        result.getCmd().getReply("interfaces", ifList);
        Set<String>wifiIfList = new NativeSet<String>(String.class, StringListIterator.class,
                ifList, StringListWrapper.class);

        wifiIf = "";
        int count = 0;
        for (String item : wifiIfList) {
            System.out.println("Wifi interface  " + count + " " + item);
            wifiIf = item;
            break;
        }
        if (wifiIf != "") {
            //Retrieve the Wifi SSID, security and password
            cmd = new CommandEntry("wifi", "");
            cmd.setInput("interface", wifiIf);
            cmd.setInput("ssid", "");
            cmd.setInput("channel", "");
            cmd.setInput("security", "");
            cmd.setInput("password", "");

            request = new BlockedCommandHandler();

            cf.sendCommand("netMgr", cmd, gwJid, request);
            result = request.waitForResult();

            if (result.getResultType() != BlockedCommandResult.REPLY) {
                System.err.println("WifiStatus: Did not receive reply");
            }else{
                permanentSsid = result.getCmd().getReply("ssid");
                permanentChannel = result.getCmd().getReply("channel");
                permanentSecurityType = result.getCmd().getReply("security");
                permanentPasswd = result.getCmd().getReply("password");
            }
        }


        // Fetch wifi guest information
        cmd = new CommandEntry("status", "");

        request = new BlockedCommandHandler();
        cf.sendCommand("guestMgr", cmd, gwJid, request);

        result = request.waitForResult();
        if (result.getResultType() != BlockedCommandResult.REPLY) {
            System.err.println("LoginActivity : Did not receive reply on guestMgr status cmd");
        }else{
            guestSsid = result.getCmd().getReply("ssid");
            guestState = result.getCmd().getReply("state");
            wifiGuestIf = result.getCmd().getReply("interface");
        }
        cmd = new CommandEntry("create", "");
        cmd.setInput("delay", "");
        cmd.setInput("state", guestState);

        request = new BlockedCommandHandler();
        cf.sendCommand("guestMgr", cmd, gwJid, request);

        result = request.waitForResult();
        if (result.getResultType() != BlockedCommandResult.REPLY) {
            System.err.println("LoginActivity : Did not receive reply on guestMgr create cmd");
        }else{
            guestDelayed = result.getCmd().getReply("delay");
        }
        statusUpToDate = true;
    }
    public String getGatewayJid() {
        return this.gwJid;
    }
    public String getWifiPermanentIf(){
        determineConnection();
        return this.wifiIf;
    }
    public String getGuestIf(){
        determineConnection();
        return this.wifiGuestIf;
    }
    public String getPermanentSsid() {
        determineConnection();
        return this.permanentSsid;
    }
    public String getPermanentSecurityType() {
        determineConnection();
        return this.permanentSecurityType;
    }
    public String getPermanentPasswd() {
        determineConnection();
        return this.permanentPasswd;
    }
    // channel can be set at different place. Need to make sure we
    public String getPermanentChannel() {
        determineConnection();
        statusUpToDate = false;
        updateWifiSettings();
        return this.permanentChannel;
    }
    public boolean isGatewayLocal() {
        determineConnection();
        return gwIsLocalAccessible;
    }
    public void setGuestTimeout(String timeout) {
        CommandEntry cmd = new CommandEntry("create", "");
        cmd.setInput("delay", timeout);
        cmd.setInput("ssid", "");
        cmd.setInput("state", guestState);

        BlockedCommandHandler request = new BlockedCommandHandler();
        cf.sendCommand("guestMgr", cmd, gwJid, request);
        BlockedCommandResult result = request.waitForResult();
        if (result.getResultType() != BlockedCommandResult.REPLY) {
            System.err.println("LoginActivity : Did not receive reply on guestMgr create cmd");
        }else{
            guestDelayed = result.getCmd().getReply("delay");
        }
    }
    public void setGuest(String ssid, String state) {
        CommandEntry cmd = new CommandEntry("create", "");
        cmd.setInput("ssid", ssid);
        cmd.setInput("state", state);

        BlockedCommandHandler request = new BlockedCommandHandler();
        cf.sendCommand("guestMgr", cmd, gwJid, request);
        BlockedCommandResult result = request.waitForResult();
        if (result.getResultType() != BlockedCommandResult.REPLY) {
            System.err.println("LoginActivity : Did not receive reply on guestMgr create cmd");
        }else{
            guestSsid = result.getCmd().getReply("ssid");
            guestState = result.getCmd().getReply("state");
        }

    }
    public void setGuestState(String state) {
        CommandEntry cmd = new CommandEntry("create", "");
        cmd.setInput("ssid", guestSsid);
        cmd.setInput("state", state);

        BlockedCommandHandler request = new BlockedCommandHandler();
        cf.sendCommand("guestMgr", cmd, gwJid, request);
        BlockedCommandResult result = request.waitForResult();
        if (result.getResultType() != BlockedCommandResult.REPLY) {
            System.err.println("LoginActivity : Did not receive reply on guestMgr create cmd");
        }else{
            guestSsid = result.getCmd().getReply("ssid");
            guestState = result.getCmd().getReply("state");
        }

    }
    public String getGuestTimeout() {
        determineConnection();
        return guestDelayed;
    }
    public String getGuestSsid() {
        determineConnection();
        return guestSsid;
    }
    public void forceUpdate() {
        statusUpToDate = false;
        gwIsLocalAccessible = false;
        gwJid = "";
        apisAvailable = false;
    }
    public String getGuestState() {
        determineConnection();
        return guestState;
    }
    public void setPermanent(String ssid, String pass) {
        CommandEntry cmd = new CommandEntry("wifi", "");
        cmd.setInput("interface", wifiIf);
        cmd.setInput("ssid", ssid);
        cmd.setInput("password", pass);

        BlockedCommandHandler request = new BlockedCommandHandler();
        cf.sendCommand("netMgr", cmd, gwJid, request);
        BlockedCommandResult result = request.waitForResult();
        if (result.getResultType() != BlockedCommandResult.REPLY) {
            System.err.println("LoginActivity : Did not receive reply on netMgr wifi cmd");
        }else{
            permanentSsid = result.getCmd().getReply("ssid");
            permanentPasswd = result.getCmd().getReply("password");
        }

    }
    public boolean isApiAvailable() {
        return apisAvailable;
    }
    public boolean installingApi() {
        return installingAPI;
    }
    private boolean makeAPIsAvailable(String deviceAccount, String topic, String apiName) {
        // Check if the needed API's are already available
        // <command xmlns='http://jabber.org/protocol/commands' node='upgrade.execute' action='execute' sessionid='9457'>
        // <x xmlns='jabber:x:data' type='form'><field type='text-single' var='widget'><value>OpenVPN</value>
        // </field><field type='text-single' var='gatewayJID'><value>40crxs4327x8g83@cloudfriends.be/5c67248a</value></field></x></command>
        String[] valueList = getArrayFromSWIG(cf.getRemoteTopicList(deviceAccount));

        int j = 0;
        while (j < valueList.length) {
            System.out.println("LoginActivity: topic present: " + valueList[j]);
            if (valueList[j].equals(topic)) {
                System.out.println("LoginActivity: topic already installed: " + valueList[j]);
                return true;
            }
            j++;
        }
        System.out.println("LoginActivity: Need to install topic: " + topic);
        installingAPI = true;
        CommandEntry cmd = new CommandEntry("execute", "");
        cmd.setInput("gatewayJID", deviceAccount);
        cmd.setInput("widget", apiName);

        BlockedCommandHandler request = new BlockedCommandHandler();
        String dst = "packagemanager." + server;
        cf.sendCommand("upgrade", cmd, dst, request);
        BlockedCommandResult result = request.waitForResult();
        installingAPI = false;
        if (result.getResultType() != BlockedCommandResult.REPLY) {
            System.err.println("LoginActivity : Did not succeed to install package");
            return false;
        }

        return true;
    }
    public String[] getArrayFromSWIG(SWIGTYPE_p_std__listT_std__string_t valueList) {
        Set<String> replySet = new NativeSet<String>(String.class, StringListIterator.class,
                valueList, StringListWrapper.class);
        String[] reply = replySet.toArray(new String[0]);
        return (reply);
    }
    public String[] getArrayReply(BlockedCommandResult result, String parameter) {
        SWIGTYPE_p_std__listT_std__string_t listReply = JNIbridge.getEmptyCoreStringList();
        result.getCmd().getReply(parameter, listReply);
        Set<String> replySet = new NativeSet<String>(String.class, StringListIterator.class,
                listReply, StringListWrapper.class);
        String[] reply = replySet.toArray(new String[0]);
        return (reply);
    }
    public boolean isTryingToReConnect() {
        return tryingToReConnect;
    }
    public void reconnect() {
        String text = getResources().getString(R.string.login_retry_message);
        LoginActivity.loginActivity.showMessage(text);
        //ImageButton launch = (ImageButton) findViewById(R.id.login_button);
        //launch.performClick();
        if (tryingToReConnect == false) {
            statusUpToDate = false;
            tryingToReConnect = true;
            LoginActivityStatus loginStatus = new LoginActivityStatus();
            Log.i("coucou","reconnect");
            loginStatus.start(userN, passWd, serV);
        }
    }
    
  //hockeyApp functions
  	private void checkForCrashes() {
  		CrashManager.register(this, HOCKEY_APP_ID);
  	}

  	private void checkForUpdates() {
  		// Remove this for store builds!
  		UpdateManager.register(this, HOCKEY_APP_ID);
  	}
}
