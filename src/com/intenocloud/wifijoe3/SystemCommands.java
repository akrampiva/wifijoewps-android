package com.intenocloud.wifijoe3;

import android.content.pm.ApplicationInfo;

import com.intenocloud.core.ServiceCommunication;

/**
 * Created by erik on 9/16/13.
 */
public class SystemCommands extends ServiceCommunication {
    private ApplicationCommand applicationCommand = null;
    private OSCommand osCommand = null;
    private StressCommunicationCommand stressCommunicationCommand = null;
    private BigMessageCommand bigMessageCommand = null;


    public SystemCommands(String topicName) {
        super(topicName);
        applicationCommand = new ApplicationCommand("application", "information about the application");
        addCommand(applicationCommand);
        osCommand = new OSCommand("os", "Retrieve information of the OS");
        addCommand(osCommand);

        boolean DEBUGGABLE = (LoginActivity.loginActivity.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        if (DEBUGGABLE) {
            System.out.println("SystemCommands added debug command to stress communication");
            stressCommunicationCommand = new StressCommunicationCommand("stressCommunication", "activate test to stress the communication between android and a GW");
            addCommand(stressCommunicationCommand);
            System.out.println("SystemCommands added debug command to test big message communication");
            bigMessageCommand = new BigMessageCommand("bigMessageCommand", "Test to send over large messages");
            addCommand(bigMessageCommand);
        }

        registerService();
    }
}
